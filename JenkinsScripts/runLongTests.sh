#!/bin/bash
set +e
cd $WORKSPACE/MUQ
build/modules/RunAllLongTests --gtest_output=xml:$WORKSPACE/TestResultsLong.xml

exit 0
