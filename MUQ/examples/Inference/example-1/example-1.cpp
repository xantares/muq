/** \example inference-example-1.cpp
 *  <h1>Introduction</h1>
 *  <p>Welcome to the Inference examples. The Inference module uses the models, random variables,
 *  and densities described in Modelling to define problems and solves them using various MCMC
 *  algorithms. This first example will introduce some of the classes and parameters used for sampling
 *  from a Gaussian target distribution using Metropolis-Hastings. Later examples will demonstrate how 
 *  to sample from user specified densities and from a Bayesian posterior using other MCMC algorithms.</p>
 *  <h3> About this example </h3>
 *  <p> This and following examples will introduce the syntax for defining and solving a simple
 *  sampling problem. In this example, we will sample from a 2D Gaussian distribution using 
 *  Metropolis-Hastings. The target distribution is: 
 *  \f[
 *  \pi_\mathbf{x}(x_1,x_2) = \frac{1}{\sqrt{\left( 2 \pi \right)^2\left| {\Sigma} \right|}}
 *  \exp{\left(-\frac{1}{2}(\mathbf{x}-{\mu})^T{\Sigma}^{-1}(\mathbf{x}-
 *  {\mu})\right)}
 *  \f]
 *  where \f${\mu}\f$ is the mean and \f${\Sigma}\f$ is the covariance
 *  \f{eqnarray*}{
 *  {\mu} &=& [0, 1]^{T} \\
 *  {\Sigma} &=& [1, 0.5; 0.5, 1]
 *  \f}
 *  The main steps are: (1) define the target density, or more generally a sampling problem,
 *  (2) sample from the density using MCMC, and (3) extract useful information
 *  from the samples.
 *  </p>
 *
 *  <h3> Line by line explanation </h3>
 *  See example-1.cpp for finely documented code.
 */

#include <iostream>
#include <Eigen/Core>
#include <boost/property_tree/ptree.hpp>

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/MCMC/MCMCBase.h"
#include "MUQ/Inference/MCMC/MCMCProposal.h"

using namespace std;
using namespace Eigen;

using namespace muq::Modelling;
using namespace muq::Inference;

int main()
{
  /// PART I: Defining the target density and posing it as a sampling problem
  // we will sample from a two dimensional Gaussian using MH
  int dim = 2;
 
  // initialize the mean and covariance for the multivariate normal density
  // here, we use vector and matrices from Eigen
  VectorXd gaussMean(dim); 
  MatrixXd gaussCov(dim,dim);
  gaussMean << 0, 1;
  gaussCov << 1, 0.5, 0.5, 1;

  // create the multivariate normal density from the mean and covariance
  // the class GaussianDensity is a Density which is a special type of ModPiece
  auto gaussDens = make_shared<GaussianDensity>(gaussMean, gaussCov);

  // the class SamplingProblem uses the density to create a sampling problem 
  // that MCMC solvers can use
  auto problem = make_shared<SamplingProblem>(gaussDens);
  
  /// PART II: Defining the MCMC algorithm and parameters for MH
  // we use the ptree from Boost to specify parameters for MCMC
  // a list of parameters for different MCMC algorithms can be found in ____
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Steps", 1000);
  params.put("MCMC.BurnIn", 10);
  params.put("MCMC.MH.PropSize", 2.4*2.4/2.0*1.5);
  params.put("Verbose", 0);

  // the following line connects the problem (of type SamplingProblem)
  // and the property tree that we just constructed to fully construct
  // the MCMC task
  auto mcmc = MCMCBase::ConstructMCMC(problem, params);

  /// PART III: Sampling using MCMC and extracting information from samples
  // we sample from the distribution by calling Sample() and passing in
  // an Eigen vector for the starting point. this constructs an EmpRvPtr 
  // which is a pointer to a class that stores all the samples.
  Eigen::VectorXd startingPoint = Eigen::VectorXd::Zero(dim);
  EmpRvPtr mcmcChain = mcmc->Sample(startingPoint);

  // we can obtain the mean and covariance of samples by calling getMean() 
  // getCov() from the EmpRvPtr
  Eigen::VectorXd mean = mcmcChain->getMean();
  Eigen::MatrixXd cov  = mcmcChain->getCov();
  cout << "True Mean:\n";
  cout << gaussMean << endl << endl;
  cout << "Sample Mean:\n";
  cout << mean << endl << endl;
  cout << "True Covariance:\n";
  cout << gaussCov << endl << endl;
  cout << "Sample Covariance:\n";
  cout << cov << endl << endl;

  // we can find the effective sample size (as an Eigen vector) using getEss()
  Eigen::VectorXd ess = mcmcChain->getEss();
  // find the minimum and maximum ess
  cout << "Minimum ESS: " << ess.minCoeff() << endl;
  cout << "Maximum ESS: " << ess.maxCoeff() << endl << endl;

  // the samples themselves can be written out in plain text for processing
  mcmcChain->WriteRawText("mcmcChain.txt");
  cout << "Raw samples are written in mcmcChain.txt" << endl;
}

