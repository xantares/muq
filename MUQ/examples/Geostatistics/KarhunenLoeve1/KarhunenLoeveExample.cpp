
#include <iostream>
#include <Eigen/Core>


#include "MUQ/Modelling/GaussianRV.h"
#include "MUQ/Modelling/LinearModel.h"

#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Geostatistics/PowerKernel.h"
#include "MUQ/Modelling/ModGraphOperators.h"
#include "MUQ/Modelling/ModGraphPiece.h"

using namespace muq::Modelling;
using namespace muq::Geostatistics;
using namespace muq::Utilities;
using namespace std;

/** This example illustrates the use of a truncated Karhunen-Loeve decomposition to sample from a one dimensional
 *  Gaussian-Process.
 *
 *   BACKGROUND:
 *   In many cases, a parameter of interest is defined as the value of a random process discretized onto some mesh.
 *   This can lead to very high dimensional parameter spaces.  However, correlation between the discretized parameters
 *      allows for an accurate approximation to the field using only a few discretization-independent parameters.  Using
 *      the Karhunen-Loeve decomposition of the random processes covariance function is one method of defining these few
 *      discretization-independent parameters.
 *
 *   THIS EXAMPLE:
 *   Consider a one dimensional Gaussian process with mean field mu(x) and covariance function cov(x,x').  This example
 *      illustrates how MUQ can be used to approximately sample from a discretization of this Gaussian Process on a one
 *      dimensional mesh using the Karhunen-Loeve decomposition.  The decomposition not only can reduce the parameter
 *      dimension, but decorrelates the paraemeters as well.
 *
 */
int main()
{
  // //////////////////////////////////////////////////////////////////////////////////
  // ////////////////////// DEFINING THE MESH /////////////////////////////////////////
  // //////////////////////////////////////////////////////////////////////////////////
  // first things first, we need to define the mesh we will be working on.  In this case, the mesh will be a uniformly
  // space grid in one dimension.

  std::cout << "Defining the one dimensional mesh..." << std::flush;

  // first define a fixed size matrix holding the lower and upper bounds
  Eigen::Matrix<double, 1, 1> lb(1, 1); // the second template parameter is the spatial dimension
  lb(0) = 0;

  // now define a fixed size matrix holding the upper bounds
  Eigen::Matrix<double, 1, 1> ub(1, 1);
  ub(0) = 1;

  // define the mesh discretization -- i.e. how many elements do we want in each dimension, the number of nodes  in the mesh will be N+1
  Eigen::Matrix<unsigned int, 1, 1> N(1);
  N(0) = 100;

  // create the mesh and store it in a smart pointer -- the template parameter (1 here) gives the spatial dimension of the mesh
  std::shared_ptr<Mesh<1>> meshPtr = make_shared<StructuredQuadMesh<1>>(lb, ub, N);

  std::cout << "done\n";

  // //////////////////////////////////////////////////////////////////////////////////
  // /////////// DEFINING THE COVARIANCE FUNCTION /////////////////////////////////////
  // //////////////////////////////////////////////////////////////////////////////////
  // The covariance function of the Gaussian process describes the smoothness of process realizations, long length
  // scales or slowly decaying functions result in smoother realizations.  Below we will use a Gaussian covariance
  // function which imposes a lot of smoothness.

  std::cout << "Building the covariance function..." << std::flush;

  // define the length scale
  double L = 0.2;

  // define the variance of the process
  double sig = 1.0;

  // create the kernel
  auto kernelPtr = make_shared<PowerKernel>(L, 2.0, sig); // using 2.0 as the second input parameter gives the classic
                                                           // "Gaussian" covariance function

  std::cout << "done\n";

  // //////////////////////////////////////////////////////////////////////////////////
  // /////////////// COMPUTE THE KL DECOMPOSITION /////////////////////////////////////
  // //////////////////////////////////////////////////////////////////////////////////

  std::cout << "Constructing the KL decomposition..." << std::flush;

  // how many bases do we want to keep in the decomposition?
  unsigned numBasis = 10;

  // compute the KL bases from the Covariance Kernel -- NOTE: this function returns the weighted modes, so the matrix is
  // ready to use for sampling.  
  // 
  // There are many ways to construct a KL decomposition.  MUQ has a few of these methods built in.  For a 1d mesh, the BuildKL function
  // will will a Nystrom method to approximate the Karhunen-Loeve integral equation and approximate the Eigen functions of the covariance kernel.
  // More information on the different ways to construct a KL decomposition can be found in the CovKernel documentation.
  Eigen::MatrixXd KlBases = kernelPtr->BuildKL(meshPtr, numBasis);

  std::cout << "done\n";

  // //////////////////////////////////////////////////////////////////////////////////
  // /////////////// USE THE KL DECOMPOSITION TO SAMPLE ///////////////////////////////
  // //////////////////////////////////////////////////////////////////////////////////

  /* Assume theta is the discretized version the random process and z are the new uncorrelated parameters.  In the
   *  simplest case, any inference or sampling would be performed directly on theta, but now we use the KL modes to
   *  decorrelate theta and we can work on the lower dimensional z instead.
   */

  // create a ModPiece to represent the IID Gaussian weights on the KL modes
  auto z = make_shared<GaussianRV>(numBasis);
  
  // define the mean of the Gaussian process -- one.  Notice this is a vector of N(0) so it has the same dimension as the number of cells in the mesh
  Eigen::VectorXd muTheta = Eigen::VectorXd::Ones(N(0));
  
  // create a ModPiece for the random field.  There is a little operator overloading magic that happens in this line.  In the background, a graph containing two ModPieces, a z ModPiece and a LinearModel ModPiece, is constructed.  This graph is then turned into a ModGraphRandVar and returned as the theta ModPiece.
  auto graph = muTheta + KlBases * z;
  auto theta = ModGraphPiece::Create(graph);
 
  // At this point, theta is a random variable with mean one and correlation structure defined by the KL modes.  We can easily sample theta by calling the 
  // the ModPiece Evaluate() function.

  // define the number of samples we want to take
  int Nsamps = 5;

  // create a vector to hold one sample
  Eigen::VectorXd SampVec(N(0));

  std::cout << "Printing a few realizations of the Gaussian random field:\n";
  for (unsigned int i = 0; i < Nsamps; ++i) {
    SampVec = theta->Evaluate();
    std::cout << SampVec.transpose() << std::endl << std::endl;
  }

  return 0;
}

