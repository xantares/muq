# This imports the modelling library. 
import libmuqModelling

# This class describes the right hand side of our predator prey model.  All MUQ models need to be defined as
# a child class of the ModPiece class.  For models that do not need the full flexibility of the ModPiece
# base class, several one-input template classes are provided.  In this tutorial, our model definition
# is going to inherit from the muq::modelling::OneInputNoDerivModPiece class.  This class provides a single
# vector-valued input template that does not require the user to provide derivative information (more on
# computing gradients, Jacobians, and Hessians can be found in later examples).
class PredPreyModel(libmuqModelling.OneInputNoDerivModPiece):
    # The constructor for our Model takes the 6 fixed parameters in the predator prey model and initializes
    # the OneInputNoDerivModPiece parent class.  The OneInputNoDerivModPiece constructor requires the input
    # and output dimensions of the model.  In our case, the input and output are both two dimensional vectors.
    # Note that the input and output dimensions of all models in MUQ are constant.  This means that the input
    # and output dimensions must be set when the constructor is called, and also that the input and output
    # dimensions of an existing Model cannot be changed.
    def __init__(self, r, k, s, a, u, v):
        libmuqModelling.OneInputNoDerivModPiece.__init__(self, 2, 2)

        self.preyGrowth      = r
        self.preyCapacity    = k
        self.predationRate   = s
        self.predationHunger = a
        self.predatorLoss    = u
        self.predatorGrowth  = v
    
        # The EvaluateImpl class is where all the magic happens.  This is where the model computation is performed.  Note
        # that this function is defined as a private function.  To provide some extra functionality such as caching, MUQ
        # separates the model implementation (this function) from the evaluation interface (the Evaluate function
        # demonstrated in the main function below).
    def EvaluateImpl(self, ins):
        # grab the prey and predator populations from the input vector
        preyPop = ins[0]
        predPop = ins[1]

        # create a vector to hold the model output, this will hold the time derivatives of population
        output = [None]*2

        # the first output of this function is the derivative of the PREY population size with resepct to time
        output[0] = preyPop * (self.preyGrowth * (1 - preyPop / self.preyCapacity) 
                               - self.predationRate * predPop / (self.predationHunger + preyPop))

        # the second output of this function is the derivative of the PREDATOR population size with resepct to time
        output[1] = predPop * (self.predatorGrowth * preyPop / (self.predationHunger + preyPop) - self.predatorLoss)

        # return the output
        return output

# Now that we have define the forward model in the PredPreyModel class, let's play around with the model a bit.
# The main function creates an instance of PredPreyModel and demonstrates how to evaluate the model.
if __name__ == '__main__':
    # Before constructing the model, we will set the fixed model values.
    preyGrowth      = 0.8
    preyCapacity    = 100
    predationRate   = 4.0
    predationHunger = 15
    predatorLoss    = 0.6
    predatorGrowth  = 0.8

    # create an instance of the ModPiece defining the predator prey model.
    predatorPreyModel = PredPreyModel(preyGrowth,
                                      preyCapacity,
                                      predationRate,
                                      predationHunger,
                                      predatorLoss,
                                      predatorGrowth)

    # create an input vector holding both the predator and prey populations
    # [prey population, predator population]
    populations = [50.0, 5.0]

    # Evaluate the model with this the values in the populations vector.  Notice that we call the Evaluate function and not the EvaluateImpl function defined in the PredPreyModel class.  The Evaluate function checks the input sizes and provides
   # one step caching.  This means that calling the model two times in a row with the same input will only result in one call to EvaluateImpl. For some algorithms and expensive Models, this can result in dramatic time savings.
    growthRates = predatorPreyModel.Evaluate(populations)
    
    # print the growth rates 
    print "The prey growth rate evaluated at     (", populations[0], ",", populations[1], ") is ", growthRates[0]
    print "The predator growth rate evaluated at (", populations[0], ",", populations[1], ") is ", growthRates[1]

    # We can also ask the model for it's input and output sizes.  Every Model in MUQ has a constant vector, "inputSizes",
    # that contains the length of each vector valued input.  Since our Model only has one input, inputSizes only has one entry.  The output dimension
    # of the model is stored in the "outputSize" variable.  Since MUQ models can only have one output, "outputSize" is simply an integer.
    print "The input size is  ", predatorPreyModel.inputSizes[0] 
    print "The output size is ", predatorPreyModel.outputSize
  
