
#include "gtest/gtest.h"

#include <iostream>
#include <Eigen/Core>
#include <Eigen/Dense>

#include "MUQ/Utilities/CachedFunctionWrapper.h"
#include "MUQ/Utilities/DataFileWrapper.h"
#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/EigenTestUtils.h"


using namespace std;
using namespace Eigen;
using namespace muq::Utilities;

//This test exercises all of the data file interfaces, along with the cached inherited functions
TEST(UtilitiesDeathTest, FunctionWrapperDataFile)
{
  ::testing::FLAGS_gtest_death_test_style = "threadsafe"; //avoids a warning about not being threadsafe

  //load some data
  DataFileWrapper::Ptr testFn = DataFileWrapper::WrapFile("data/tests/DataFileWrapperExample.dat", 3, 2);

  MatrixXd allPoints = testFn->GetCachePoints();

  EXPECT_EQ(3, allPoints.rows());
  EXPECT_EQ(3, allPoints.cols());

  Vector3d testInput;
  testInput << 1, 2, 3;
  Vector2d testTrueResult;
  testTrueResult << 4, 5;
  VectorXd testResult = testFn->Evaluate(testInput);
  EXPECT_TRUE(MatrixApproxEqual(testTrueResult, testResult, 1e-14));

  //not there, so it fails
  Vector3d testBadInput;
  testBadInput << 1, 2, 1;
  EXPECT_DEATH(testFn->Evaluate(testBadInput), "");
}

TEST(Utilities, ExternalFunction)
{
  CachedFunctionWrapper::Ptr fn = CachedFunctionWrapper::WrapExecutableFunction("data/tests/testShellFn", 3, 3);

  MatrixXd testPoints(3, 4);

  testPoints << 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12;

  EXPECT_TRUE(MatrixApproxEqual(testPoints, fn->Evaluate(testPoints), 1e-14));
  EXPECT_EQ(4, fn->GetNumOfEvals());
}

VectorXd simplefn(VectorXd const&)
{
  return VectorXd::Zero(1);
}

TEST(Utilities, CachedNeighborFunctions)
{
  CachedFunctionWrapper::Ptr fn = CachedFunctionWrapper::WrapVectorFunction(simplefn, 3, 1);

  MatrixXd points(3, 4);

  points << 1, 2, 3, 4, 2, 6, 7, 8, 3, 10, 11, 12;

  fn->Evaluate(points); //add to cache


  VectorXd testPoint(3);
  testPoint << 2, 2, 2;

  MatrixXd twoNearest(3, 2);
  twoNearest <<  1,  2, 2,  6, 3, 10;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, twoNearest, fn->FindKNearestNeighbors(testPoint, 2), 1e-14);

  MatrixXd newPoints(3, 2);
  newPoints << 2, 3, 2, 3, 3, 9;
  fn->Evaluate(newPoints); //add to cache

  MatrixXd radiusNearest(3, 2);
  radiusNearest << 2, 1, 2, 2, 3, 3;
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, radiusNearest, fn->FindNeighborsWithinRadius(testPoint, 3.0), 1e-14);
}
