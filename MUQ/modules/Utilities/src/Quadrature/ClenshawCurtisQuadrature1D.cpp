#include "MUQ/Utilities/Quadrature/ClenshawCurtisQuadrature1D.h"

#include <assert.h>
#include <fstream>
#include <string>
#include <sstream>

#include <boost/serialization/base_object.hpp>
#include <boost/serialization/export.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/foreach.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/tokenizer.hpp>

#include "MUQ/Utilities/LogConfig.h"

using namespace Eigen;
using namespace muq::Utilities;

ClenshawCurtisQuadrature1D::ClenshawCurtisQuadrature1D() {}

ClenshawCurtisQuadrature1D::~ClenshawCurtisQuadrature1D() {}

template<class Archive>
void ClenshawCurtisQuadrature1D::serialize(Archive& ar, const unsigned int version)
{
  ar& boost::serialization::base_object<QuadratureFamily1D>(*this);
}

BOOST_CLASS_EXPORT(ClenshawCurtisQuadrature1D) unsigned int ClenshawCurtisQuadrature1D::GetPrecisePolyOrder(
  unsigned int const order) const
{
  switch (order) {
  case 1: return 1;

  case 2: return 3;

  case 3: return 5;

  case 4: return 9;

  case 5: return 17;

  case 6: return 33;

  case 7: return 65;

  case 8: return 129;

  case 9: return 257;

  case 10: return 513;

  case 11: return 1025;

  case 12: return 2049;

  case 13: return 4097;

  case 14: return 8193;

  case 15: return 16385;

  case 16: return 32769;

  default: assert(false);
  }
}

void ClenshawCurtisQuadrature1D::ComputeNodesAndWeights(unsigned int const            order,
                                                        std::shared_ptr<RowVectorXd>& nodes,
                                                        std::shared_ptr<RowVectorXd>& weights) const
{
  BOOST_LOG_POLY("quad", debug) << "Beginning loading of Clenshaw Curtis nodes and weights.";

    assert(nodes && weights); //check the inputs were allocated

  nodes->resize(0);
  weights->resize(0);

  //rewrite the order if you're using non-standard indexing. Limited to the ones that
  //actually show up in the HYCOM data.
  unsigned int numPoints = order;

  switch (order) {
  case 1: numPoints = 1; break;

  case 2: numPoints = 3; break;

  case 3: numPoints = 5; break;

  case 4: numPoints = 9; break;

  case 5: numPoints = 17; break;

  case 6: numPoints = 33; break;

  case 7: numPoints = 65; break;

  case 8: numPoints = 129; break;

  case 9: numPoints = 257; break;

  case 10: numPoints = 513; break;

  case 11: numPoints = 1025; break;

  case 12: numPoints = 2049; break;

  case 13: numPoints = 4097; break;

  case 14: numPoints = 8193; break;

  case 15: numPoints = 16385; break;

  case 16: numPoints = 32769; break;

  default: BOOST_LOG_POLY("quad", error) << "Asked for Clenshaw Curtis " << order << " which is not available.";
    assert(false);
  }

  std::stringstream ssNodes;
  ssNodes << "data/Clenshaw_Curtis/cc_rule_" << numPoints << "_x.txt";
  std::stringstream ssWeights;
  ssWeights << "data/Clenshaw_Curtis/cc_rule_" << numPoints << "_w.txt";


  std::ifstream nodesFile(ssNodes.str().c_str());
    assert(nodesFile.good());   //check that the file is good

  std::ifstream weightsFile(ssWeights.str().c_str());
    assert(weightsFile.good()); //check that the file is good

  //allocate enough space
  nodes->resize(numPoints);
  weights->resize(numPoints);

  std::string nodesLine;
  std::string weightLine;

  //nodes first
  boost::char_separator<char> sep(", \t");    //set up the separator
    assert(getline(nodesFile, nodesLine));    //must read a line
    assert(getline(weightsFile, weightLine)); //must read a line


  //look at the data once to count the number
  boost::tokenizer<boost::char_separator<char> > nodesTokens(nodesLine, sep);
  boost::tokenizer<boost::char_separator<char> > weightsTokens(weightLine, sep);

  unsigned int i = 0;
  BOOST_FOREACH(const std::string & t, nodesTokens)
  {
    (*nodes)(i) = boost::lexical_cast<double>(t);
    ++i;
  }

  assert(i == numPoints); //check enough data was loaded

  i = 0;
  BOOST_FOREACH(const std::string & t, weightsTokens)
  {
    (*weights)(i) = boost::lexical_cast<double>(t);
    ++i;
  }

  assert(i == numPoints); //check enough data was loaded

  BOOST_LOG_POLY("quad", debug) << "Done with loading CC nodes and weights.";
}

