
#include "MUQ/Utilities/LinearOperators/Solvers/CG.h"

// standard library includes
#include <memory>
#include <iostream>

// other muq includes
#include "MUQ/Utilities/LinearOperators/Operators/SymmetricOpBase.h"
#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Utilities;
using namespace std;

Eigen::VectorXd CG::solve(const Eigen::VectorXd& b)
{
  // make sure b is the correct size

  assert(A->cols() == b.rows());
  assert(A->rows() == A->cols());
  assert(dynamic_pointer_cast<SymmetricOpBase>(A));

  // current solution
  Eigen::VectorXd xc = Eigen::VectorXd::Zero(b.rows());

  // current residual
  Eigen::VectorXd rc = b;

  double rSquaredNorm = rc.squaredNorm();

  //
  Eigen::VectorXd pc = rc;

  Eigen::VectorXd Apc = A->apply(pc);

  double dc = pc.dot(Apc);

  for (unsigned int i = 0; i < b.rows(); ++i) {
    // minimize the quadratic in pc direction
    double gamma = rSquaredNorm / dc;

    // update the current solution
    xc += gamma * pc;

    // update the residual
    rc -= gamma * (Apc);
    double rNewSquaredNorm = rc.squaredNorm();

    if (rNewSquaredNorm == 0) {
      break;
    }

    // check termination
    if (sqrt(rNewSquaredNorm) < tol) {
      break;
    }

    double beta          = -1.0 * rNewSquaredNorm / rSquaredNorm;
    Eigen::VectorXd step = -1.0 * beta * pc;
    pc           = rc + step;
    rSquaredNorm = rNewSquaredNorm;

    Apc = A->apply(pc);
    dc  = pc.dot(Apc);
  }

  return xc;
}

Eigen::VectorXd CG::sampleCov(const Eigen::VectorXd& b)
{
  return A->apply(samplePrec(b));
}

Eigen::VectorXd CG::samplePrec(const Eigen::VectorXd& b)
{
  // current solution
  Eigen::VectorXd xc = RandomGenerator::GetNormalRandomVector(b.size());

  // current residual
  Eigen::VectorXd rc = b - A->apply(xc);

  double rSquaredNorm = rc.squaredNorm();

  //
  Eigen::VectorXd pc = rc;

  Eigen::VectorXd Apc = A->apply(pc);
  double dc           = pc.dot(Apc);
  Eigen::VectorXd yc  = Eigen::VectorXd::Zero(b.size());


  for (unsigned int i = 0; i < b.size(); ++i) {
    // minimize the quadratic in pc direction
    double gamma = rSquaredNorm / dc;

    // update the current solution
    xc += gamma * pc;

    // update the residual
    rc -= gamma * (Apc);
    double rNewSquaredNorm = rc.squaredNorm();

    if (rNewSquaredNorm == 0) {
      break;
    }

    yc += (RandomGenerator::GetNormal() / sqrt(dc)) * pc;

    // check termination
    if (sqrt(rNewSquaredNorm) < tol) {
      break;
    }

    double beta          = -1.0 * rNewSquaredNorm / rSquaredNorm;
    Eigen::VectorXd step = -1.0 * beta * pc;
    pc           = rc + step;
    rSquaredNorm = rNewSquaredNorm;

    Apc = A->apply(pc);
    dc  = pc.dot(Apc);
  }


  // return A^{-0.5}*input
  return yc;
}

