
#include "PolynomialChaos/smolyak/SmolyakTraditionalPCEFactory.h"


#include "PolynomialChaos/multiIndex/AdaptiveMultiIndexFamily.h"
#include "PolynomialChaos/pce/SingleQuadraturePCEFactory.h"
#include "PolynomialChaos/pce/PolynomialChaosExpansion.h"
#include "PolynomialChaos/quadrature/QuadratureFamily1D.h"
#include "PolynomialChaos/utilities/LogConfig.h"
#include "PolynomialChaos/utilities/VariableCollection.h"
#include "PolynomialChaos/utilities/FunctionWrapper.h"

using namespace arma;

SmolyakTraditionalPCEFactory::SmolyakTraditionalPCEFactory(VariableCollection::Ptr inVariables,
                                                           FunctionWrapper::Ptr    inFn) : SmolyakEstimate<
                                                                                             PolynomialChaosExpansion::
                                                                                             Ptr>(inVariables->length()),
variables(
                                                                                             inVariables),
                                                                                           fn(
                                                                                             inFn)
{}

SmolyakTraditionalPCEFactory::~SmolyakTraditionalPCEFactory() {}

PolynomialChaosExpansion::Ptr SmolyakTraditionalPCEFactory::ComputeOneEstimate(arma::urowvec const& multiIndex)
{
  SingleQuadraturePCEFactory pceFactory(variables);

  //bump up the order by one because 0th order quadrature is meaningless
  return pceFactory.ConstructPCE(fn, multiIndex, ones<urowvec>(multiIndex.n_cols), termsIncluded);
}

PolynomialChaosExpansion::Ptr SmolyakTraditionalPCEFactory::WeightedSumOfEstimates(arma::colvec const& weights) const
{
  BOOST_LOG_POLY("smolyak", debug) << "Computing a weighted sum";

  PolynomialChaosExpansion::Ptr sumPCE(new PolynomialChaosExpansion(variables));
  AdaptiveMultiIndexFamily::Ptr sumPCEadaptiveTerms = AdaptiveMultiIndexFamily::Create(variables->length(), 0, 1);
  sumPCE->terms = sumPCEadaptiveTerms; //store and down-cast

  //forcibly activate every polynomial term used in the component expansions
  for (unsigned int i = 0; i < weights.n_rows; i++) {
    if (termWeights(i) == 0.0) { //skip any not used
      continue;
    }
    PolynomialChaosExpansion::Ptr ithEst = termEstimates.at(i);
    for (unsigned int j = 0; j < ithEst->terms->GetNumberOfIndices(); j++) {
      sumPCEadaptiveTerms->ForciblyActivateMultiIndex(ithEst->terms->IndexToMulti(j));
    }
  }

  BOOST_LOG_POLY("smolyak", debug) << "Done finding polys";

  //allocate space for all the coeffs
  sumPCE->coeffs = zeros<mat>(fn->outputDim, sumPCEadaptiveTerms->GetNumberOfIndices());

  //loop over every polynomial in the result
  for (unsigned int i = 0; i < sumPCEadaptiveTerms->GetNumberOfIndices(); i++) {
    //allocate space for the values of the individual terms
    mat termCoeffs = zeros<mat>(fn->outputDim, weights.n_rows);

    //save the multi-index we're looking for
    urowvec multiToFind = sumPCEadaptiveTerms->IndexToMulti(i);

    //		BOOST_LOG_POLY("smolyak",debug) << "Finding instances";

    //search over the terms that don't have zero weight for this polynomial
    for (unsigned int j = 0; j < weights.n_rows; j++) {
      if (weights(j) == 0) { //skip zero weight ones
        continue;
      }

      //if this one has the current polynomial
      PolynomialChaosExpansion::Ptr jthEst = termEstimates.at(j);
      if (jthEst->terms->IsMultiInFamily(multiToFind)) {
        //copy the coeffs into the results
        unsigned int index = jthEst->terms->MultiToIndex(multiToFind);
        termCoeffs.col(j) = jthEst->coeffs.col(index);
      }
    }

    //finish by summing the coeffs with the weights and store them in the result
    sumPCE->coeffs.col(i) = (termCoeffs * weights);
  }

  return sumPCE;
}

double SmolyakTraditionalPCEFactory::ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate)
{
  mat coeffCopy = estimate->coeffs;

  //sum the magnitude of the coeffs with the same pce terms
  coeffCopy = sum(abs(coeffCopy), 0);

  //then multiply against the one term normalizations and sum
  mat temp = coeffCopy * estimate->GetNormalizationVec();
  return temp(0, 0);
}

double SmolyakTraditionalPCEFactory::ComputeLocalErrorIndicator(unsigned int const termIndex)
{
  BOOST_LOG_POLY("smolyak", debug) << "starting local error";
  urowvec backwardNeighborIndices = termsIncluded->GetActiveBackwardNeighborIndices(termIndex);

  double localError = differentialMagnitudes.at(termIndex);

  //loop over the valid backward neighbors and add them in
  for (urowvec::iterator k = backwardNeighborIndices.begin(); k != backwardNeighborIndices.end(); ++k) {
    //add in the differential magnitudes of the backward neighbors of the input term,
    //divided by the number found
    localError +=  differentialMagnitudes.at(*k) / (double)backwardNeighborIndices.n_cols;
  }

  BOOST_LOG_POLY("smolyak", debug) << "done local error";

  return localError;
}

umat SmolyakTraditionalPCEFactory::GetEffectiveIncludedTerms()
{
  umat result = zeros<umat>(termsIncluded->GetNumberOfIndices(), termsIncluded->GetMultiIndexLength());

  for (unsigned int i = 0; i < termsIncluded->GetNumberOfIndices(); ++i) {
    urowvec multiIndex = termsIncluded->IndexToMulti(i);
    for (unsigned int j = 0; j < termsIncluded->GetMultiIndexLength(); ++j) {
      result(i, j) = variables->GetVariable(j)->quadFamily->GetPrecisePolyOrder(multiIndex(j) + 1);
    }
  }

  return result;
}

bool SmolyakTraditionalPCEFactory::Refine(arma::ucolvec& newTermIndices)
{
  //use the standard way to figure out which ones to refine
  super::Refine(newTermIndices);

  //return all of them, because we've changed the terms each pce should have, so they all
  //need to be recomputed
  newTermIndices = termsIncluded->GetAllIndices();

  return true;
}

unsigned int SmolyakTraditionalPCEFactory::ComputeWorkDone() const
{
  return fn->GetNumOfEvals();
}

