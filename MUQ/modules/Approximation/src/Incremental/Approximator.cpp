#include "MUQ/Approximation/Incremental/Approximator.h"

#include <boost/math/special_functions.hpp>

#include "MUQ/Utilities/EigenUtils.h"

#include "MUQ/Optimization/Algorithms/OptAlgBase.h"

#include "MUQ/Approximation/Incremental/SpaceFillOptimization.h"
#include "MUQ/Approximation/BallConstraint.h"
#include "MUQ/Utilities/HDF5Logging.h"


using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Optimization;
using namespace muq::Approximation;

Approximator::Approximator(shared_ptr<ModPiece> const& fnIn,
                                               boost::property_tree::ptree para) : OneInputJacobianModPiece(fnIn->inputSizes(
                                                                                                              0),
                                                                                                            fnIn->outputSize),
                                                                                   fn(MakeCached(fnIn)),
                                                                                   spaceFillTol(ReadAndLogParameter<
                                                                                                  double>(
                                                                                                  para,
                                                                                                  "Approximator.SpaceFillTol",
                                                                                                  1e-3)),
                                                                                   spaceFillTries(ReadAndLogParameter<
                                                                                                    int>(
                                                                                                    para,
                                                                                                    "Approximator.SpaceFillTries",
                                                                                                    3))
{
  // the approximated function must only have one input
  assert(fn->inputSizes.size() == 1);

  // no points have been added to the cache
  usedCurrentCache = false;

  // create the optimizer
  boost::optional<boost::property_tree::ptree&> child = para.get_child_optional("Opt");
  if (child) {
    optOptions.add_child("Opt", *child);
  } else {
    optOptions.put("Opt.Method", "NLOPT");
    optOptions.put("Opt.NLOPT.Method", "MMA");
    optOptions.put("Opt.MaxIts", 10000);
    optOptions.put("Opt.NLOPT.xtol_rel", spaceFillTol);
    optOptions.put("Opt.NLOPT.xtol_abs", spaceFillTol / 10.0);
    optOptions.put("Opt.stepLength", .001);
  }

}

boost::optional<Eigen::VectorXd> Approximator::RefineNear(Eigen::VectorXd point)
{
  // inputs and outputs of nearest neighbors
  Eigen::MatrixXd inputs;
  Eigen::MatrixXd outputs;

  // find the nearest neighbors
  NearestNeighbors(inputs, outputs, point);

  // compute how far each point is from the center and the maximum distance
  const Eigen::VectorXd distances = (inputs.colwise() - point).colwise().norm();
  const double maxDistance        = distances.maxCoeff();

  // if the point is in the cache
  while (fn->IsEntryInCache(point)) {
    // move a little bit
    point += maxDistance * spaceFillTol * point / point.norm();
  }

  Eigen::VectorXd refine;

  //From above, R=maxDistance. We'd like to force it to find a new point within R/3, so
  //it needs all the neighbors within R to work with. In principle, the new point only needs
  //to be within R, but keeping it closer to the middle ensures it will be relevant.
  for (unsigned int i = 0; i < spaceFillTries; ++i) {
    // find the neighbors that are maxDistance away
    Eigen::MatrixXd neighbors = fn->FindNeighborsWithinRadius(point, maxDistance / (double)exp2(i));

    // if there are 0 or 1 neighbors
    if (neighbors.cols() <= 1) {
      // make sure the point is not already in the cache
      assert(!fn->IsEntryInCache(point));

      refine = point;

      // get out of for loop
      break;
    }

    // center the points
    Eigen::MatrixXd centered = neighbors.colwise() - point;

    //we know the scaling from our choices of R
    const double scaling = 0.333 * maxDistance / (double)exp2(i);

    //check the furthest distance
    const double furthestDistance = centered.colwise().norm().maxCoeff();
      assert(furthestDistance > 0); // it can't be the center itself

    // scale the centered points
    centered = centered.array() / scaling;

    // create an optimization problem
    auto problem = make_shared<SpaceFillOptimization>(fn->inputSizes(0), centered);

    // the constraint for the optimization problem
    auto constraint = make_shared<BallConstraint>(fn->inputSizes(0));

    // add the constraint to the problem (it is inequality)
    problem->AddConstraint(constraint, false);

    // create a solver for the optimization
    auto solver = OptAlgBase::Create(problem, optOptions);

    // solve the optimization, starting from the center point
    refine = solver->solve(Eigen::VectorXd::Zero(fn->inputSizes(0)));

    // if the solve worked
    if (solver->GetStatus() > 0) {
      // violates bound constraint? farther away from the center of the ball (which is zero because we have shifted)
      // than the radius (which is one because we rescaled)
      assert(refine.norm() <= 1.01);

      // rescale the point
      refine = scaling * refine + point;

      //this point is already present, so keep going
      if (fn->IsEntryInCache(refine)) {
        continue;
      }

      // add the new point to the cache
        AddToCache(refine);

      //check if the refine actually worked
      if (fn->IsEntryInCache(refine)) {
        //if so, quit successfully and return the new point
        return boost::optional<Eigen::VectorXd>(refine);
      }
    } else if (i == spaceFillTries - 1) { // failed and we are out of tried
      // try to add the center point
      if (!fn->IsEntryInCache(point)) {
        refine = point;

        // add the new point to the cache
        AddToCache(refine);

        if (fn->IsEntryInCache(refine)) {
          //if so, quit successfully and return the new point
          return boost::optional<Eigen::VectorXd>(refine);
        }
      }
    }
  }

  //apparently we're out of tries and even the center point didn't work. Return failure.
  return boost::optional<Eigen::VectorXd>();
}

shared_ptr<CachedModPiece> Approximator::MakeCached(shared_ptr<ModPiece> const& fnIn)
{
  // try to convert to CachedModPiece
  auto fnCached = dynamic_pointer_cast<CachedModPiece>(fnIn);

  // if covertion worked
  if (fnCached) {
    // return the CachedModPiece
    return fnCached;
  }

  // make a CachedModPiece
  return make_shared<CachedModPiece>(fnIn);
}

bool Approximator::AddToCache(Eigen::MatrixXd const& pts)
{
  bool addedSuccessfully = true;

  // make sure the dimension is the right size
  assert(pts.rows() == fn->inputSizes(0));

  // how many points are we adding?
  const unsigned int N = pts.cols();

  // loop through the number of points
  for (unsigned int i = 0; i < N; ++i) {
    // evaluate the function; adds point to the cached since this is a CachedModPiece
    fn->Evaluate(pts.col(i));

    addedSuccessfully = addedSuccessfully && fn->IsEntryInCache(pts.col(i));
  }

  // more points have been added to the cache
  InvalidateCache();
  usedCurrentCache = false;

  return addedSuccessfully;
}

void Approximator::NearestNeighbors(Eigen::MatrixXd      & inputs,
                                              Eigen::MatrixXd      & outputs,
                                              Eigen::VectorXd const& point) const
{
  // if we don't have enough points; use the maximum we can
  const unsigned int numPoints = min(fn->GetNumOfEvals(), kN);

  assert(numPoints > 0);

  // get the kN nearest neighbors
  auto samps = fn->FindKNearestNeighborsWithEvaluations(point, numPoints);

  // resize the inputs and outputs
  inputs.resize(fn->inputSizes(0), numPoints);
  outputs.resize(fn->outputSize, numPoints);

  // counter
  unsigned int cnt = 0;

  // loop through each sample
  for (auto s : samps) {
    inputs.col(cnt)    = s->cachedInput.at(0);
    outputs.col(cnt++) = *(s->GetCachedEvaluate());
  }
}

bool Approximator::ResetNumNearestNeighbors(unsigned int const newKN)
{
  assert(newKN > 0);

  kN = newKN;

  return fn->GetNumOfEvals() >= kN;
}

unsigned int Approximator::NumNearestNeighbors() const
{
  return kN;
}

unsigned int Approximator::NumAvailableNearestNeighbors() const
{
  return fn->GetNumOfEvals();
}


Eigen::MatrixXd Approximator::GetCachePoints() const
{
  return fn->GetCachePoints();
}

shared_ptr<CachedModPiece> Approximator::GetModel() const
{
  return fn;
}

void Approximator::SetMode(Modes newMode)
{
	assert(newMode == standard); //only this mode is supported by default
}


void Approximator::SetCrossValidationIndex(int const i)
{
	assert(false); //this function may not be used by default
}

