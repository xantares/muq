set(approximationSources 
    utilities/PolychaosXMLHelpers.cpp 
    
    Expansions/ExpansionBase.cpp
    Expansions/PolynomialExpansion.cpp
    
    pce/PolynomialChaosExpansion.cpp
    pce/SingleQuadraturePCEFactory.cpp

    smolyak/SmolyakEstimate.cpp
    smolyak/SmolyakPCEFactory.cpp
    smolyak/SmolyakQuadrature.cpp

    Regression/Regressor.cpp
    Regression/PolynomialRegressor.cpp
    Regression/GaussianProcessRegressor.cpp
    Regression/PoisednessCost.cpp

    BallConstraint.cpp	
)

if(MUQ_USE_NLOPT)
    set(approximationSources
        ${approximationSources}
    Incremental/Approximator.cpp	
    Incremental/PolynomialApproximator.cpp	
    Incremental/SpaceFillOptimization.cpp	
    Incremental/GPApproximator.cpp	
	)
endif(MUQ_USE_NLOPT)

if(MUQ_USE_PYTHON)
    set(approximationSources
        ${approximationSources}
       ../python/RegressorPython.cpp
       ../python/PolynomialRegressorPython.cpp
       ../python/PolynomialChaosExpansionPython.cpp
       ../python/ApproximationPythonModule.cpp
       ../python/SmolyakPython.cpp
	)
endif(MUQ_USE_PYTHON)

if(MUQ_USE_NLOPT AND MUQ_USE_PYTHON)
    set(approximationSources
        ${approximationSources}

       ../python/PolynomialApproximatorPython.cpp
       ../python/ApproximatorPython.cpp
	)
endif(MUQ_USE_NLOPT AND MUQ_USE_PYTHON)

#### Define an InferenceEngine library
ADD_LIBRARY(muqApproximation ${approximationSources})
TARGET_LINK_LIBRARIES(muqApproximation muqOptimization muqUtilities muqGeostatistics ${MUQ_LINK_LIBS})

if(APPLE AND MUQ_USE_PYTHON)
    set_property(TARGET muqApproximation PROPERTY PREFIX "lib")
    set_property(TARGET muqApproximation PROPERTY OUTPUT_NAME "muqApproximation.so")
    set_property(TARGET muqApproximation PROPERTY SUFFIX "")
    set_property(TARGET muqApproximation PROPERTY SOVERSION "32.1.2.0")
endif(APPLE AND MUQ_USE_PYTHON)

install(TARGETS muqApproximation
    EXPORT MUQDepends
    LIBRARY DESTINATION "${CMAKE_INSTALL_PREFIX}/lib"
    ARCHIVE DESTINATION "${CMAKE_INSTALL_PREFIX}/lib")
	
