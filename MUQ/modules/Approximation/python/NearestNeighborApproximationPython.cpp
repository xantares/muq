#include "MUQ/Approximation/python/NearestNeighborApproximationPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

NearestNeighborApproximation::RefinementStatus NearestNeighborApproximationPython::PyRefineNear(py::list const& input)
{
  return RefineNear(GetEigenVector<Eigen::VectorXd>(input));
}

NearestNeighborApproximation::RefinementStatus NearestNeighborApproximationPython::PyRefineWithPoint(
  py::list const& input)
{
  return RefineWithPoint(GetEigenVector<Eigen::VectorXd>(input));
}

NearestNeighborApproximationPython::NearestNeighborApproximationPython(shared_ptr<ModPiece> model,
                                                                       py::dict const     & dict) :
  NearestNeighborApproximation(model, PythonDictToPtree(dict)) {}

void muq::Approximation::ExportNearestNeighborApproximation()
{
  py::enum_<NearestNeighborApproximation::Modes> modes("NearestNeighborApproximationMode");

  modes.value("standard", NearestNeighborApproximation::standard);
  modes.value("crossValidation", NearestNeighborApproximation::crossValidation);
  modes.value("trueDirect", NearestNeighborApproximation::trueDirect);

  py::enum_<NearestNeighborApproximation::RefinementStatus> status("NearestNeighborApproximationStatus");

  status.value("success", NearestNeighborApproximation::success);
  status.value("successNoContinue", NearestNeighborApproximation::successNoContinue);
  status.value("failure", NearestNeighborApproximation::failure);

  py::class_<NearestNeighborApproximationPython,
             shared_ptr<NearestNeighborApproximationPython>,
             py::bases<ModPiece>,
             boost::noncopyable> exportNNA("NearestNeighborApproximation",
                                           py::init<shared_ptr<ModPiece>, py::dict const&>());

  exportNNA.def("SetMode", &NearestNeighborApproximation::SetMode);
  exportNNA.def("GetNumberOfCrossValidationFits", &NearestNeighborApproximation::GetNumberOfCrossValidationFits);
  exportNNA.def("SetCrossValidationIndex", &NearestNeighborApproximation::SetCrossValidationIndex);
  exportNNA.def("RefineNear", &NearestNeighborApproximationPython::PyRefineNear);
  exportNNA.def("RefineWithPoint", &NearestNeighborApproximationPython::PyRefineWithPoint);
  exportNNA.def("GetCachedModPiece", &NearestNeighborApproximation::GetCachedModPiece);

  py::implicitly_convertible<shared_ptr<NearestNeighborApproximationPython>, shared_ptr<ModPiece> >();
}

