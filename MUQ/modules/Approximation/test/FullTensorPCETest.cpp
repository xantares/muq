#include <boost/property_tree/ptree.hpp> // needed here to avoid weird osx "toupper" bug when compiling MUQ with python

#include <gtest/gtest.h>

#include "MUQ/Utilities/EigenUtils.h"
#include "MUQ/Utilities/EigenTestUtils.h"

#include "MUQ/Utilities/Quadrature/ExpGrowthQuadratureFamily1D.h"
#include "MUQ/Utilities/Quadrature/GaussLegendreQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussHermiteQuadrature1D.h"
#include "MUQ/Utilities/Quadrature/GaussPattersonQuadrature1D.h"
#include "MUQ/Utilities/QuadratureTestFunctions.h"
#include <MUQ/Utilities/Quadrature/ClenshawCurtisQuadrature1D.h>
#include "MUQ/Utilities/VariableCollection.h"
#include <MUQ/Utilities/Polynomials/LegendrePolynomials1DRecursive.h>
#include <MUQ/Utilities/Polynomials/HermitePolynomials1DRecursive.h>
#include "MUQ/Modelling/CachedModPiece.h"
#include "MUQ/Modelling/ModPieceTemplates.h"

#include "MUQ/Approximation/smolyak/SmolyakQuadrature.h"
#include <MUQ/Approximation/pce/SingleQuadraturePCEFactory.h>

using namespace std;
using namespace Eigen;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;

VectorXd Legendre15(VectorXd const& input)
{
  VectorXd output(1);

  LegendrePolynomials1DRecursive LegendrePolyR;

  output <<  LegendrePolyR.evaluate(15, input(0));

  return output;
}

VectorXd Hermite13(VectorXd const& input)
{
  VectorXd output(1);

  HermitePolynomials1DRecursive HermitePolyR;

  output <<  HermitePolyR.evaluate(13, input(0));

  return output;
}

TEST(PolynomialChaos, FullTensorPCE_legendre_basis_recovery)
{
  GaussLegendreQuadrature1D::Ptr legendreQuad1D(new GaussLegendreQuadrature1D());

  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(Legendre15, 1, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SingleQuadraturePCEFactory singleFactory(varCollection);

  auto pce = singleFactory.ConstructPCE(fn, RowVectorXu::Constant(1, 18), RowVectorXu::Zero(1));


  VectorXd point1(1);
  VectorXd point2(1);
  VectorXd point3(1);
  VectorXd point4(1);

  //two arbitrary points to test
  point1 << .9;       //, .22;
  point2 << -.3;      //, .5;
  point3 << .58;      //, .7;
  point4 << -.192747; // , -.4;

  //check the values against stored ones that came from this code - regression
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point1), pce->Evaluate(point1), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point2), pce->Evaluate(point2), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point3), pce->Evaluate(point3), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point4), pce->Evaluate(point4), 1e-12);
}

TEST(PolynomialChaos, FullTensorPCE_legendre_basis_recovery_patterson)
{
  auto legendreQuad1D = make_shared<GaussPattersonQuadrature1D>();

  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(Legendre15, 1, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SingleQuadraturePCEFactory singleFactory(varCollection);

  auto pce = singleFactory.ConstructPCE(fn, RowVectorXu::Constant(1, 6), RowVectorXu::Zero(1));


  VectorXd point1(1);
  VectorXd point2(1);
  VectorXd point3(1);
  VectorXd point4(1);

  //two arbitrary points to test
  point1 << .9;       //, .22;
  point2 << -.3;      //, .5;
  point3 << .58;      //, .7;
  point4 << -.192747; // , -.4;

  //check the values against stored ones that came from this code - regression
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point1), pce->Evaluate(point1), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point2), pce->Evaluate(point2), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point3), pce->Evaluate(point3), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point4), pce->Evaluate(point4), 1e-12);
}


TEST(PolynomialChaos, FullTensorPCE_legendre_basis_recovery_clenshaw)
{
  auto legendreQuad1D = make_shared<ClenshawCurtisQuadrature1D>();

  LegendrePolynomials1DRecursive::Ptr legendrePoly1D(new LegendrePolynomials1DRecursive());

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", legendrePoly1D, legendreQuad1D);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(Legendre15, 1, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SingleQuadraturePCEFactory singleFactory(varCollection);

  auto pce = singleFactory.ConstructPCE(fn, RowVectorXu::Constant(1, 8), RowVectorXu::Zero(1));


  VectorXd point1(1);
  VectorXd point2(1);
  VectorXd point3(1);
  VectorXd point4(1);

  //two arbitrary points to test
  point1 << .9;       //, .22;
  point2 << -.3;      //, .5;
  point3 << .58;      //, .7;
  point4 << -.192747; // , -.4;

  //check the values against stored ones that came from this code - regression
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point1), pce->Evaluate(point1), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point2), pce->Evaluate(point2), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point3), pce->Evaluate(point3), 1e-12);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point4), pce->Evaluate(point4), 1e-12);
}

TEST(PolynomialChaos, FullTensorPCE_hermite_basis_recovery)
{
  auto hermiteQuad = make_shared<GaussHermiteQuadrature1D>();
  auto hermitePoly = make_shared<HermitePolynomials1DRecursive>();

  VariableCollection::Ptr varCollection(new VariableCollection());

  //Try a mixed integration type
  varCollection->PushVariable("x", hermitePoly, hermiteQuad);

  auto rawFn = make_shared<WrapVectorFunctionModPiece>(Hermite13, 1, 1);
  auto fn    = make_shared<CachedModPiece>(rawFn);

  SingleQuadraturePCEFactory singleFactory(varCollection);

  auto pce = singleFactory.ConstructPCE(fn, RowVectorXu::Constant(1, 18), RowVectorXu::Zero(1));


  VectorXd point1(1);
  VectorXd point2(1);
  VectorXd point3(1);
  VectorXd point4(1);

  //two arbitrary points to test
  point1 << .9;       //, .22;
  point2 << -.3;      //, .5;
  point3 << .58;      //, .7;
  point4 << -.192747; // , -.4;

  //check the values against stored ones that came from this code - regression
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point1), pce->Evaluate(point1), 2);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point2), pce->Evaluate(point2), 2);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point3), pce->Evaluate(point3), 2);
  EXPECT_PRED_FORMAT3(MatrixApproxEqual, fn->Evaluate(point4), pce->Evaluate(point4), 2);
}
