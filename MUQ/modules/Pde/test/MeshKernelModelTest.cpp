
#include "gtest/gtest.h"

#include "MUQ/Geostatistics/PowerKernel.h"

#include "MUQ/Pde/MeshKernelModel.h"

using namespace std;
using namespace muq::Geostatistics;
using namespace muq::Pde;

class MeshKernelModelTest : public::testing::Test {
public:

  MeshKernelModelTest()
  {
    param.put("mesh.type", "SquareMesh");
    param.put("mesh.Nx", 13);
    param.put("mesh.Ny", 8);
    param.put("mesh.Lx", 1.0);
    param.put("mesh.Ly", 1.0);

    param.put("MeshKernelModel.Name", "paraName");
    param.put("MeshKernelModel.KLModes", 100);

    double L   = 0.2; // length scale
    double P   = 2.0; // kernel power
    double sig = 1.0; // variance

    // create the kernel
    kernel = make_shared<PowerKernel>(L, P, sig);

    system = make_shared<GenericEquationSystems>(param);
  }

  void TearDown()
  {
    EXPECT_EQ(model->inputSizes.size(), 1);
    EXPECT_EQ(model->inputSizes[0],     param.get<unsigned int>("MeshKernelModel.KLModes", 0));
  }

  boost::property_tree::ptree param;

  shared_ptr<PowerKernel> kernel;

  shared_ptr<GenericEquationSystems> system;

  shared_ptr<MeshKernelModel> model;
};

TEST_F(MeshKernelModelTest, LinearShapeFunctionTest)
{
  param.put("MeshKernelModel.Order", 1);
  model = MeshKernelModel::Construct(system, kernel, param);
  EXPECT_EQ(model->outputSize, 126);
}

TEST_F(MeshKernelModelTest, QuadraticShapeFunctionTest)
{
  param.put("MeshKernelModel.Order", 2);
  model = MeshKernelModel::Construct(system, kernel, param);

  Eigen::VectorXd input = Eigen::VectorXd::Zero(model->inputSizes[0]);

  input[1] = 1.0;

  Eigen::VectorXd result = model->Evaluate(input);

  system->PrintToExodus("results/tests/MeshKernelModelBasis.exo");

  EXPECT_EQ(model->outputSize, 459);
}
