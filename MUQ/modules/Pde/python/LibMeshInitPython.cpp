#include <boost/property_tree/ptree.hpp> // need to avoid weird toupper bug on osx

#include "MUQ/Pde/python/LibMeshInitPython.h"

#ifdef MUQ_MPI
#include "mpi.h"
#endif

using namespace std;
namespace py = boost::python;
using namespace muq::Pde;

LibMeshInit::LibMeshInit(py::list const& pyArgv)
{
  if( !libmeshInit ) {
    int argc = py::len(pyArgv);
    const char **argv = new const char*[argc];
    
    for (int i = 0; i < argc; ++i) {
      string val = py::extract<string>(pyArgv[i]);
      argv[i] = val.c_str();
    }

#ifdef MUQ_MPI
    libmeshInit = make_shared<libMesh::LibMeshInit>(argc, argv, MPI_COMM_SELF);
#else 
     libmeshInit = make_shared<libMesh::LibMeshInit>(argc, argv);
#endif


    assert(LibMeshInit::libmeshInit);
  }
}

void muq::Pde::ExportLibMeshInit()
{
  py::class_<LibMeshInit, shared_ptr<LibMeshInit>, boost::noncopyable> exportInit("LibMeshInit", py::init<py::list const&>());
}
