import unittest

import libmuqGeostatistics

class CovKernelTest(unittest.TestCase):
    def testPowerKernelCreation(self):
        dim = 10
        dx  = 1.0/(dim-1.0)
        dy  = 1.0/(dim-1.0)

        # length scale
        L = 0.7

        # power 
        P = 1.5 

        # variance
        sig = 0.25

        # use different constructors
        testKern0 = libmuqGeostatistics.PowerKernel()
        testKern1 = libmuqGeostatistics.PowerKernel(L)
        testKern2 = libmuqGeostatistics.PowerKernel(L, P)
        testKern3 = libmuqGeostatistics.PowerKernel(L, P, sig)

        # make a dummy set of points
        x = [None]*(dim*dim)
        for i in range(dim):
            for j in range(dim):
                x[i+j*dim] = [i*dx, j*dy]

        # make covariance
        covMat0 = testKern0.BuildCov(x)
        covMat1 = testKern1.BuildCov(x)
        covMat2 = testKern2.BuildCov(x)
        covMat3 = testKern3.BuildCov(x)

        for i in range(dim*dim):
            self.assertAlmostEqual(covMat0[i][i], 1.0, 9)
            self.assertAlmostEqual(covMat1[i][i], 1.0, 9)
            self.assertAlmostEqual(covMat2[i][i], 1.0, 9)
            self.assertAlmostEqual(covMat3[i][i], sig, 9)

