
#include "MUQ/Geostatistics/CompactPolynomialKernel.h"


#include <assert.h>
#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/LogConfig.h"

using namespace muq::Geostatistics;

REGISTER_KERNEL_DEF_TYPE(CompactPolynomialKernel)
/** Construct the kernel from the characteristic length scale, power, and variance.
 */
CompactPolynomialKernel::CompactPolynomialKernel(boost::property_tree::ptree& ptree) : IsotropicCovKernel(1), logVar(0)
{
  LOG(INFO) << "Creating CompactPolynomialKernel";

  logVar = log(ptree.get("Kernel.Variance", exp(logVar)));
  LOG(INFO) << "with Kernel.Variance" << exp(logVar);
}

CompactPolynomialKernel::CompactPolynomialKernel(double VarIn) : IsotropicCovKernel(1), logVar(log(VarIn)) {}

/** This function evaluates the power kernel.
 */
double CompactPolynomialKernel::DistKernel(double d) const
{
  if (d >= 1.0) { //check for outside compact kernel
    return 0;
  } else {
    double j      = floor(dimIn / 2) + 3;
    double result = exp(logVar) *
                    pow(1.0 - d, j + 2) * ((j * j + 4.0 * j + 3.0) * d * d + (3.0 * j + 6.0) * d + 3.0) / 3.0;
    assert(!std::isnan(result)); //can underflow during optimization
    // evaluate the kernel
    return result;
  }
}

/** Set the covariance kernel parameters (Length, power, variance) from a vector of parameters.  */
void CompactPolynomialKernel::SetParms(const Eigen::VectorXd& theta)
{
  // check the vector length
    assert(theta.size() == 1);

  logVar = theta[0];
}

/** Compute the gradient of the covariance kernel wrt the gradient. */
void CompactPolynomialKernel::GetGrad(double d, Eigen::VectorXd& Grad) const
{
  Grad.resize(1);

  //grad is just itself for the variance parameter
  Grad(0) = DistKernel(d);
}

double CompactPolynomialKernel::KernelRadialDerivative(double const r)
{
  if (r >= 1.0) { //check for outside compact kernel
    return 0;
  } else {
    double j = floor(dimIn / 2) + 3;

    //differentiated by matlab


    double result = exp(logVar) *  pow(1.0 - r, j + 1) * (r * (j + 3.0) * (j + 4.0) * (r + j * r + 1.0)) / (-3.0);
    assert(!std::isnan(result)); //can underflow during optimization
    // evaluate the kernel
    return result;
  }
}
