
#include "MUQ/Geostatistics/SphericalKernel.h"

#include <assert.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/math/constants/constants.hpp>

using namespace muq::Geostatistics;


/** Construct the kernel from the characteristic length scale and variance.
 */
SphericalKernel::SphericalKernel(double LengthIn, double VarIn) : IsotropicCovKernel(2), Length(LengthIn), Var(VarIn) {}

/** This function evaluates the spherical kernel.
 */
double SphericalKernel::DistKernel(double d) const
{
  const double PI = boost::math::constants::pi<double>();

  if (d < Length) {
    return Var * (1 - (2 / PI) * (d / Length * sqrt(1 - pow(d / Length, 2)) + asin(d / Length)));
  } else {
    return 0;
  }
}

/** Set the covariance kernel parameters from a vector of parameters.  */
void SphericalKernel::SetParms(const Eigen::VectorXd& theta)
{
  // check the dimension
  assert(theta.size() == 2);

  // set the parameters
  Length = theta[0];
  Var    = theta[1];
}

/** Compute the gradient of the covariance kernel wrt the gradient. */
void SphericalKernel::GetGrad(double d, Eigen::VectorXd& Grad) const
{
  const double PI = boost::math::constants::pi<double>();

  Grad.resize(2);

  if (d >= Length) {
    Grad = Eigen::VectorXd::Zero(2);
  } else {
    Grad(0) = (4 * Var * d * (pow(Length, 2) - d * d)) / (pow(Length, 4) * PI * sqrt(1 - d * d / pow(Length, 2))); //
                                                                                                                   //
                                                                                                                   //
                                                                                                                   // wrt
                                                                                                                   //
                                                                                                                   //
                                                                                                                   // Length
    Grad(1) = 1 - (2 / PI) * (d / Length * sqrt(1 - pow(d / Length, 2)) + asin(d / Length));                       //wrt
                                                                                                                   //
                                                                                                                   //
                                                                                                                   // Var
  }
}
