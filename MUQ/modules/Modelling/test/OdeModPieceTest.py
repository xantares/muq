import unittest
import numpy as np # math stuff in python
import random

from libmuqModelling import *


# jacobian only
class OdeRhs(ModPiece):
    def __init__(self,aIn,bIn,cIn,dIn):
        ModPiece.__init__(self,[1,2],2, 0, 1, 0, 0, 0)
        self.a = aIn
        self.b = bIn
        self.c = cIn
        self.d = dIn
    
    def EvaluateImpl(self,inputs):
        
        time = inputs[0][0]
        
        dpdt = [0.0]*2
        
        dpdt[0] = self.a*inputs[1][0] - self.b*inputs[1][0]*inputs[1][1]
        dpdt[1] = self.c*inputs[1][0]*inputs[1][1] - self.d*inputs[1][1];
        return dpdt

    def JacobianImpl(self,inputs,inputDimWrt):
        
        time = inputs[0][0]
        
        jac = [[0.0]*2 for i in range(2)]
        jac[0][0] = self.a-self.b*inputs[1][1];
        jac[0][1] = -self.b*inputs[1][0];
        jac[1][0] = self.c*inputs[1][1];
        jac[1][1] = self.c*inputs[1][0]-self.d;
    
        return jac
        
# test the model implementation
class OdeModPieceTest(unittest.TestCase):
    preyGrowth = 1.5;
    preyDeath  = 0.05;
    predGrowth = 0.01;
    predDeath  = 0.5;
    obsTimes = np.linspace(0,10,20);
    p0 = [80,20]
    
    def testEvaluate(self):
        rhs = OdeRhs(self.preyGrowth, self.preyDeath, self.predGrowth, self.predDeath)
        odeSys = OdeModPiece(rhs,self.obsTimes.tolist());
        
        allPops = odeSys.Evaluate([self.p0])
        
        self.assertAlmostEqual(allPops[-1], 46.5598, 4)
        self.assertAlmostEqual(allPops[-2], 43.4105, 4)
        
    def testGradient(self):
        rhs = OdeRhs(self.preyGrowth, self.preyDeath, self.predGrowth, self.predDeath)
        odeSys = OdeModPiece(rhs,self.obsTimes.tolist());
        
        sens = [1.0]*40
        grad = odeSys.Gradient([self.p0],sens,0)
        
        self.assertAlmostEqual(grad[0], 1.8949998798505618, 5)
        self.assertAlmostEqual(grad[1], -15.128676355440621, 5)
        
    def testJacobian(self):
        rhs = OdeRhs(self.preyGrowth, self.preyDeath, self.predGrowth, self.predDeath)
        odeSys = OdeModPiece(rhs,self.obsTimes.tolist());
        
        jac = odeSys.Jacobian([self.p0],0)
        
        self.assertAlmostEqual(jac[-1][0], 0.15967943703986637, 5)
        self.assertAlmostEqual(jac[-1][1], -1.5784216938000561, 5)
        self.assertAlmostEqual(jac[-2][0], -0.5994834925912651, 5)
        self.assertAlmostEqual(jac[-2][1], -1.9877458974589473, 5)
        
    def testJacobianAction(self):
        rhs = OdeRhs(self.preyGrowth, self.preyDeath, self.predGrowth, self.predDeath)
        odeSys = OdeModPiece(rhs,self.obsTimes.tolist());
        
        x = [0.5,0.5]
        jacAct = odeSys.JacobianAction([self.p0],x,0)
        
        self.assertAlmostEqual(jacAct[-1], -0.7093711283800949, 5)
        self.assertAlmostEqual(jacAct[-2], -1.2936146950251062, 5)