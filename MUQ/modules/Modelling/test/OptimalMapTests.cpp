
#include "gtest/gtest.h"

#include <Eigen/Dense>


#include "MUQ/Modelling/OptimalMaps/DiscreteOptimalMap.h"
#include "MUQ/Modelling/OptimalMaps/TriangularDiscreteMap.h"
#include "MUQ/Modelling/OptimalMaps/GprOptimalMap.h"

#include "MUQ/Modelling/Model.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Modelling/Auctioneer.h"

using namespace muq::Modelling;
using namespace muq::utilities;
using namespace std;


TEST(ModellingAuctioneerTest, BasicAuctionTest)
{
  unsigned int Npts = 1000;
  Eigen::MatrixXd Inputs(1, Npts);
  Eigen::MatrixXd Outputs(1, Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    Inputs(0, i)  = i;
    Outputs(0, i) = Npts - i - 1;
  }

  Auctioneer<float> solver(Inputs, Outputs);

  solver.solve();

  std::vector<unsigned int> asses = solver.GetSolution();

  for (unsigned int i = 0; i < Npts; ++i) {
    EXPECT_EQ(Npts - i - 1, asses[i]);
  }
}


TEST(ModellingDiscreteOptimalMapTest, BasicMap)
{
  unsigned int Npts = 3;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(2, Npts);

  xi << 0.0, 0.5, 1.0, 0.0, 0.5, 1.0;

  Eigen::MatrixXd xo(2, Npts);
  xo << 0.4, 0.2, 1.2, 0.51, 0.01, 1.2;

  Model testMap = make_shared<DiscreteOptimalMap>(xi, xo, 2, "Legendre");

  Eigen::VectorXd testIn = Eigen::VectorXd::Ones(2);
  testMap.Update(testIn);

  Eigen::VectorXd testOut(2);
  testMap.Eval(testOut);

  EXPECT_NEAR(1.2, testOut[0], 1e-3);
  EXPECT_NEAR(1.2, testOut[1], 1e-3);
}


TEST(ModellingTriangularMapTest, BasicMap)
{
  unsigned int Npts = 3;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(2, Npts);

  xi << 0.0, 0.5, 1.0, 0.0, 0.5, 1.0;

  Eigen::MatrixXd xo(2, Npts);
  xo << 0.4, 0.2, 1.2, 0.51, 0.01, 1.2;
  Model testMap = make_shared<TriangularDiscreteMap>(xi, xo, 2, "Legendre");

  Eigen::VectorXd testIn = Eigen::VectorXd::Ones(2);
  testMap.Update(testIn);

  Eigen::VectorXd testOut(2);
  testMap.Eval(testOut);

  EXPECT_NEAR(1.2, testOut[0], 1e-3);
  EXPECT_NEAR(1.2, testOut[1], 1e-3);
}


TEST(ModellingDiscreteOptimalMapTest, DifferentDimensionMap)
{
  unsigned int Npts = 3;

  // first, creat a bunch of points in two dimensions
  Eigen::MatrixXd xi(2, Npts);

  xi.setRandom();


  Eigen::MatrixXd xo(4, Npts);
  xo.setRandom();

  Model testMap = make_shared<DiscreteOptimalMap>(xi, xo, 2, "Legendre", true);

  Eigen::VectorXd testIn = xi.col(0);
  testMap.Update(testIn);

  Eigen::VectorXd testOut(2);
  testMap.Eval(testOut);

  EXPECT_NEAR(xo(0, 0), testOut[0], 1e-3);
  EXPECT_NEAR(xo(1, 0), testOut[1], 1e-3);
}


TEST(ModellingDiscreteOptimalMapTest, GradientTest)
{
  unsigned int Npts = 200;

  // first, creat a bunch of iid gaussian points in two dimensions
  Eigen::MatrixXd xi(2, Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    xi(0, i) = RandomGenerator::GetNormal();
    xi(1, i) = RandomGenerator::GetNormal();
  }


  // exponentiate the samples
  Eigen::MatrixXd xo = xi.array().exp();

  auto testMap = make_shared<DiscreteOptimalMap>(xi, xo, 5, "Hermite");

  Eigen::VectorXd Loc = Eigen::VectorXd::Ones(2);
  Loc << 1, 0.1;
  Eigen::VectorXd Sens = Eigen::VectorXd::Ones(2);
  Eigen::VectorXd deriv(2);
  Eigen::VectorXd fdderiv(2);

  testMap->UpdateBase(Loc);
  testMap->Grad(Loc, Sens, deriv, 0);
  testMap->Grad(Loc, Sens, fdderiv, 0, 1e-6, 1e-6);

  EXPECT_NEAR(fdderiv[0], deriv[0], 1e-5);
  EXPECT_NEAR(fdderiv[1], deriv[1], 1e-5);
}

TEST(ModellingDiscreteOptimalMapTest, ExpMap)
{
  unsigned int Npts = 700;

  RandomGenerator rng;

  // first, creat a bunch of iid gaussian points in two dimensions
  Eigen::MatrixXd xi(1, Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    xi(0, i) = rng.GetNormal();
  }


  // exponentiate the samples
  Eigen::MatrixXd xo = xi.array().exp();

  Model testMap = make_shared<DiscreteOptimalMap>(xi, xo, 5, "Hermite");

  // we will compare the means
  Eigen::VectorXd testMean = Eigen::VectorXd::Zero(1);

  Eigen::VectorXd testIn(1);
  Eigen::VectorXd testOut(1);

  for (unsigned int i = 0; i < Npts; ++i) {
    testIn(0) = rng.GetNormal();

    testMap.Update(testIn);
    testMap.Eval(testOut);

    testMean += testOut;
  }
  testMean /= Npts;

  EXPECT_NEAR(exp(0 + 0.5), testMean[0], 2e-1);
}


TEST(ModellingDiscreteOptimalMapTest, ExpGprMap)
{
  unsigned int Npts = 20;

  RandomGenerator rng;

  // first, creat a bunch of iid gaussian points in two dimensions
  Eigen::MatrixXd xi(1, Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    xi(0, i) = rng.GetNormal();
  }


  // exponentiate the samples
  Eigen::MatrixXd xo = xi.array().exp();

  Model testMap = make_shared<GprOptimalMap>(xi, xo);

  // we will compare the means
  Eigen::VectorXd testMean = Eigen::VectorXd::Zero(1);

  Eigen::VectorXd testIn(1);
  Eigen::VectorXd testOut(1);

  for (unsigned int i = 0; i < 1000; ++i) {
    testIn(0) = rng.GetNormal();

    testMap.Update(testIn);
    testMap.Eval(testOut);

    testMean += testOut;

    //std::cout << testOut[0] << "  " << testIn[0] << std::endl;
  }
  testMean /= 1000;


  EXPECT_NEAR(exp(0 + 0.5), testMean[0], 2e-1);
}


TEST(ModellingTriangularMapTest, ExpMap)
{
  unsigned int Npts = 700;

  RandomGenerator rng;

  // first, creat a bunch of iid gaussian points in two dimensions
  Eigen::MatrixXd xi(1, Npts);

  for (unsigned int i = 0; i < Npts; ++i) {
    xi(0, i) = rng.GetNormal();
  }


  // exponentiate the samples
  Eigen::MatrixXd xo = xi.array().exp();

  Model testMap = make_shared<TriangularDiscreteMap>(xi, xo, 5, "Hermite");

  // we will compare the means
  Eigen::VectorXd testMean = Eigen::VectorXd::Zero(1);

  Eigen::VectorXd testIn(1);
  Eigen::VectorXd testOut(1);

  for (unsigned int i = 0; i < Npts; ++i) {
    testIn(0) = rng.GetNormal();

    testMap.Update(testIn);
    testMap.Eval(testOut);

    testMean += testOut;
  }
  testMean /= Npts;

  EXPECT_NEAR(exp(0 + 0.5), testMean[0], 2e-1);
}