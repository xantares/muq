#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp> // this include is weird, but must be here to avoid a strange bug on osx

#include <boost/math/constants/constants.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Modelling/GaussianRV.h"
#include "MUQ/Modelling/ModParameter.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

TEST(GaussianTest, IsotropicDensity)
{
  const unsigned int N = 10;
  
  
  auto dens = make_shared<GaussianDensity>(N);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const double eval = dens->LogDensity(in);

  //-0.5 * (N * log(2.0*boost::math::constants::pi<double>()) + in.dot(in));
  EXPECT_NEAR(-0.5 * (N * log(2.0*boost::math::constants::pi<double>()) + in.dot(in)), eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 * in, grad, 1e-12);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 * in.transpose(), jac, 1e-12);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 * in.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);

  const Eigen::MatrixXd hess = dens->Hessian(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 * Eigen::MatrixXd::Identity(N, N), hess, 1e-12);
}

  TEST(GaussianTest, ScaledIsotropicDensity)
{
  const unsigned int N = 10;
  const double cov     = 0.25;

  auto dens = make_shared<GaussianDensity>(N, cov);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const double eval = dens->LogDensity(in);

  EXPECT_NEAR(-0.5 * (N * log(2.0*boost::math::constants::pi<double>()) + N*log(cov) + in.dot(in) / cov), eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * in, grad, 1e-12);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * in.transpose(), jac, 1e-12);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * in.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);

  const Eigen::MatrixXd hess = dens->Hessian(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * Eigen::MatrixXd::Identity(N, N), hess, 1e-12);
}

  TEST(GaussianTest, ScaledIsotropic_NonzeroMeanDensity)
{
  const unsigned int N     = 10;
  const Eigen::VectorXd mu = Eigen::VectorXd::Random(N);
  const double cov         = 0.25;

  auto dens = make_shared<GaussianDensity>(mu, cov);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const Eigen::VectorXd delta = in - mu;

  const double eval = dens->LogDensity(in);

  EXPECT_NEAR(-0.5 * (N * log(2.0*boost::math::constants::pi<double>()) + N * log(cov) + delta.dot(delta) / cov), eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * delta, grad, 1e-12);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * delta.transpose(), jac, 1e-12);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * delta.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);

  const Eigen::MatrixXd hess = dens->Hessian(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 / cov * Eigen::MatrixXd::Identity(N, N), hess, 1e-12);
}

  TEST(GaussianTest, DiagonalCovariance)
{
  const unsigned int N          = 10;
  const Eigen::VectorXd mu      = Eigen::VectorXd::Random(N);
  const Eigen::VectorXd covDiag = 1e-2 * Eigen::VectorXd::Ones(N) + RandomGenerator::GetUniformRandomVector(N);

  auto dens = make_shared<GaussianDensity>(mu, covDiag);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const Eigen::VectorXd delta = in - mu;

  const double eval = dens->LogDensity(in);

  const double truth = -0.5 * (N * log(2.0*boost::math::constants::pi<double>()) + covDiag.array().log().sum() + delta.dot((1.0 / covDiag.array()).matrix().asDiagonal() * delta));

  EXPECT_NEAR(truth, eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  const Eigen::VectorXd gradTrue = (-1.0 / covDiag.array()).matrix().asDiagonal() * delta;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue, grad, 1e-12);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.transpose(), jac, 1e-12);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);

  const Eigen::MatrixXd hess = dens->Hessian(in, Eigen::VectorXd::Ones(1), 0);

  const Eigen::MatrixXd hessTrue = (-1.0 / covDiag.array()).matrix().asDiagonal();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, hessTrue, hess, 1e-12);
}

  TEST(GaussianTest, DiagonalCovarianceSample)
{
  const unsigned int Nsamps = 1e5;

  const unsigned int N          = 10;
  const Eigen::VectorXd mu      = Eigen::VectorXd::Random(N);
  const Eigen::VectorXd covDiag = 1e-2 * Eigen::VectorXd::Ones(N) + RandomGenerator::GetUniformRandomVector(N);

  auto rv = make_shared<GaussianRV>(mu, covDiag);

  Eigen::MatrixXd samps = rv->Sample(Nsamps);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, mu, samps.rowwise().sum() / (double)Nsamps, 5.0e-2);
}

TEST(GaussianTest, DiagonalPrecision)
{
  const unsigned int N           = 10;
  const Eigen::VectorXd mu       = Eigen::VectorXd::Random(N);
  const Eigen::VectorXd precDiag = 1e-2 * Eigen::VectorXd::Ones(N) + RandomGenerator::GetUniformRandomVector(N);

  auto dens = make_shared<GaussianDensity>(mu, precDiag, GaussianSpecification::DiagonalPrecision);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const Eigen::VectorXd delta = in - mu;

  const double eval = dens->LogDensity(in);

  const double truth = -0.5 * (N * log(2.0*boost::math::constants::pi<double>()) - precDiag.array().log().sum() + delta.dot(precDiag.asDiagonal() * delta));

  EXPECT_NEAR(truth, eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  const Eigen::VectorXd gradTrue = -1.0 * precDiag.asDiagonal() * delta;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue, grad, 1e-12);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.transpose(), jac, 1e-12);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);

  const Eigen::MatrixXd hess = dens->Hessian(in, Eigen::VectorXd::Ones(1), 0);

  const Eigen::MatrixXd hessTrue = -1.0 * precDiag.asDiagonal();

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, hessTrue, hess, 1e-12);
}

  TEST(GaussianTest, FullCovariance)
{
  const unsigned int N      = 10;
  const Eigen::VectorXd mu  = Eigen::VectorXd::Random(N);
  const Eigen::MatrixXd L   = Eigen::MatrixXd::Random(N, N);
  const Eigen::MatrixXd cov = 1e-4 * Eigen::MatrixXd::Identity(N, N) + L *L.transpose();

  Eigen::LLT<Eigen::MatrixXd> CholSolver;
  CholSolver.compute(cov);
  Eigen::MatrixXd cholDecomp = CholSolver.matrixL();

  auto dens = make_shared<GaussianDensity>(mu, cov);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const Eigen::VectorXd delta = in - mu;

  Eigen::MatrixXd solved = cholDecomp.triangularView<Eigen::Lower>().solve(delta);
  cholDecomp.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);

  const double eval = dens->LogDensity(in);

  const double truth = -0.5 * (N * log(2.0*boost::math::constants::pi<double>()) + 2.0 * cholDecomp.diagonal().array().log().sum() + delta.dot(solved.col(0)));

  EXPECT_NEAR(truth, eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  const Eigen::VectorXd gradTrue = -1.0 * solved;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue, grad, 1e-12);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.transpose(), jac, 1e-12);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);
}

  TEST(GaussianTest, FullPrecision)
{
  const unsigned int N       = 10;
  const Eigen::VectorXd mu   = Eigen::VectorXd::Random(N);
  const Eigen::MatrixXd L    = Eigen::MatrixXd::Random(N, N);
  const Eigen::MatrixXd prec = 1e-4 * Eigen::MatrixXd::Identity(N, N) + L *L.transpose();

  Eigen::LLT<Eigen::MatrixXd> CholSolver;
  CholSolver.compute(prec);
  Eigen::MatrixXd cholDecomp = CholSolver.matrixL();

  auto dens = make_shared<GaussianDensity>(mu, prec, GaussianSpecification::PrecisionMatrix);

  const Eigen::VectorXd in = Eigen::VectorXd::Random(N);

  const Eigen::VectorXd delta = in - mu;

  const double eval = dens->LogDensity(in);

  const double truth = -0.5 * (N * log(2.0*boost::math::constants::pi<double>()) -2.0 * cholDecomp.diagonal().array().log().sum() + delta.dot(prec * delta));

  EXPECT_NEAR(truth, eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(in, Eigen::VectorXd::Ones(1), 0);

  const Eigen::VectorXd gradTrue = -1.0 * prec * delta;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue, grad, 1.0e-8);

  const Eigen::MatrixXd jac = dens->Jacobian(in, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.transpose(), jac, 1.0e-8);

  const Eigen::VectorXd jacAct = dens->JacobianAction(in, Eigen::VectorXd::Ones(N), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual,
                      gradTrue.dot(Eigen::VectorXd::Ones(N)) * Eigen::VectorXd::Ones(1), jacAct, 1.0e-8);

  const Eigen::MatrixXd hess = dens->Hessian(in, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 * prec, hess, 1e-12);
}

TEST(GaussianTest, ConditionalFullCovariance)
{
  const unsigned int N          = 10;
  const unsigned int Ncond      = 3;
  const Eigen::VectorXd mu      = Eigen::VectorXd::Random(N);
  const Eigen::MatrixXd L       = Eigen::MatrixXd::Random(N, N);
  const Eigen::MatrixXd fullCov = 1e-4 * Eigen::MatrixXd::Identity(N, N) + L *L.transpose();

  const Eigen::MatrixXd A = fullCov.topLeftCorner(Ncond, Ncond);
  const Eigen::MatrixXd B = fullCov.bottomRightCorner(N - Ncond, N - Ncond);
  const Eigen::MatrixXd C = fullCov.bottomLeftCorner(N - Ncond, Ncond);

  // compute conditional covariance
  Eigen::LLT<Eigen::MatrixXd> cholSolver;
  cholSolver.compute(B);
  const Eigen::MatrixXd LB = cholSolver.matrixL();

  Eigen::MatrixXd CBinv = LB.triangularView<Eigen::Lower>().solve(C);
  LB.triangularView<Eigen::Lower>().transpose().solveInPlace(CBinv);

  Eigen::MatrixXd cov = A - C.transpose() * CBinv;

  // Cholesky decomposition of the conditional covariance
  cholSolver.compute(cov);
  const Eigen::MatrixXd cholDecomp = cholSolver.matrixL();

  auto dens = make_shared<GaussianDensity>(Ncond, mu, fullCov);

  const Eigen::VectorXd   in = Eigen::VectorXd::Random(N);
  vector<Eigen::VectorXd> inputs(2);
  inputs[0] = in.head(Ncond);
  inputs[1] = in.tail(N - Ncond);

  // compute conditional mean
  Eigen::VectorXd condMean = LB.triangularView<Eigen::Lower>().solve(in.tail(N - Ncond) - mu.tail(N - Ncond));
  LB.triangularView<Eigen::Lower>().transpose().solveInPlace(condMean);

  condMean = mu.head(Ncond) + C.transpose() * condMean;

  const Eigen::VectorXd delta = in.head(Ncond) - condMean;

  Eigen::MatrixXd solved = cholDecomp.triangularView<Eigen::Lower>().solve(delta);
  cholDecomp.triangularView<Eigen::Lower>().transpose().solveInPlace(solved);

  const double eval = dens->LogDensity(inputs);

  const double truth = -0.5 * (condMean.size() * log(2.0*boost::math::constants::pi<double>()) + 2.0 * cholDecomp.diagonal().array().log().sum() + delta.dot(solved.col(0)));

  EXPECT_NEAR(truth, eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(inputs, Eigen::VectorXd::Ones(1), 0);

  const Eigen::VectorXd gradTrue = -1.0 * solved;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue, grad, 1.0e-8);

  const Eigen::MatrixXd jac = dens->Jacobian(inputs, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.transpose(), jac, 1.0e-8);

  const Eigen::VectorXd jacAct = dens->JacobianAction(inputs, Eigen::VectorXd::Ones(Ncond), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.dot(Eigen::VectorXd::Ones(Ncond)) * Eigen::VectorXd::Ones(1), jacAct, 1e-12);
}

TEST(GaussianTest, ConditionalFullPrecision)
{
  const unsigned int N           = 10;
  const unsigned int Ncond       = 3;
  const Eigen::VectorXd mu       = Eigen::VectorXd::Random(N);
  const Eigen::MatrixXd L        = Eigen::MatrixXd::Random(N, N);
  const Eigen::MatrixXd fullPrec = 1e-4 * Eigen::MatrixXd::Identity(N, N) + L *L.transpose();

  auto dens = make_shared<GaussianDensity>(Ncond, mu, fullPrec, GaussianSpecification::PrecisionMatrix);

  const Eigen::MatrixXd A = fullPrec.topLeftCorner(Ncond, Ncond);
  const Eigen::MatrixXd C = fullPrec.bottomLeftCorner(N - Ncond, Ncond);

  // compute conditional covariance
  Eigen::LLT<Eigen::MatrixXd> cholSolver;
  cholSolver.compute(A);
  const Eigen::MatrixXd LA = cholSolver.matrixL();

  Eigen::MatrixXd prec = A;
  cholSolver.compute(prec);
  const Eigen::MatrixXd cholDecomp = cholSolver.matrixL();

  const Eigen::VectorXd   in = Eigen::VectorXd::Random(N);
  vector<Eigen::VectorXd> inputs(2);
  inputs[0] = in.head(Ncond);
  inputs[1] = in.tail(N - Ncond);

  // compute conditional mean
  Eigen::VectorXd condMean =
    LA.triangularView<Eigen::Lower>().solve(C.transpose() * (in.tail(N - Ncond) - mu.tail(N - Ncond)));
  LA.triangularView<Eigen::Lower>().transpose().solveInPlace(condMean);

  condMean = mu.head(Ncond) + condMean;

  const Eigen::VectorXd delta = in.head(Ncond) - condMean;

  const double eval = dens->LogDensity(inputs);

  const double truth = -0.5 * (condMean.size() * log(2.0*boost::math::constants::pi<double>()) -2.0 * cholDecomp.diagonal().array().log().sum() + delta.dot(prec * delta));

  EXPECT_NEAR(truth, eval, 1e-12);

  const Eigen::VectorXd grad = dens->Gradient(inputs, Eigen::VectorXd::Ones(1), 0);

  const Eigen::VectorXd gradTrue = -1.0 * prec * delta;

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue, grad, 1.0e-8);

  const Eigen::MatrixXd jac = dens->Jacobian(inputs, 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, gradTrue.transpose(), jac, 1.0e-8);

  const Eigen::VectorXd jacAct = dens->JacobianAction(inputs, Eigen::VectorXd::Ones(Ncond), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual,
                      gradTrue.dot(Eigen::VectorXd::Ones(Ncond)) * Eigen::VectorXd::Ones(1),
                      jacAct, 1.0e-8);

  const Eigen::MatrixXd hess = dens->Hessian(inputs, Eigen::VectorXd::Ones(1), 0);

  EXPECT_PRED_FORMAT3(MatrixApproxEqual, -1.0 * prec, hess, 1e-12);
}

