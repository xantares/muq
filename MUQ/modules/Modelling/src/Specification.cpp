#include "MUQ/Modelling/Specification.h"

using namespace std;
using namespace muq::Modelling;

Specification::Specification(Eigen::VectorXi const dim, shared_ptr<ModGraph> const& graph,
                             vector<string> const& names) :
  dim(dim), names(names)
{
  map<string, unsigned int> indexMap;

  inputSizes = graph->inputSizes();

  unsigned int numInputs = 0;
  for (unsigned int i = 0; i < names.size(); ++i) {
    assert(graph->NodeExists(names[i]));

    // the inputs for this parameter
    vector<string> inNames = graph->InputNames(names[i]);

    // a model for this parameter
    auto mod = ModGraph::ConstructModPiece(graph, names[i], inNames);

    // the indices for this mod piece
    vector<unsigned int> indices(inNames.size());

    for (unsigned int j = 0; j < inNames.size(); ++j) {
      if (indexMap.insert(pair<string, unsigned int>(inNames[j], numInputs)).second) {
        ++numInputs;
      }

      indices[j] = indexMap[inNames[j]];
    }

    pair<shared_ptr<ModGraphPiece>, vector<unsigned int> > p(mod, indices);
    paraMods.insert(pair<string, pair<shared_ptr<ModGraphPiece>, vector<unsigned int> > >(names[i], p));
  }
}

Specification::Specification(Eigen::VectorXi const       dim,
                             shared_ptr<ModGraph> const& graph,
                             vector<string> const      & names,
                             Eigen::VectorXi const     & inputSizes) :
  inputSizes(inputSizes), dim(dim), names(names)
{
  map<string, unsigned int> indexMap;

    assert(graph->inputSizes().size() == 0);

  unsigned int numInputs = 0;
  for (unsigned int i = 0; i < names.size(); ++i) {
    assert(graph->NodeExists(names[i]));

    // the inputs for this parameter
    vector<string> inNames = graph->InputNames(names[i]);

    // a model for this parameter
    auto mod = ModGraph::ConstructModPiece(graph, names[i], inNames);

    // the indices for this mod piece
    vector<unsigned int> indices(inNames.size());

    for (unsigned int j = 0; j < inNames.size(); ++j) {
      if (indexMap.insert(pair<string, unsigned int>(inNames[j], numInputs)).second) {
        ++numInputs;
      }

      indices[j] = indexMap[inNames[j]];
    }

    pair<shared_ptr<ModGraphPiece>, vector<unsigned int> > p(mod, indices);
    paraMods.insert(pair<string, pair<shared_ptr<ModGraphPiece>, vector<unsigned int> > >(names[i], p));
  }
}

