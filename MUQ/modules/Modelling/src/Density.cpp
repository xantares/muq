
#include "MUQ/Modelling/Density.h"


using namespace muq::Modelling;

Density::Density(Eigen::VectorXi const& inputSizes,
                 bool const             hasDirectGradient,
                 bool const             hasDirectJacobian,
                 bool const             hasDirectJacobianAction,
                 bool const             hasDirectHessian) : ModPiece(inputSizes,
                                                                                      1,
                                                                                      hasDirectGradient,
                                                                                      hasDirectJacobian,
                                                                                      hasDirectJacobianAction,
                                                                                      hasDirectHessian,
                                                                                      false)
{}

