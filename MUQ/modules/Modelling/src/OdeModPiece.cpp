

#include "MUQ/Modelling/OdeModPiece.h"

#include <boost/property_tree/ptree.hpp>

#include <cvodes/cvodes.h>           /* main integrator header file */
#include <cvodes/cvodes_spgmr.h>     /* prototypes & constants for CVSPGMR solver */
#include <cvodes/cvodes_spbcgs.h>    /* prototypes & constants for CVSPBCG solver */
#include <cvodes/cvodes_sptfqmr.h>   /* prototypes & constants for SPTFQMR solver */
#include <cvodes/cvodes_dense.h>

#include <nvector/nvector_serial.h>  /* serial N_Vector types, fct. and macros */
#include <sundials/sundials_dense.h> /* use generic DENSE solver in preconditioning */
#include <sundials/sundials_types.h> /* definition of realtype */
#include <sundials/sundials_math.h>  /* contains the macros ABS, SQR, and EXP */

#include "MUQ/Modelling/IdentityObserver.h"


using namespace muq::Modelling;
using namespace std;

struct RhsData {
  std::shared_ptr<ModPiece>    f, g;
  std::vector<Eigen::VectorXd> inputs;
  int                          stateSize;
  int                          inputDimWrt;
  int                          paramSize;
};

static void sundialsErrorFunc(int error_code, const char *module, const char *function, char *msg, void *eh_data) {}

/* Functions Called by the CVODES Solver */
static int  sundialsFunc(realtype time, N_Vector state, N_Vector deriv, void *user_data)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvDerivMap(NV_DATA_S(deriv), fgdata->stateSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = nvStateMap;

  nvDerivMap = fgdata->f->Evaluate(fgdata->inputs);

  return 0;
}

static int sundialsJac(long int N,
                       realtype time,
                       N_Vector state,
                       N_Vector rhs,
                       DlsMat   jac,
                       void    *user_data,
                       N_Vector tmp1,
                       N_Vector tmp2,
                       N_Vector tmp3)
{
  RhsData *fgdata = (RhsData *)user_data;

      assert(N == fgdata->stateSize);

  Eigen::Map<Eigen::VectorXd> stateMap(NV_DATA_S(state), fgdata->stateSize);
      assert(jac->M == fgdata->stateSize);

  Eigen::Map < Eigen::MatrixXd, 0, Eigen::OuterStride < Eigen::Dynamic >> jacMap(jac->data,
                                                                                 jac->M,
                                                                                 jac->N,
                                                                                 Eigen::OuterStride<Eigen::Dynamic>(jac
                                                                                                                    ->
                                                                                                                    ldim));

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = stateMap;

  jacMap = fgdata->f->Jacobian(fgdata->inputs, 1);

  return 0;
}

static int sundialsJacApply(N_Vector v,
                            N_Vector Jv,
                            realtype time,
                            N_Vector y,
                            N_Vector fy,
                            void    *user_data,
                            N_Vector tmp)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> vMap(NV_DATA_S(v), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> JvMap(NV_DATA_S(Jv), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> yMap(NV_DATA_S(y), fgdata->stateSize);

  //Eigen::Map<Eigen::VectorXd> fyMap(NV_DATA_S(deriv),fgdata->stateSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = yMap;

  fgdata->f->Evaluate(fgdata->inputs);
  JvMap = fgdata->f->JacobianAction(fgdata->inputs, vMap, 1);

  return 0;
}

static int adjointRhs(realtype time, N_Vector state, N_Vector lambda, N_Vector deriv, void *user_data)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvLambdaMap(NV_DATA_S(lambda), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvDerivMap(NV_DATA_S(deriv), fgdata->stateSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = nvStateMap;

  nvDerivMap = -1.0 * fgdata->f->Gradient(fgdata->inputs, nvLambdaMap, 1);

  return 0;
}

static int adjointJacApply(N_Vector target,
                           N_Vector output,
                           realtype time,
                           N_Vector state,
                           N_Vector lambda,
                           N_Vector adjRhs,
                           void    *user_data,
                           N_Vector tmp)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvTargetMap(NV_DATA_S(target), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvOutputMap(NV_DATA_S(output), fgdata->stateSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = nvStateMap;

  nvOutputMap = -1.0 * fgdata->f->Gradient(fgdata->inputs, nvTargetMap, 1);

  return 0;
}

// NOT SURE IF THIS IS CORRECT TREATMENT OF DlsMat
static int adjointJacobian(long int N,
                           realtype time,
                           N_Vector state,
                           N_Vector lambda,
                           N_Vector rhs,
                           DlsMat   jac,
                           void    *user_data,
                           N_Vector tmp1,
                           N_Vector tmp2,
                           N_Vector tmp3)
{
  RhsData *fgdata = (RhsData *)user_data;

  // assert(N==fgdata->stateSize);

  Eigen::Map<Eigen::VectorXd> stateMap(NV_DATA_S(state), fgdata->stateSize);
  // assert(jac->M==fgdata->stateSize);

  Eigen::Map < Eigen::MatrixXd, 0, Eigen::OuterStride < Eigen::Dynamic >> jacMap(jac->data,
                                                                                 jac->M,
                                                                                 jac->N,
                                                                                 Eigen::OuterStride<Eigen::Dynamic>(jac
                                                                                                                    ->
                                                                                                                    ldim));

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = stateMap;

  jacMap = -1.0 * fgdata->f->Jacobian(fgdata->inputs, 1).transpose();

  return 0;
}

static int adjointQuad(realtype time, N_Vector state, N_Vector lambda, N_Vector quadRhs, void *user_data)
{
  RhsData *fgdata = (RhsData *)user_data;

  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvLambdaMap(NV_DATA_S(lambda), fgdata->stateSize);
  Eigen::Map<Eigen::VectorXd> nvQuadMap(NV_DATA_S(quadRhs), fgdata->paramSize);

  fgdata->inputs.at(0) = time * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = nvStateMap;

  nvQuadMap = fgdata->f->Gradient(fgdata->inputs, nvLambdaMap, fgdata->inputDimWrt + 1);
  return 0;
}

/** This function returns the rhs of the sensitivity equations:
 *  \f[
 *  \frac{ds_i}{dt} = \frac{df}{dy}s_i + \frac{df}{dp_i}
 *  \f]
 *  for each component (indexed by i) of the parameter.  Here, y is the state, s_i is the sensitivity variable for
 * parameter index i of the parameter.
 */
static int sundialsForwardSensRhs(int       Ns,
                                  realtype  time,
                                  N_Vector  y,
                                  N_Vector  ydot,
                                  N_Vector *ys,
                                  N_Vector *ySdot,
                                  void     *user_data,
                                  N_Vector  tmp1,
                                  N_Vector  tmp2)
{
  RhsData *fgdata = (RhsData *)user_data;

      assert(fgdata->f->inputSizes(fgdata->inputDimWrt + 1) == Ns);

  // first, obtain df/dy
  Eigen::MatrixXd dfdy = fgdata->f->Jacobian(fgdata->inputs, 1);                       // 0 input is time, 1 input is
                                                                                       // the state

  // now, obtain df/dp
  Eigen::MatrixXd dfdp = fgdata->f->Jacobian(fgdata->inputs, fgdata->inputDimWrt + 1); // inputDimWrt does not account
                                                                                       // for time, so we add 1

  // now, loop through and fill in the rhs vectors stored in ySdot
  for (int i = 0; i < Ns; ++i) {
    Eigen::Map<Eigen::VectorXd> rhsView(NV_DATA_S(ySdot[i]), fgdata->stateSize);
    Eigen::Map<Eigen::VectorXd> sensView(NV_DATA_S(ys[i]), fgdata->stateSize);
    rhsView = dfdy * sensView + dfdp.col(i);
  }

  return 0;
}

static void check_sundials_flag(void *flagvalue, string funcname, int opt)
{
  int *errflag;

  /* Check if SUNDIALS function returned NULL pointer - no memory allocated */
  if ((opt == 0) && (flagvalue == NULL)) {
    std::cerr << "\nSUNDIALS_ERROR: " << funcname << " failed - returned NULL pointer\n\n";
      assert(flagvalue);
  }
  /* Check if flag < 0 */
  else if (opt == 1) {
    errflag = (int *)flagvalue;
    if (*errflag < 0) {
      std::cerr << "\nSUNDIALS_ERROR: " << funcname << " failed with flag = " << *errflag << "\n\n";
      assert(flagvalue >= 0);
    }
  }
  /* Check if function returned NULL pointer - no memory allocated */
  else if ((opt == 2) && (flagvalue == NULL)) {
    std::cerr << "\nMEMORY_ERROR: " << funcname << " failed - returned NULL pointer\n\n";
      assert(flagvalue);
  }
}

OdeModPiece::OdeModPiece(std::shared_ptr<ModPiece>    fIn,
                         std::shared_ptr<ModPiece>    gIn,
                         Eigen::VectorXd const      & evalTimes,
                         boost::property_tree::ptree properties) : ModPiece(fIn->inputSizes.tail(fIn->inputSizes.size() - 1), gIn->outputSize * evalTimes.rows(),
                                                                             true, true, false, false, gIn->isRandom),
                                                                    f(fIn), g(gIn), obsTimes(evalTimes),
                                                                    linSolver(properties.get("CVODES.LinearSolver", "Dense")),
                                                                    abstol(properties.get("CVODES.ATOL", 1e-3)),
                                                                    reltol(properties.get("CVODES.RTOL",1e-5)),
                                                                    intMethod(properties.get("CVODES.Method","BDF")),
                                                                    solveMethod(properties.get("CVODES.SolveMethod","Newton")),
                                                                    maxStepSize(properties.get("CVODES.MaxStepSize",1e20)),
                                                                    checkPtGap(properties.get("CVODES.CheckPtGap",50)),
																	preferAdjoint(properties.get("CVODES.PreferAdjoint",true))
{
      assert(!fIn->isRandom);
  // check sizes of f and g
      assert(fIn->outputSize == fIn->inputSizes(1));
      assert(fIn->inputSizes(0) == 1);
      assert(gIn->inputSizes(0) == 1);
      assert(gIn->inputSizes(1) == fIn->inputSizes(1));

      assert(gIn->inputSizes.size() == fIn->inputSizes.size());
  for (int inDim = 2; inDim < inputSizes.size(); ++inDim) {
      assert(gIn->inputSizes(inDim) == fIn->inputSizes(inDim));
  }
}

OdeModPiece::OdeModPiece(std::shared_ptr<ModPiece> fIn, std::shared_ptr<ModPiece> gIn,
                         Eigen::VectorXd const& evalTimes) : ModPiece(fIn->inputSizes.tail(
                                                                      fIn->inputSizes.size() - 1),
                                                                      gIn->outputSize * evalTimes.rows(), true, true, false, false,
                                                                      gIn->isRandom), f(fIn), g(gIn),
                                                                      obsTimes(evalTimes), linSolver("Dense"),
                                                                      abstol(1e-3), reltol(1e-5),
																	  intMethod("BDF"), solveMethod("Newton"),
																	   maxStepSize(1e20), checkPtGap(50), preferAdjoint(true)
{
      assert(!fIn->isRandom);
  // check sizes of f and g
      assert(fIn->outputSize == fIn->inputSizes(1));
      assert(fIn->inputSizes(0) == 1);
      assert(gIn->inputSizes(0) == 1);
      assert(gIn->inputSizes(1) == fIn->inputSizes(1));

      assert(gIn->inputSizes.size() == fIn->inputSizes.size());
  for (int inDim = 2; inDim < inputSizes.size(); ++inDim) {
      assert(gIn->inputSizes(inDim) == fIn->inputSizes(inDim));
  }
}

OdeModPiece::OdeModPiece(std::shared_ptr<ModPiece>    fIn,
                         Eigen::VectorXd const      & evalTimes,
                         boost::property_tree::ptree properties) : OdeModPiece(fIn,
                                                                                make_shared<IdentityObserver>(fIn->inputSizes,1),
																				evalTimes,
                                                                                properties)
{}

OdeModPiece::OdeModPiece(std::shared_ptr<ModPiece> fIn, Eigen::VectorXd const& evalTimes) : OdeModPiece(fIn,
                                                                                                        make_shared<IdentityObserver>(fIn->inputSizes,1),
                                                                                                        evalTimes)
{}


void OdeModPiece::RunCvodesForward(std::vector<Eigen::VectorXd> const& inputs,
                                   bool                                runSens,
                                   int const                           inputDimWrt,
                                   Eigen::VectorXd                   & eval,
                                   Eigen::MatrixXd                   & jac) const
{
  // if sensitivities are requested, make sure f has a jacobian function
  if (runSens) {
    assert(f->hasDirectJacobian);
  }

  N_Vector state;
  RhsData *fgdata;
  void    *cvode_mem;
  int iout, flag;

  state     = NULL;
  fgdata    = NULL;
  cvode_mem = NULL;

  int stateSize = inputSizes(0);

  /* Allocate memory, and set problem data, initial values, tolerances */
  state = N_VNew_Serial(stateSize);
  check_sundials_flag((void *)state,  "N_VNew_Serial", 0);

  fgdata = new RhsData;
  check_sundials_flag((void *)fgdata, "AllocUserData", 2);

  // set the rhs and observation modpieces
  fgdata->stateSize = stateSize;
  fgdata->f         = f;
  fgdata->g         = g;
  fgdata->inputs.push_back(Eigen::VectorXd::Zero(1));
  fgdata->inputs.insert(fgdata->inputs.end(), inputs.begin(), inputs.end());

  if (runSens) {
    fgdata->inputDimWrt = inputDimWrt;
  }

  // copy the initial state
  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), stateSize);
  nvStateMap = inputs.at(0);

  /* Call CVodeCreate to create the solver memory and specify the
   * Backward Differentiation Formula and the use of a Newton iteration */
  if (!intMethod.compare("BDF")) {
    if (!solveMethod.compare("Newton")) {
      cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
    } else if (!solveMethod.compare("Iter")) {
      cvode_mem = CVodeCreate(CV_BDF, CV_FUNCTIONAL);
    } else {
      std::cerr << "\nInvalid CVODES nonlinear solver type.  Options are Newton or Iter\n\n";
      assert(false);
    }
  } else if (!intMethod.compare("Adams")) {
    if (!solveMethod.compare("Newton")) {
      cvode_mem = CVodeCreate(CV_ADAMS, CV_NEWTON);
    } else if (!solveMethod.compare("Iter")) {
      cvode_mem = CVodeCreate(CV_ADAMS, CV_FUNCTIONAL);
    } else {
      std::cerr << "\nInvalid CVODES nonlinear solver type.  Options are Newton or Iter\n\n";
      assert(false);
    }
  } else {
    std::cerr << "\nInvalid CVODES solver type.  Options are BDF or Adams\n\n";
      assert(false);
  }

    check_sundials_flag((void *)cvode_mem, "CVodeCreate", 0);

  /* Set the pointer to user-defined data */
  flag = CVodeSetUserData(cvode_mem, fgdata);
    check_sundials_flag(&flag, "CVodeSetUserData",     1);

  flag = CVodeSetErrHandlerFn(cvode_mem, sundialsErrorFunc, fgdata);
    check_sundials_flag(&flag, "CVodeSetErrHandlerFn", 1);

  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in u'=f(t,u), the inital time T0, and
   * the initial dependent variable vector u. */
  flag = CVodeInit(cvode_mem, sundialsFunc, 0.0, state);
    check_sundials_flag(&flag, "CVodeInit", 1);

  /* Call CVodeSStolerances to specify the scalar relative tolerance
   * and scalar absolute tolerances */
  flag = CVodeSStolerances(cvode_mem, reltol, abstol);
    check_sundials_flag(&flag, "CVodeSStolerances", 1);

  // sensState will hold several N_Vectors (because it's a Jacobian)
  N_Vector *sensState;

  // the size of the parameter
  const int paramSize = inputSizes(inputDimWrt);

  if (runSens) {
    int sensitivityMethod = CV_SIMULTANEOUS;

    sensState = N_VCloneVectorArray_Serial(paramSize, state);
    check_sundials_flag((void *)sensState, "N_VCloneVectorArray_Serial", 0);

    // initialize the sensitivies to zero
    for (int is = 0; is < paramSize; ++is) {
      N_VConst(0.0, sensState[is]);
    }

    // initialze the forward sensitivity solver
    flag = CVodeSensInit(cvode_mem, paramSize, sensitivityMethod, sundialsForwardSensRhs, sensState);
      check_sundials_flag(&flag, "CVodeSensInit1", 1);

    Eigen::VectorXd absTolVec = abstol * Eigen::VectorXd::Ones(paramSize);
    flag = CVodeSensSStolerances(cvode_mem, reltol, absTolVec.data());
      check_sundials_flag(&flag, "CVodeSensSStolerances", 1);

    flag = CVodeSetSensErrCon(cvode_mem, true);
      check_sundials_flag(&flag, "CVodeSetSensErrCon",    1);
  }

  flag = CVodeSetMaxStep(cvode_mem, maxStepSize);
    check_sundials_flag(&flag, "CVodeSetMaxStep",       1);

  if (!linSolver.compare("Dense")) {
    /* Call CVDense to specify the CVDENSE dense linear solver */
    flag = CVDense(cvode_mem, stateSize);
      check_sundials_flag(&flag, "CVDense", 1);

    /* Set the Jacobian routine to Jac (user-supplied) */
    flag = CVDlsSetDenseJacFn(cvode_mem, sundialsJac);
      check_sundials_flag(&flag, "CVDlsSetDenseJacFn", 1);
  } else {
    if (!linSolver.compare("SPGMR")) {
      flag = CVSpgmr(cvode_mem, 0, 0);
      check_sundials_flag(&flag, "CVSpgmr",   1);
    } else if (!linSolver.compare("SPBCG")) {
      flag = CVSpbcg(cvode_mem, 0, 0);
      check_sundials_flag(&flag, "CVSpbcg",   1);
    } else if (!linSolver.compare("SPTFQMR")) {
      flag = CVSptfqmr(cvode_mem, 0, 0);
      check_sundials_flag(&flag, "CVSptfqmr", 1);
    } else {
      std::cerr << "\nInvalid CVODES linear solver type.  Options are Dense, SPGMR, SPBCG, or SPTFQMR\n\n";
      assert(false);
    }
    /* set the Jacobian-times-vector function */
    flag = CVSpilsSetJacTimesVecFn(cvode_mem, sundialsJacApply);
    check_sundials_flag(&flag, "CVSpilsSetJacTimesVecFn", 1);
  }
  /* Set the preconditioner solve and setup functions */
  //flag = CVSpilsSetPreconditioner(cvode_mem, Precond, PSolve);
  //if(check_sundials_flag(&flag, "CVSpilsSetPreconditioner", 1)) return(1);

  /* In loop over output points, call CVode, print results, test for error */
  double tout = 0;
  eval = Eigen::VectorXd::Constant(outputSize, std::numeric_limits<double>::signaling_NaN());
  Eigen::Map<Eigen::MatrixXd> outMap(eval.data(), g->outputSize, obsTimes.size());

  // resize the jacobian
  if (runSens) {
    jac = Eigen::MatrixXd::Constant(outputSize, inputSizes(inputDimWrt), std::numeric_limits<double>::signaling_NaN());
  }

  // loop over all the observation times
  for (int i = 0; i < obsTimes.size(); ++i) {
    if (fabs(obsTimes(i) - tout) > 1e-14) {
      flag = CVode(cvode_mem, obsTimes(i), state, &tout, CV_NORMAL);

      // if there was an error, clean up the memory and return
      if (flag < 0) {
        N_VDestroy_Serial(state);
        if (runSens) {
          N_VDestroyVectorArray_Serial(sensState, paramSize);
        }
        delete fgdata;
        CVodeFree(&cvode_mem);
        return;
      }


      if (runSens) {
        flag = CVodeGetSens(cvode_mem, &tout, sensState);

        // if there was an error, clean up the memory and return
        if (flag < 0) {
          N_VDestroy_Serial(state);
          N_VDestroyVectorArray_Serial(sensState, paramSize);
          delete fgdata;
          CVodeFree(&cvode_mem);
          return;
        }
      }
    }

    // evaluate g and store the results
    fgdata->inputs.at(0) = obsTimes(i) * Eigen::VectorXd::Ones(1);
    fgdata->inputs.at(1) = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(state), stateSize);

    outMap.col(i) = g->Evaluate(fgdata->inputs);
    if (runSens) {
      Eigen::Map < Eigen::MatrixXd, 0, Eigen::OuterStride < Eigen::Dynamic >> jacMap(&jac(i * g->outputSize,
                                                                                          0), g->outputSize,                                                                  inputSizes(
                                                                                       inputDimWrt),
                                                                                     Eigen::OuterStride<Eigen::Dynamic>(
                                                                                       outputSize));

      auto gJacParam = g->Jacobian(fgdata->inputs, inputDimWrt + 1);
      auto gJacState = g->Jacobian(fgdata->inputs, 1);
      for (int i = 0; i < paramSize; ++i) {
        jacMap.col(i) =  gJacParam.col(i) + gJacState * Eigen::Map<Eigen::VectorXd>(NV_DATA_S(sensState[i]), stateSize);
      }
    }
  }

  /* Free memory */
  N_VDestroy_Serial(state);
  if (runSens) {
    N_VDestroyVectorArray_Serial(sensState, paramSize);
  }

  delete fgdata;
  CVodeFree(&cvode_mem);
}

void OdeModPiece::RunCvodesAdjoint(std::vector<Eigen::VectorXd> const& inputs,         // input parameters
                                   Eigen::VectorXd              const& outputSens,     // dY/dg for some function Y
                                   int const                           inputDimWrt,    // differentiat wrt this
                                                                                       // parameter
                                   Eigen::VectorXd                   & gradient) const // jacobian will hold
                                                                                       // dY/dg*(dg/dx*dx/dp + dg/dp)
{
  N_Vector state;
  RhsData *fgdata;
  void    *cvode_mem;
  int iout, flag;

  state     = NULL;
  fgdata    = NULL;
  cvode_mem = NULL;

  int stateSize = inputSizes(0);

  /* Allocate memory, and set problem data, initial values, tolerances */
  state = N_VNew_Serial(stateSize);
  check_sundials_flag((void *)state,  "N_VNew_Serial", 0);

  fgdata = new RhsData;
  check_sundials_flag((void *)fgdata, "AllocUserData", 2);

  // set the rhs and observation modpieces
  fgdata->stateSize = stateSize;
  fgdata->f         = f;
  fgdata->g         = g;
  fgdata->inputs.push_back(Eigen::VectorXd::Zero(1));
  fgdata->inputs.insert(fgdata->inputs.end(), inputs.begin(), inputs.end());

  fgdata->inputDimWrt = inputDimWrt;

  // copy the initial state
  Eigen::Map<Eigen::VectorXd> nvStateMap(NV_DATA_S(state), stateSize);
  nvStateMap = inputs.at(0);

  /* Call CVodeCreate to create the solver memory and specify the
   * Backward Differentiation Formula and the use of a Newton iteration */
  if (!intMethod.compare("BDF")) {
    if (!solveMethod.compare("Newton")) {
      cvode_mem = CVodeCreate(CV_BDF, CV_NEWTON);
    } else if (!solveMethod.compare("Iter")) {
      cvode_mem = CVodeCreate(CV_BDF, CV_FUNCTIONAL);
    } else {
      std::cerr << "\nInvalid CVODES nonlinear solver type.  Options are Newton or Iter\n\n";
      assert(false);
    }
  } else if (!intMethod.compare("Adams")) {
    if (!solveMethod.compare("Newton")) {
      cvode_mem = CVodeCreate(CV_ADAMS, CV_NEWTON);
    } else if (!solveMethod.compare("Iter")) {
      cvode_mem = CVodeCreate(CV_ADAMS, CV_FUNCTIONAL);
    } else {
      std::cerr << "\nInvalid CVODES nonlinear solver type.  Options are Newton or Iter\n\n";
      assert(false);
    }
  } else {
    std::cerr << "\nInvalid CVODES solver type.  Options are BDF or Adams\n\n";
      assert(false);
  }

      check_sundials_flag((void *)cvode_mem, "CVodeCreate", 0);

  /* Set the pointer to user-defined data */
  flag = CVodeSetUserData(cvode_mem, fgdata);
      check_sundials_flag(&flag, "CVodeSetUserData", 1);

  /* Call CVodeInit to initialize the integrator memory and specify the
   * user's right hand side function in u'=f(t,u), the inital time T0, and
   * the initial dependent variable vector u. */
  flag = CVodeInit(cvode_mem, sundialsFunc, 0.0, state);
      check_sundials_flag(&flag, "CVodeInit", 1);

  /* Call CVodeSStolerances to specify the scalar relative tolerance
   * and scalar absolute tolerances */
  flag = CVodeSStolerances(cvode_mem, reltol, abstol);
      check_sundials_flag(&flag, "CVodeSStolerances", 1);

  flag = CVodeSetMaxStep(cvode_mem, maxStepSize);
      check_sundials_flag(&flag, "CVodeSetMaxStep",   1);


  if (!linSolver.compare("Dense")) {
    /* Call CVDense to specify the CVDENSE dense linear solver */
    flag = CVDense(cvode_mem, stateSize);
      check_sundials_flag(&flag, "CVDense", 1);

    /* Set the Jacobian routine to Jac (user-supplied) */
    flag = CVDlsSetDenseJacFn(cvode_mem, sundialsJac);
      check_sundials_flag(&flag, "CVDlsSetDenseJacFn", 1);
  } else {
    if (!linSolver.compare("SPGMR")) {
      flag = CVSpgmr(cvode_mem, 0, 0);
      check_sundials_flag(&flag, "CVSpgmr",   1);
    } else if (!linSolver.compare("SPBCG")) {
      flag = CVSpbcg(cvode_mem, 0, 0);
      check_sundials_flag(&flag, "CVSpbcg",   1);
    } else if (!linSolver.compare("SPTFQMR")) {
      flag = CVSptfqmr(cvode_mem, 0, 0);
      check_sundials_flag(&flag, "CVSptfqmr", 1);
    }
    /* set the Jacobian-times-vector function */
    flag = CVSpilsSetJacTimesVecFn(cvode_mem, sundialsJacApply);
      check_sundials_flag(&flag, "CVSpilsSetJacTimesVecFn", 1);
  }


  int Nstep = checkPtGap; // how often are checkpoints stored
  assert(checkPtGap > 0);
  int numCheckpts;

  flag = CVodeAdjInit(cvode_mem, Nstep, CV_HERMITE);
    check_sundials_flag(&flag, "CVodeAdjInit", 1);

  // forward integration
  double tend = obsTimes(obsTimes.size() - 1);
  double tout = 0;

  Eigen::MatrixXd savedStates(stateSize, obsTimes.size());
  savedStates.col(0) = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(state), stateSize);
  for (int i = 1; i < obsTimes.size(); ++i) {
    flag = CVodeF(cvode_mem, obsTimes(i), state, &tout, CV_NORMAL, &numCheckpts);
    check_sundials_flag(&flag, "CVodeF", 1);

    savedStates.col(i) = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(state), stateSize);
  }

  // the size of the parameter
  const int paramSize = inputSizes(inputDimWrt);
  fgdata->paramSize = paramSize;

  // adjoint variable
  N_Vector lambda, nvGrad;
  lambda = N_VNew_Serial(stateSize);
  nvGrad = N_VNew_Serial(paramSize);
    check_sundials_flag((void *)lambda, "N_VNew", 0);

  // initiaze the gradient to zero
  N_VConst(0.0, nvGrad);
  N_VConst(0.0, lambda);

  Eigen::Map<Eigen::VectorXd> nvLambdaMap(NV_DATA_S(lambda), stateSize);
  nvLambdaMap = -g->Gradient(fgdata->inputs, outputSens.tail(g->outputSize), 1);

  // initialize the adjoint variable
  fgdata->inputs.at(0) = tout * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(state), stateSize);

  int indexB;
  if (!intMethod.compare("BDF")) {
    if (!solveMethod.compare("Newton")) {
      flag = CVodeCreateB(cvode_mem, CV_BDF, CV_NEWTON, &indexB);
    } else if (!solveMethod.compare("Iter")) {
      flag = CVodeCreateB(cvode_mem, CV_BDF, CV_FUNCTIONAL, &indexB);
    } else {
      std::cerr << "\nInvalid CVODES nonlinear solver type.  Options are Newton or Iter\n\n";
      assert(false);
    }
  } else if (!intMethod.compare("Adams")) {
    if (!solveMethod.compare("Newton")) {
      flag = CVodeCreateB(cvode_mem, CV_ADAMS, CV_NEWTON, &indexB);
    } else if (!solveMethod.compare("Iter")) {
      flag = CVodeCreateB(cvode_mem, CV_ADAMS, CV_FUNCTIONAL, &indexB);
    } else {
      std::cerr << "\nInvalid CVODES nonlinear solver type.  Options are Newton or Iter\n\n";
      assert(false);
    }
  } else {
    std::cerr << "\nInvalid CVODES solver type.  Options are BDF or Adams\n\n";
      assert(false);
  }

      check_sundials_flag(&flag, "CVodeCreateB",       1);

  flag = CVodeSetUserDataB(cvode_mem, indexB, fgdata);
      check_sundials_flag(&flag, "CVodeSetUserDataB",  1);

  flag = CVodeInitB(cvode_mem, indexB, adjointRhs, tout, lambda);
      check_sundials_flag(&flag, "CVodeInitB",         1);

  flag = CVodeSStolerancesB(cvode_mem, indexB, reltol, abstol);
      check_sundials_flag(&flag, "CVodeSStolerancesB", 1);


  flag = CVodeSetMaxStepB(cvode_mem, indexB, maxStepSize);
      check_sundials_flag(&flag, "CVodeSetMaxStepB", 1);


  if (!linSolver.compare("Dense")) {
    /* Call CVDense to specify the CVDENSE dense linear solver */
    flag = CVDenseB(cvode_mem, indexB, stateSize);
      check_sundials_flag(&flag, "CVDenseB", 1);

    /* Set the Jacobian routine to Jac (user-supplied) */
    flag = CVDlsSetDenseJacFnB(cvode_mem, indexB, adjointJacobian);
      check_sundials_flag(&flag, "CVDlsSetDenseJacFnB", 1);
  } else {
    if (!linSolver.compare("SPGMR")) {
      flag = CVSpgmrB(cvode_mem, indexB, 0, 0);
      check_sundials_flag(&flag, "CVSpgmrB",   1);
    } else if (!linSolver.compare("SPBCG")) {
      flag = CVSpbcgB(cvode_mem, indexB,  0, 0);
      check_sundials_flag(&flag, "CVSpbcgB",   1);
    } else if (!linSolver.compare("SPTFQMR")) {
      flag = CVSptfqmrB(cvode_mem, indexB, 0, 0);
      check_sundials_flag(&flag, "CVSptfqmrB", 1);
    }
    /* set the Jacobian-times-vector function */
    flag = CVSpilsSetJacTimesVecFnB(cvode_mem, indexB, adjointJacApply);
      check_sundials_flag(&flag, "CVSpilsSetJacTimesVecFnB", 1);
  }

  flag = CVodeQuadInitB(cvode_mem, indexB, adjointQuad, nvGrad);
      check_sundials_flag(&flag, "CVodeQuadInitB",         1);

  flag = CVodeSetQuadErrCon(cvode_mem, true);
      check_sundials_flag(&flag, "CVodeSetQuadErrCon",     1);

  flag = CVodeQuadSStolerancesB(cvode_mem, indexB, reltol, abstol);
      check_sundials_flag(&flag, "CVodeQuadSStolerancesB", 1);


  // backwards integration for lagrange multiplier
  tout = obsTimes(obsTimes.size() - 1);
  for (int i = obsTimes.size() - 2; i >= 0; --i) {
    // integrate the adjoint variable back in time
    flag = CVodeB(cvode_mem, obsTimes(i), CV_NORMAL);
      check_sundials_flag(&flag, "CVodeB", 1);

    // get the adjoint variable
    flag = CVodeGetB(cvode_mem, indexB, &tout, lambda);
      check_sundials_flag(&flag, "CVodeGetB", 1);

    fgdata->inputs.at(0) = obsTimes(i) * Eigen::VectorXd::Ones(1);
    fgdata->inputs.at(1) = savedStates.col(i);

    // introduce the delta function right hand side
    nvLambdaMap = Eigen::Map<Eigen::VectorXd>(NV_DATA_S(lambda), stateSize);

    nvLambdaMap -= g->Gradient(fgdata->inputs, outputSens.segment(g->outputSize * i, g->outputSize), 1);

    // reinitialize the adjoint integrator because of the discontinuity in the state
    flag = CVodeReInitB(cvode_mem, indexB, obsTimes(i), lambda);
      check_sundials_flag(&flag, "CVodeReInitB", 1);

    // re init the quadrature
    flag = CVodeGetQuadB(cvode_mem, indexB, &tout, nvGrad);
      check_sundials_flag(&flag, "CVodeGetQuad",     1);
    flag = CVodeQuadReInitB(cvode_mem, indexB, nvGrad);
      check_sundials_flag(&flag, "CVodeQuadReInitB", 1);
  }


  // initial gradient and add each dg/dp component
  fgdata->inputs.at(0) = obsTimes(0) * Eigen::VectorXd::Ones(1);
  fgdata->inputs.at(1) = savedStates.col(0);
  gradient             = g->Gradient(fgdata->inputs, outputSens.head(g->outputSize), inputDimWrt + 1);
  for (int i = 1; i < obsTimes.size(); ++i) {
    fgdata->inputs.at(0) = obsTimes(i) * Eigen::VectorXd::Ones(1);
    fgdata->inputs.at(1) = savedStates.col(i);
    gradient            += g->Gradient(fgdata->inputs, outputSens.segment(g->outputSize * i,
                                                                          g->outputSize), inputDimWrt + 1);
  }

  // get the quadrature part of the gradient
  flag = CVodeGetQuadB(cvode_mem, indexB, &tout, nvGrad);
      check_sundials_flag(&flag, "CVodeGetQuad", 1);

  // add the quadrature part to the gradient
  gradient += Eigen::Map<Eigen::VectorXd>(NV_DATA_S(nvGrad), paramSize);

  /* Free memory */
  N_VDestroy_Serial(state);
  N_VDestroy_Serial(lambda);
  N_VDestroy_Serial(nvGrad);

  delete fgdata;
  CVodeFree(&cvode_mem);
}

Eigen::VectorXd OdeModPiece::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  Eigen::VectorXd output;
  Eigen::MatrixXd jac;

  RunCvodesForward(inputs, false, 0, output, jac);

  return output;
}

Eigen::VectorXd OdeModPiece::GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
                                          Eigen::VectorXd const             & sensitivity,
                                          int const                           inputDimWrt)
{
  Eigen::VectorXd gradient;
  
  // if the parameter has a large dimension, use adjoint sensitivities
  if((2*g->outputSize < inputSizes(inputDimWrt))&&(preferAdjoint)){
    RunCvodesAdjoint(inputs, sensitivity, inputDimWrt, gradient);
	
  // if the parameter has a small dimension compared to the output, use the forward sensitivities	
  }else{
    gradient = Jacobian(inputs,inputDimWrt).transpose()*sensitivity;
  }
  

  return gradient;
}

Eigen::MatrixXd OdeModPiece::JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
{
  Eigen::VectorXd output;
  Eigen::MatrixXd jac;

  RunCvodesForward(inputs, true, inputDimWrt, output, jac);

  return jac;
}
