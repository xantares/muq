
#include "MUQ/Modelling/ProductModel.h"

using namespace std;
using namespace muq::Modelling;
using namespace Eigen;

ProductModel::ProductModel(int const numInputs, int const outputSize) : ModPiece(outputSize *
                                                                                 Eigen::VectorXi::Ones(
                                                                                   numInputs), outputSize, true, true, true, true,
                                                                                 false)
{}

Eigen::VectorXd ProductModel::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  Eigen::VectorXd result = Eigen::VectorXd::Ones(outputSize);

  for (unsigned int i = 0; i < inputSizes.size(); ++i) {
    result.array() *= inputs[i].array();
  }

  return result;
}

///A helper function, take the product of the inputs without the one in spot dim.
VectorXd ProductWithoutOne(vector<Eigen::VectorXd> const& inputs, int const dim)
{
  Eigen::VectorXd otherTerms = Eigen::VectorXd::Ones(inputs.at(0).rows());

  for (unsigned int i = 0; i < inputs.size(); ++i) {
    if (i != dim) {
      otherTerms.array() *= inputs[i].array();
    }
  }
  return otherTerms;
}

Eigen::MatrixXd ProductModel::JacobianImpl(vector<Eigen::VectorXd> const& inputs, int const dim)
{
  return ProductWithoutOne(inputs, dim).asDiagonal();
}

Eigen::VectorXd ProductModel::JacobianActionImpl(vector<Eigen::VectorXd> const& inputs,
                                                 Eigen::VectorXd const        & target,
                                                 int const                      dim)
{
  return ProductWithoutOne(inputs, dim).array() * target.array();
}

Eigen::VectorXd ProductModel::GradientImpl(vector<Eigen::VectorXd> const& inputs,
                                           Eigen::VectorXd const        & sens,
                                           int const                      dim)
{
  return ProductWithoutOne(inputs, dim).array() * sens.array();
}

Eigen::MatrixXd ProductModel::HessianImpl(vector<Eigen::VectorXd> const&, Eigen::VectorXd const&, int const)
{
  return Eigen::MatrixXd::Zero(outputSize, outputSize);
}

