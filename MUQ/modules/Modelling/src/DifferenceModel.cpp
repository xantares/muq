
#include "MUQ/Modelling/DifferenceModel.h"

using namespace std;
using namespace muq::Modelling;

DifferenceModel::DifferenceModel(int const outputSize) : ModPiece(outputSize * Eigen::VectorXi::Ones(
                                                                    2), outputSize, true, true, true, true,
                                                                  false)
{}

Eigen::VectorXd DifferenceModel::EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs)
{
  Eigen::VectorXd result = Eigen::VectorXd::Zero(outputSize);

  result = inputs[0] - inputs[1];

  return result;
}

Eigen::MatrixXd DifferenceModel::JacobianImpl(vector<Eigen::VectorXd> const&, int const dim)
{
  if (dim == 0) {
    return Eigen::MatrixXd::Identity(outputSize, outputSize);
  } else {
    return -Eigen::MatrixXd::Identity(outputSize, outputSize);
  }
}

Eigen::VectorXd DifferenceModel::JacobianActionImpl(vector<Eigen::VectorXd> const&,
                                                    Eigen::VectorXd const      & target,
                                                    int const                    dim)
{
  if (dim == 0) {
    return target;
  } else {
    return -target;
  }
}

Eigen::VectorXd DifferenceModel::GradientImpl(vector<Eigen::VectorXd> const&, Eigen::VectorXd const& sens,
                                              int const dim)
{
  if (dim == 0) {
    return sens;
  } else {
    return -sens;
  }
}

Eigen::MatrixXd DifferenceModel::HessianImpl(vector<Eigen::VectorXd> const&, Eigen::VectorXd const&, int const)
{
  return Eigen::MatrixXd::Zero(outputSize, outputSize);
}

