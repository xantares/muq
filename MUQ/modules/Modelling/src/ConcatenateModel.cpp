#include "MUQ/Modelling/ConcatenateModel.h"

using namespace muq::Modelling;
using namespace std;

#if MUQ_PYTHON == 1
ConcatenateModel::ConcatenateModel(boost::python::list const& inputSizes) : 
  ConcatenateModel(muq::Utilities::GetEigenVector<Eigen::VectorXi>(inputSizes)) {}
#endif // MUQ_PYTHON == 1

Eigen::VectorXd ConcatenateModel::EvaluateImpl(std::vector<Eigen::VectorXd> const& input)
{
  // allocate the output vector
  Eigen::VectorXd output(outputSize);

  // insert each input vector
  int currInd = 0;

  for (int i = 0; i < inputSizes.size(); ++i) {
    output.segment(currInd, inputSizes[i]) = input[i];
    currInd                               += inputSizes[i];
  }

  return output;
}

Eigen::VectorXd ConcatenateModel::GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                               Eigen::VectorXd const             & sensitivity,
                                               int const                           inputDimWrt)
{
  if (inputDimWrt != 0) {
    return sensitivity.segment(inputSizes.head(inputDimWrt - 1).sum(), inputSizes[inputDimWrt]);
  } else {
    return sensitivity.head(inputSizes[0]);
  }
}

Eigen::MatrixXd ConcatenateModel::JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt)
{
  Eigen::MatrixXd Jacobian = Eigen::MatrixXd::Zero(outputSize, inputSizes[inputDimWrt]);
  int ind                  = (inputDimWrt != 0) ? inputSizes.head(inputDimWrt - 1).sum() : 0;

  Jacobian.block(ind, 0, inputSizes[inputDimWrt],
                 inputSizes[inputDimWrt]) = Eigen::MatrixXd::Identity(inputSizes[inputDimWrt], inputSizes[inputDimWrt]);

  return Jacobian;
}

Eigen::VectorXd ConcatenateModel::JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                                     Eigen::VectorXd const             & target,
                                                     int const                           inputDimWrt)
{
  Eigen::VectorXd output = Eigen::VectorXd::Zero(outputSize);

  if (inputDimWrt != 0) {
    output.segment(inputSizes.head(inputDimWrt - 1).sum(), inputSizes[inputDimWrt]) = target;
  } else {
    output.head(inputSizes[0]) = target;
  }
  return output;
}

