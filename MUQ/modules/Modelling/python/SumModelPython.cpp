#include "MUQ/Modelling/python/SumModelPython.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;

SumModelPython::SumModelPython(int const numInputs, int const outputSize) : SumModel(numInputs, outputSize) {}

boost::python::list SumModelPython::PyEvaluate(boost::python::list const& inputs)
{
  vector<Eigen::VectorXd> eigenInputs = PythonListToVector(inputs);

  return GetPythonVector<Eigen::VectorXd>(Evaluate(eigenInputs));
}

boost::python::list SumModelPython::PyGradient(boost::python::list const& pyInputs,
                                               boost::python::list const& pySensitivity,
                                               int const                  inputDimWrt)
{
  Eigen::VectorXd sensitivity = GetEigenVector<Eigen::VectorXd>(pySensitivity);

  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  assert((int)inputs.size() == inputSizes.size());

  return GetPythonVector<Eigen::VectorXd>(Gradient(inputs, sensitivity, inputDimWrt));
}

boost::python::list SumModelPython::PyJacobian(boost::python::list const& pyInputs, int const inputDimWrt)
{
  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  assert((int)inputs.size() == inputSizes.size());

  return GetPythonMatrix(Jacobian(inputs, inputDimWrt));
}

boost::python::list SumModelPython::PyJacobianAction(boost::python::list const& pyInputs,
                                                     boost::python::list const& pyTarget,
                                                     int const                  inputDimWrt)
{
  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  assert((int)inputs.size() == inputSizes.size());

  Eigen::VectorXd target = GetEigenVector<Eigen::VectorXd>(pyTarget);

  return GetPythonVector<Eigen::VectorXd>(JacobianAction(inputs, target, inputDimWrt));
}

boost::python::list SumModelPython::PyHessian(boost::python::list const& pyInputs,
                                              boost::python::list const& pySensitivity,
                                              int const                  inputDimWrt)
{
  Eigen::VectorXd sensitivity = GetEigenVector<Eigen::VectorXd>(pySensitivity);

  vector<Eigen::VectorXd> inputs = PythonListToVector(pyInputs);
  assert((int)inputs.size() == inputSizes.size());

  return GetPythonMatrix(Hessian(inputs, sensitivity, inputDimWrt));
}

void muq::Modelling::ExportSumModel()
{
  boost::python::class_<SumModelPython, std::shared_ptr<SumModelPython>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportSumModelPython(
    "SumModel",
    boost::python::init<int const, int const>());

  exportSumModelPython.def("Evaluate", &SumModelPython::PyEvaluate);
  exportSumModelPython.def("Gradient", &SumModelPython::PyGradient);
  exportSumModelPython.def("Jacobian", &SumModelPython::PyJacobian);
  exportSumModelPython.def("JacobianAction", &SumModelPython::PyJacobianAction);
  exportSumModelPython.def("Hessian", &SumModelPython::PyHessian);

  boost::python::implicitly_convertible<std::shared_ptr<SumModelPython>,
                                        std::shared_ptr<ModPiece> >();
}

