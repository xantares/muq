#include "MUQ/Modelling/python/GaussianDensityPython.h"

using namespace std;
namespace py = boost::python;
using namespace muq::Utilities;
using namespace muq::Modelling;

GaussianDensity::GaussianDensity(py::list const& dimIn) :
  GaussianDensity(GetEigenVector<Eigen::VectorXi>(dimIn)) {}

GaussianDensity::GaussianDensity(py::list const& mu, double const scaledCov) :
  GaussianDensity(GetEigenVector<Eigen::VectorXd>(mu), scaledCov) {}

GaussianDensity::GaussianDensity(py::list const& mu, py::list const& diag) :
  GaussianDensity(GetEigenVector<Eigen::VectorXd>(mu), GetEigenVector<Eigen::VectorXd>(diag)) {}

shared_ptr<GaussianDensity> GaussianDensity::PyCreate(py::list const& mu, py::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::DiagonalPrecision) || (mode == GaussianSpecification::DiagonalCovariance)) {
    return make_shared<GaussianDensity>(GetEigenVector<Eigen::VectorXd>(mu), GetEigenVector<Eigen::VectorXd>(cov_or_prec), mode);
  }
  
  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    return make_shared<GaussianDensity>(GetEigenVector<Eigen::VectorXd>(mu), GetEigenMatrix(cov_or_prec), mode);
  }

  assert((mode == GaussianSpecification::DiagonalPrecision) ||
         (mode == GaussianSpecification::DiagonalCovariance) ||
         (mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

shared_ptr<GaussianDensity> GaussianDensity::PyCreateJoint(py::list const& dimIn, py::list const& mu, py::list const& py_cov_or_prec, GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::DiagonalPrecision) || (mode == GaussianSpecification::DiagonalCovariance)) {
    Eigen::VectorXd cov_or_prec = GetEigenMatrix(py_cov_or_prec);

    return make_shared<GaussianDensity>(GetEigenVector<Eigen::VectorXi>(dimIn), GetEigenVector<Eigen::VectorXd>(mu), cov_or_prec, mode);
  }


  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    Eigen::MatrixXd cov_or_prec = GetEigenMatrix(py_cov_or_prec);

    return make_shared<GaussianDensity>(GetEigenVector<Eigen::VectorXi>(dimIn), GetEigenVector<Eigen::VectorXd>(mu), cov_or_prec, mode);
  }

                                              assert((mode == GaussianSpecification::DiagonalPrecision) ||
         (mode == GaussianSpecification::DiagonalCovariance) ||
         (mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

shared_ptr<GaussianDensity> GaussianDensity::PyCreateConditional(unsigned int const dim, py::list const& mu, py::list const& cov_or_prec, GaussianSpecification::SpecificationMode const& mode)
{
  if ((mode == GaussianSpecification::PrecisionMatrix) || (mode == GaussianSpecification::CovarianceMatrix)) {
    return make_shared<GaussianDensity>(dim, GetEigenVector<Eigen::VectorXd>(mu), GetEigenMatrix(cov_or_prec), mode);
  }

  assert((mode == GaussianSpecification::CovarianceMatrix) ||
         (mode == GaussianSpecification::PrecisionMatrix));

  return nullptr;
}

py::list GaussianDensity::PyMean() const
{
  assert(specification->inputSizes.size() == 0);

  return GetPythonVector<Eigen::VectorXd>(Mean());
}

py::list GaussianDensity::PyMeanIns(py::list const& ins) const
{
  return GetPythonVector<Eigen::VectorXd>(Mean(PythonListToVector(ins)));
}

void muq::Modelling::ExportGaussianDensity()
{
  py::enum_<GaussianSpecification::SpecificationMode> specEnum("GaussianSpecification");

  specEnum.value("ScaledIdentity", GaussianSpecification::ScaledIdentity);
  specEnum.value("CovarianceMatrix", GaussianSpecification::CovarianceMatrix);
  specEnum.value("PrecisionMatrix", GaussianSpecification::PrecisionMatrix);
  specEnum.value("DiagonalCovariance", GaussianSpecification::DiagonalCovariance);
  specEnum.value("DiagonalPrecision", GaussianSpecification::DiagonalPrecision);

  py::class_<GaussianDensity, shared_ptr<GaussianDensity>,
             py::bases<Density, ModPiece>, boost::noncopyable> exportGaussianDens("GaussianDensity",
                                                                                  py::init<unsigned int const>());

  exportGaussianDens.def(py::init<unsigned int const, double const>());
  exportGaussianDens.def(py::init<py::list const&>());
  exportGaussianDens.def(py::init<py::list const&, double const>());
  exportGaussianDens.def(py::init<py::list const&, py::list const&>());
  exportGaussianDens.def("__init__", py::make_constructor(&GaussianDensity::PyCreate));
  exportGaussianDens.def("__init__", py::make_constructor(&GaussianDensity::PyCreateJoint));
  exportGaussianDens.def("__init__", py::make_constructor(&GaussianDensity::PyCreateConditional));

  exportGaussianDens.def("Mean", &GaussianDensity::PyMean);
  exportGaussianDens.def("Mean", &GaussianDensity::PyMeanIns);

  // convert to ModPiece and Density
  py::implicitly_convertible<shared_ptr<GaussianDensity>, shared_ptr<ModPiece> >();
  py::implicitly_convertible<shared_ptr<GaussianDensity>, shared_ptr<Density> >();
}

