//
//  AugLag.cpp
//
//
//  Created by Matthew Parno on 5/15/13.
//  Copyright (c) 2013 MIT. All rights reserved.
//

#include "MUQ/Optimization/Algorithms/AugLag.h"
#include "MUQ/Optimization/Problems/AugLagProb.h"

using namespace muq::Optimization;
using namespace std;


REGISTER_OPT_DEF_TYPE(AugLag)
/** Construct an Augmented Lagrangian based optimization solver from the settings in a ptree.  The ptree should use some
 * base optimization algorithm as Opt.Method and should have an Opt.ConstraintHandler set to AugLag. */
AugLag::AugLag(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : OptAlgBase(ProbPtr,
                                                                                                           properties,
                                                                                                           false,
                                                                                                           true,
                                                                                                           false,
                                                                                                           true,
                                                                                                           true,
                                                                                                           true,
                                                                                                           true,
                                                                                                           false),
                                                                                                uncProp(properties)
{
  // get augmented lagrangian specific parameters
  auglag_gamma = properties.get("Opt.AugLag.gamma", 2.0);
  auglag_tau   = properties.get("Opt.AugLag.tau", 0.6);
  maxOuterIts  = properties.get("Opt.AugLag.maxOuter", 100);
}

/** Solve the optimization problem using x0 as a starting point for the iteration. */
Eigen::VectorXd AugLag::solve(const Eigen::VectorXd& x0)
{
  // penalty parameter
  Eigen::VectorXd mu = Eigen::VectorXd::Ones(OptProbPtr->NumInequalities());

  // approximation of lagrange multipliers
  Eigen::VectorXd lambda = Eigen::VectorXd::Zero(OptProbPtr->NumEqualities());

  // current position
  Eigen::VectorXd xc = x0;
  double rho         = 1;
  double gamma       = auglag_gamma;
  double tau         = auglag_tau;

  double new_Cnorm, new_SigNorm, old_Cnorm = 0, old_SigNorm = 0;

  // define the upper and lower bounds on lambdas and mus
  Eigen::VectorXd mu_max     = 5 * Eigen::VectorXd::Ones(mu.size());
  Eigen::VectorXd lambda_min = -5 * Eigen::VectorXd::Ones(lambda.size());
  Eigen::VectorXd lambda_max = 5 * Eigen::VectorXd::Ones(lambda.size());

  // augmented lagrangian iteration
  bool   feasibleFlag = false;
  bool   optimalFlag  = false;
  double dx           = 10;
  int    it           = 0;

  const double tolScale = 0.03;
  double minFtol        = uncProp.get("Opt.ftol", 1e-4);
  double minXtol        = uncProp.get("Opt.xtol", 1e-4);
  double minGtol        = uncProp.get("Opt.gtol", 1e-4);
  double baseFTol       = exp(log(minFtol) / (tolScale * maxOuterIts));
  double baseXTol       = exp(log(minXtol) / (tolScale * maxOuterIts));
  double baseGTol       = exp(log(minGtol) / (tolScale * maxOuterIts));

  while (((!feasibleFlag) || (!optimalFlag)) && (it < maxOuterIts)) {
    if (verbose > 0) {
      std::cout << "Solving Augmented-Lagrangian subproblem...\n";
      std::cout << "\tOuter iteration " << it << std::endl;
    }

    // created the augmented optimization problem
    shared_ptr<OptProbBase> AugProb = make_shared<AugLagProb>(OptProbPtr, mu, lambda, rho);

    // create a unconstrained solver for this problem
    uncProp.put("Opt.gtol", std::max(pow(baseGTol, it), minGtol));
    uncProp.put("Opt.xtol", std::max(pow(baseXTol, it), minXtol));
    uncProp.put("Opt.ftol", std::max(pow(baseFTol, it), minFtol));
    shared_ptr<OptAlgBase> uncSolver = OptAlgBase::Create(AugProb, uncProp);


    // solve the unconstrainted problem
    Eigen::VectorXd newX = uncSolver->solve(xc);
    status = uncSolver->GetStatus();
    if (verbose > 0) {
      std::cout << "\tInner iteration terminated with status " << status << std::endl;
    }

    // if we had a successful solve, update the current position to the min of the unconstrained problem
    if (status >= 0) {
      // check to see how much we've changed
      dx = (newX - xc).norm();

      // update the current iterate with the results of the subproblem optimization
      xc = newX;
    } else {
      // we failed, so just return what we have and leave the failed status flag
      return newX;
    }

    // check for convergence in equality constraints and update lambda if we need to continue
    feasibleFlag = true;
    Eigen::VectorXd cVals;
    if (OptProbPtr->NumEqualities() > 0) {
      cVals = OptProbPtr->equalityConsts.eval(xc);

      if (verbose > 0) {
        std::cout << "\tCurrent equality residual: " << cVals.norm() << std::endl << std::endl;
      }

      // check for equality feasibility
      if (cVals.norm() > ctol) {
        lambda += rho * cVals;

        // make sure the new lambdas are in the bounds of lambda
        lambda = lambda.cwiseMax(lambda_min).cwiseMin(lambda_max);

        feasibleFlag = false;
        // if there are only equality constraints, then we are optimal
        if (OptProbPtr->NumInequalities() == 0) {
          optimalFlag = true;
        }
      } else {
        feasibleFlag = true;
      }
    }


    Eigen::VectorXd inVals;
    Eigen::VectorXd sigma;

    // check for convergence in inequality constraints and update mu if we need to continue
    if (OptProbPtr->NumInequalities() > 0) {
      inVals = OptProbPtr->inequalityConsts.eval(xc);


      if (verbose > 0) {
        std::cout << "\tCurrent inequality residual: " << inVals.maxCoeff() << std::endl << std::endl;
      }


      // check for inequality feasibility
      if ((inVals.maxCoeff() > ctol)) {
        feasibleFlag = false;
      }


      // update mu
      sigma = inVals.cwiseMax(-mu / rho);
      mu    = (mu + rho * inVals).cwiseMax(Eigen::VectorXd::Zero(inVals.size()));

      // make sure mu is within the bounds
      mu = mu.cwiseMin(mu_max);
    }

    if ((cVals.size() > 0) && (inVals.size() > 0)) {
      new_Cnorm = cVals.lpNorm<Eigen::Infinity>();

      new_SigNorm =  sigma.lpNorm<Eigen::Infinity>();


      if (std::max(new_Cnorm, new_SigNorm) > tau * std::max(old_Cnorm, old_SigNorm)) {
        rho = gamma * rho;
      }

      old_Cnorm   = new_Cnorm;
      old_SigNorm = new_SigNorm;
    } else if (inVals.size() > 0) {
      new_SigNorm =  sigma.lpNorm<Eigen::Infinity>();

      if (new_SigNorm > tau * old_SigNorm) {
        rho = gamma * rho;
      }

      old_SigNorm = new_SigNorm;
    } else if (cVals.size() > 0) {
      new_Cnorm = cVals.lpNorm<Eigen::Infinity>();
      if (new_Cnorm > tau * old_Cnorm) {
        rho = gamma * rho;
      }
      old_Cnorm = new_Cnorm;
    }

    // check for convergence.  If we are feasible and haven't moved much, we are converged
    if (dx < minXtol) {
      optimalFlag = true;
    }

    ++it;
  }

  return xc;
}

