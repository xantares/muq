
#include "MUQ/Optimization/python/OptProbBasePython.h"

using namespace std;
using namespace muq::Optimization;
using namespace muq::Utilities;

OptProbBasePython::OptProbBasePython(int dimIn) : OptProbBase(dimIn) {}

double OptProbBasePython::PyEval(boost::python::list const& xc)
{
  return this->get_override("Evaluate") (xc);
}

double OptProbBasePython::eval(Eigen::VectorXd const& xc)
{
  return PyEval(GetPythonVector<Eigen::VectorXd>(xc));
}

double OptProbBasePython::grad(Eigen::VectorXd const& xc, Eigen::VectorXd& gradient)
{
  boost::python::list obj = PyGrad(GetPythonVector<Eigen::VectorXd>(xc));

  gradient = GetEigenVector<Eigen::VectorXd>(boost::python::extract<boost::python::list>(obj[1]));

  return boost::python::extract<double>(obj[0]);
}

boost::python::list OptProbBasePython::PyGrad(boost::python::list const& xc)
{
  if (boost::python::override PyGrad = this->get_override("Grad")) {
    return PyGrad(xc);
  }

  Eigen::VectorXd gradient;
  double obj = this->OptProbBase::grad(GetEigenVector<Eigen::VectorXd>(xc), gradient);

  boost::python::list pyGradient;
  for (unsigned int i = 0; i < gradient.size(); ++i) {
    pyGradient.append(gradient(i));
  }

  boost::python::list result;
  result.append(obj);
  result.append(pyGradient);

  return result;
}

Eigen::VectorXd OptProbBasePython::applyInvHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  return GetEigenVector<Eigen::VectorXd>(PyApplyInvHess(GetPythonVector<Eigen::VectorXd>(xc),
                                                        GetPythonVector<Eigen::VectorXd>(vecIn)));
}

Eigen::MatrixXd OptProbBasePython::getInvHess(const Eigen::VectorXd& xc)
{
  return GetEigenMatrix(PyGetInvHess(GetPythonVector<Eigen::VectorXd>(xc)));
}

boost::python::list OptProbBasePython::PyApplyInvHess(const boost::python::list& xc, const boost::python::list& vecIn)
{
  if (boost::python::override PyApplyInvHess = this->get_override("ApplyInvHess")) {
    return PyApplyInvHess(xc, vecIn);
  }

  return GetPythonVector<Eigen::VectorXd>(this->OptProbBase::applyInvHess(GetEigenVector<Eigen::VectorXd>(xc),
                                                                          GetEigenVector<Eigen::VectorXd>(vecIn)));
}

boost::python::list OptProbBasePython::PyGetInvHess(const boost::python::list& xc)
{
  if (boost::python::override PyGetInvHess = this->get_override("GetInvHess")) {
    return PyGetInvHess(xc);
  }

  return GetPythonMatrix(this->OptProbBase::getInvHess(GetEigenVector<Eigen::VectorXd>(xc)));
}

Eigen::VectorXd OptProbBasePython::applyHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn)
{
  return GetEigenVector<Eigen::VectorXd>(PyApplyHess(GetPythonVector<Eigen::VectorXd>(xc),
                                                     GetPythonVector<Eigen::VectorXd>(vecIn)));
}

Eigen::MatrixXd OptProbBasePython::getHess(const Eigen::VectorXd& xc)
{
  return GetEigenMatrix(PyGetHess(GetPythonVector<Eigen::VectorXd>(xc)));
}

boost::python::list OptProbBasePython::PyApplyHess(const boost::python::list& xc, const boost::python::list& vecIn)
{
  if (boost::python::override PyApplyHess = this->get_override("ApplyHess")) {
    return PyApplyHess(xc, vecIn);
  }

  return GetPythonVector<Eigen::VectorXd>(this->OptProbBase::applyHess(GetEigenVector<Eigen::VectorXd>(xc),
                                                                       GetEigenVector<Eigen::VectorXd>(vecIn)));
}

boost::python::list OptProbBasePython::PyGetHess(const boost::python::list& xc)
{
  if (boost::python::override PyGetHess = this->get_override("GetHess")) {
    return PyGetHess(xc);
  }

  return GetPythonMatrix(this->OptProbBase::getHess(GetEigenVector<Eigen::VectorXd>(xc)));
}

void               muq::Optimization::ExportOptProbBase()
{
  boost::python::class_<OptProbBasePython, std::shared_ptr<OptProbBasePython>, boost::noncopyable> exportOptProbBase(
    "OptProbBase",
    boost::python::init<int>());

  // expose functions
  // eval needs to become evaluate because eval is a keyword in python
  exportOptProbBase.def("Evaluate", boost::python::pure_virtual(&OptProbBasePython::PyEval));
  exportOptProbBase.def("Grad", &OptProbBasePython::PyGrad);
  exportOptProbBase.def("ApplyInvHess", &OptProbBasePython::PyApplyInvHess);
  exportOptProbBase.def("GetInvHess", &OptProbBasePython::PyGetInvHess);
  exportOptProbBase.def("ApplyHess", &OptProbBasePython::PyApplyHess);
  exportOptProbBase.def("GetHess", &OptProbBasePython::PyGetHess);
  exportOptProbBase.def("IsConstrained", &OptProbBase::isConstrained);
  exportOptProbBase.def("IsStochastic", &OptProbBase::isStochastic);
  exportOptProbBase.def("IsUnconstrained", &OptProbBase::isUnconstrained);
  exportOptProbBase.def("IsUnconstrained", &OptProbBase::isUnconstrained);
  exportOptProbBase.def("IsBoundConstrained", &OptProbBase::isBoundConstrained);
  exportOptProbBase.def("IsPartlyBoundConstrained", &OptProbBase::isPartlyBoundConstrained);
  exportOptProbBase.def("IsFullyBoundConstrained", &OptProbBase::isFullyBoundConstrained);
  exportOptProbBase.def("IsLinearlyConstrained", &OptProbBase::isLinearlyConstrained);
  exportOptProbBase.def("IsNonlinearlyConstrained", &OptProbBase::isNonlinearlyConstrained);
  exportOptProbBase.def("GetDim", &OptProbBase::GetDim);
  exportOptProbBase.def("AddConstraint", &OptProbBase::AddConstraint);
  exportOptProbBase.def("NumEqualities", &OptProbBase::NumEqualities);
  exportOptProbBase.def("NumInequalities", &OptProbBase::NumInequalities);

  // register parent to python
  boost::python::register_ptr_to_python<std::shared_ptr<OptProbBase> >();

  // allow conversion to parent
  boost::python::implicitly_convertible<std::shared_ptr<OptProbBasePython>, std::shared_ptr<OptProbBase> >();
}
