
#include <gtest/gtest.h>

#include "MUQ/Optimization/Algorithms/LinearLeastSquares.h"
#include "MUQ/Utilities/LinearOperators/Operators/EigenLinOp.h"

#include "MUQ/Utilities/RandomGenerator.h"

using namespace muq::Utilities;
using namespace muq::Optimization;
using namespace std;

// set up a gtest test fixture for unconstrained deterministic solvers
class Optimization_LeastSquaresTests : public::testing::Test {
public:

  // the initial point to start the optimization
  Eigen::VectorXd d;
  Eigen::MatrixXd V;
  Eigen::VectorXd x;
  Eigen::VectorXd trueAlpha;

  Eigen::VectorXd normConsts;

  // constructor
  Optimization_LeastSquaresTests(int npts = 1e3)
  {
    // get the positions to evaluate the points
    x = RandomGenerator::GetNormalRandomVector(npts);

    trueAlpha = Eigen::VectorXd::Zero(4);
    trueAlpha << 0.5, 0.25, -0.125, 0.0125;

    // build the vandermond matrix
    V.resize(npts, 4);
    for (int i = 0; i < npts; ++i) {
      V(i, 0) = HermiteEval0(x(i));
      V(i, 1) = HermiteEval1(x(i));
      V(i, 2) = HermiteEval2(x(i));
      V(i, 3) = HermiteEval3(x(i));
    }

    d = V * trueAlpha + 1e-1 * RandomGenerator::GetNormalRandomVector(npts);

    // set the normalization constants
    normConsts = Eigen::VectorXd::Ones(4);
    normConsts << 0, 1, 2.0, 6.0;
  }

private:

  static double HermiteEval0(double x)
  {
    return 1;
  }

  static double HermiteEval1(double x)
  {
    return x;
  }

  static double HermiteEval2(double x)
  {
    return x * x - 1;
  }

  static double HermiteEval3(double x)
  {
    return x * (x * x - 3.0);
  }
};


TEST_F(Optimization_LeastSquaresTests, QRsolve)
{
  Eigen::VectorXd solAlpha = LinearLeastSquares::solve(V, d);

  EXPECT_NEAR(trueAlpha[0], solAlpha[0], 4e-2);
  EXPECT_NEAR(trueAlpha[1], solAlpha[1], 4e-2);
  EXPECT_NEAR(trueAlpha[2], solAlpha[2], 4e-2);
  EXPECT_NEAR(trueAlpha[3], solAlpha[3], 4e-2);
}


TEST_F(Optimization_LeastSquaresTests, LinOpSolve)
{
  auto AOp      = make_shared<EigenLinOp>(V);
  auto ATransOp = make_shared<EigenLinOp>(V.transpose());

  Eigen::VectorXd solAlpha = LinearLeastSquares::solve(AOp, ATransOp, d);

  EXPECT_NEAR(trueAlpha[0], solAlpha[0], 4e-2);
  EXPECT_NEAR(trueAlpha[1], solAlpha[1], 4e-2);
  EXPECT_NEAR(trueAlpha[2], solAlpha[2], 4e-2);
  EXPECT_NEAR(trueAlpha[3], solAlpha[3], 4e-2);
}


TEST_F(Optimization_LeastSquaresTests, QuadConstSolve)
{
  Eigen::MatrixXd B        = normConsts.asDiagonal();
  double sig               = 0.1;
  Eigen::VectorXd solAlpha = LinearLeastSquares::solve(V, d, B, sig);

  EXPECT_NEAR(sig,          solAlpha.transpose() * B * solAlpha, 1e-5);

  EXPECT_NEAR(trueAlpha[0], solAlpha[0],                         4e-2);
  EXPECT_NEAR(trueAlpha[1], solAlpha[1],                         4e-2);
  EXPECT_NEAR(trueAlpha[2], solAlpha[2],                         4e-2);
  EXPECT_NEAR(trueAlpha[3], solAlpha[3],                         4e-2);
}


TEST_F(Optimization_LeastSquaresTests, QuadConstSolve_Vector)
{
  double sig               = 0.1;
  Eigen::VectorXd solAlpha = LinearLeastSquares::solve(V, d, normConsts, sig);

  EXPECT_NEAR(sig,          solAlpha.transpose() * normConsts.asDiagonal() * solAlpha, 1e-5);

  EXPECT_NEAR(trueAlpha[0], solAlpha[0],                                               4e-2);
  EXPECT_NEAR(trueAlpha[1], solAlpha[1],                                               4e-2);
  EXPECT_NEAR(trueAlpha[2], solAlpha[2],                                               4e-2);
  EXPECT_NEAR(trueAlpha[3], solAlpha[3],                                               4e-2);
}
