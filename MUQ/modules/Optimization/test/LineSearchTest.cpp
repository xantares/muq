
#include "UnconstrainedTestClasses.h"

using namespace muq::Optimization;
using namespace std;


TEST_F(Optimization_QuadLineSearchTest, QuadSD)
{
  // set the method
  testParams.put("Opt.Method", "SD_Line");
}

TEST_F(Optimization_QuadLineSearchTest, QuadBFGS)
{
  // set the method
  testParams.put("Opt.Method", "BFGS_Line");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockSD)
{
  // set the method
  testParams.put("Opt.Method", "SD_Line");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockBFGS)
{
  // set the method
  testParams.put("Opt.Method", "BFGS_Line");
}

TEST_F(Optimization_QuadLineSearchTest, QuadNewton)
{
  // set the method
  testParams.put("Opt.Method", "Newton");
}

TEST_F(Optimization_RoseLineSearchTest, RosenbrockNewton)
{
  // set the method
  testParams.put("Opt.Method", "Newton");
}


