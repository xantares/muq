
##########################################
#  Objects to build


# files containing google tests
set(Test_Inference_Sources
     Inference/test/BasicMCMCtest.cpp
     Inference/test/BasicMapTest.cpp
     Inference/test/TransportMapTests.cpp
    
    PARENT_SCOPE
)


set(Local_Long_Sources
  Inference/test/ImportanceSamplerTest.cpp
  Inference/test/MCMCConvergenceTests.cpp
  Inference/test/TransportMapMCMCTests.cpp
)

if(MUQ_USE_NLOPT AND Approximation_build)
  set(Local_Long_Sources
    ${Local_Long_Sources}
  	  Inference/test/PseudoMarginalMCMCTest.cpp
  )   
endif()


set(LongTest_Inference_Sources
    ${Local_Long_Sources}
    PARENT_SCOPE
)





