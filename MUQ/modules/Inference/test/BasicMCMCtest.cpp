
#include "gtest/gtest.h"

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Utilities/mesh/StructuredQuadMesh.h"
#include "MUQ/Utilities/RandomGenerator.h"

#include "MUQ/Inference/TransportMaps/TransportMap.h"
#include "MUQ/Inference/TransportMaps/MapFactory.h"

// #include "MUQ/Geostats/PowerKernel.h"
//#include "MUQ/Inference/MCMC/pCN.h"
#include "MUQ/Inference/MCMC/MCMCBase.h"
#include <MUQ/Inference/MCMC/MCMCProposal.h>
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/GaussianPair.h"
#include "MUQ/Modelling/EmpiricalRandVar.h"
#include "MUQ/Utilities/EigenTestUtils.h"
#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Inference/ProblemClasses/InferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/MarginalInferenceProblem.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/ProblemClasses/GaussianFisherInformationMetric.h"
#include "MUQ/Inference/ProblemClasses/EuclideanMetric.h"

#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Modelling/ModPieceTemplates.h"
#include "MUQ/Modelling/VectorPassthroughModel.h"
#include "MUQ/Modelling/DensityProduct.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Utilities/HDF5Wrapper.h"
#include "MUQ/Modelling/LinearModel.h"
#include "MUQ/Modelling/SumModel.h"

using namespace muq::Modelling;
using namespace muq::Inference;
using namespace muq::Utilities;

// using namespace muq::geostats;
using namespace std;
using namespace Eigen;

#define MCMCTOL 1e-9


TEST(InferenceMCMCTest, MHProposal)
{
  // create a mean vector, filled with ones
  Eigen::VectorXd TempMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 1, 1, 1, 4;


  auto GaussTest = make_shared<SamplingProblem>(make_shared<GaussianDensity>(TempMean, trueCov));


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Steps", 1000);
  params.put("MCMC.BurnIn", 10);
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("Verbose", 0);
  auto mhProposal = MCMCProposal::Create(GaussTest, params);

  VectorXd currentVector(2);
  currentVector << 0.1, 3.4;
  auto currentState = GaussTest->ConstructState(currentVector, 0, nullptr);

  VectorXd nextVector(2);
  nextVector << 0.2, 1.4;
  auto nextState = GaussTest->ConstructState(nextVector, 0, nullptr);
  
  auto logEntry                            = make_shared<HDF5LogEntry>();
  std::shared_ptr<MCMCState> proposedState = mhProposal->DrawProposal(currentState, logEntry);

  EXPECT_NEAR(-3.842877066409345, mhProposal->ProposalDensity(currentState, nextState), 1e-7);
}

// this test just makes sure the MHKernel runs
TEST(InferenceMCMCTest, MHKernel)
{
  // create a mean vector, filled with ones
  Eigen::VectorXd TempMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 1, 1, 1, 4;


  auto GaussTest = make_shared<SamplingProblem>(make_shared<GaussianDensity>(TempMean, trueCov));


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Steps", 1000);
  params.put("MCMC.BurnIn", 10);
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("Verbose", 0);
  auto mhKernel = MCMCKernel::Create(GaussTest, params);

  VectorXd currentVector(2);
  currentVector << 0.1, 3.4;
  auto currentState = GaussTest->ConstructState(currentVector, 0, nullptr);

  bool accepted;
  std::shared_ptr<MCMCState> newState;
  auto logEntry = make_shared<HDF5LogEntry>();
  newState = mhKernel->ConstructNextState(currentState, 0, logEntry);

}

TEST(InferenceMCMCTest, MHLogging)
{
  // create a mean vector, filled with ones
  Eigen::VectorXd TempMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 1, 1, 1, 4;


  auto GaussTest = make_shared<SamplingProblem>(make_shared<GaussianDensity>(TempMean, trueCov));


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Proposal", "MHProposal");
  params.put("MCMC.Kernel", "MHKernel");


  params.put("MCMC.Steps", 1000 - 1);
  params.put("MCMC.BurnIn", 10);
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("HDF5.LogWriteFrequency", 100);
  params.put("Verbose", 0);
  params.put("MCMC.HDF5OutputGroup", "/test/mh/");
  params.put("MCMC.HDF5OutputDetails", true);
  
  HDF5Logging::Configure(params);
  HDF5Wrapper::OpenFile("results/tests/MCMCLogging.h5");

  auto TestSolver = MCMCBase::ConstructMCMC(GaussTest, params);

  // sample the distribution
  EmpRvPtr TestPost = TestSolver->Sample(TempMean);

  HDF5Logging::WipeBuffers();
  
  // make sure the logging worked
  Eigen::MatrixXd chain = HDF5Wrapper::ReadMatrix<double>("/test/mh/chain");
  EXPECT_EQ(2,chain.rows());
  EXPECT_EQ(1000,chain.cols());
  
  HDF5Wrapper::CloseFile();
}

TEST(InferenceMCMCTest, ManifoldMALAProposal)
{
  /** Same as MALA, but uses less samples because of the manifold. */

  // create a mean vector of dimension 10, filled with ones
  Eigen::VectorXd TempMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 4, 2, 2, 6;


  // create a Density pointer to a gaussian
  auto graph = std::make_shared<muq::Modelling::ModGraph>();

  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");
  graph->AddNode(std::make_shared<GaussianDensity>(TempMean, trueCov), "likelihood");
  graph->AddNode(std::make_shared<UniformDensity>(std::make_shared<UniformBox>(VectorXd::Constant(2,
                                                                                                 -1000),
                                                                              VectorXd::Constant(2, 1000))), "prior");
  graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");
  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "forwardModel");

  graph->AddEdge("inferenceTarget", "forwardModel", 0);
  graph->AddEdge("forwardModel", "likelihood", 0);
  graph->AddEdge("inferenceTarget", "prior", 0);
  graph->AddEdge("likelihood", "posterior", 0);
  graph->AddEdge("prior", "posterior", 1);


  auto metric =
    make_shared<GaussianFisherInformationMetric>(dynamic_pointer_cast<GaussianDensity>(graph->GetNodeModel(
                                                                                    "likelihood")),
                                                 graph->GetNodeModel("forwardModel"));

  auto GaussTest = make_shared<InferenceProblem>(graph, metric);


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Proposal", "MMALA");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Steps", 20000);
  params.put("MCMC.BurnIn", 200);
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("MCMC.MMALA.AdaptDelay", 20000);
  params.put("Verbose", 0);

  auto mmalaProposal = MCMCProposal::Create(GaussTest, params);

  VectorXd currentVector(2);
  currentVector << 0.1, 3.4;
  auto currentState = GaussTest->ConstructState(currentVector, 0, nullptr);

  auto logEntry                            = make_shared<HDF5LogEntry>();
  std::shared_ptr<MCMCState> proposedState = mmalaProposal->DrawProposal(currentState, logEntry);

  VectorXd nextVector(2);
  nextVector << 0.2, 1.4;
  auto nextState = GaussTest->ConstructState(nextVector, 0, nullptr);
  EXPECT_NEAR(-3.3901182031863408, mmalaProposal->ProposalDensity(currentState, nextState), 1e-7);
}

TEST(InferenceMCMCTest, PreMALAProposal)
{
  /** Same as MALA, but uses less samples because of the manifold. */

  // create a mean vector of dimension 10, filled with ones
  Eigen::VectorXd TempMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 4, 2, 2, 6;

  Eigen::VectorXd priorMean(2);
  priorMean << 0,0;
  
  // create a Density pointer to a gaussian
  auto graph = std::make_shared<muq::Modelling::ModGraph>();

  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "inferenceTarget");
  graph->AddNode(std::make_shared<GaussianDensity>(TempMean, trueCov), "likelihood");
  graph->AddNode(std::make_shared<GaussianDensity>(priorMean,10), "prior");
  
  graph->AddNode(std::make_shared<DensityProduct>(2), "posterior");
  graph->AddNode(std::make_shared<VectorPassthroughModel>(2), "forwardModel");

  graph->AddEdge("inferenceTarget", "forwardModel", 0);
  graph->AddEdge("forwardModel", "likelihood", 0);
  graph->AddEdge("inferenceTarget", "prior", 0);
  graph->AddEdge("likelihood", "posterior", 0);
  graph->AddEdge("prior", "posterior", 1);


  auto GaussTest = make_shared<InferenceProblem>(graph);

  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Proposal", "PreMALA");
  params.put("MCMC.Kernel", "MHKernel");
  params.put("MCMC.Steps", 20000);
  params.put("MCMC.BurnIn", 200);
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("MCMC.MMALA.AdaptDelay", 20000);
  params.put("Verbose", 0);

  auto premalaProposal = MCMCProposal::Create(GaussTest, params);

  VectorXd currentVector(2);
  currentVector << 0.1, 3.4;
  auto currentState = GaussTest->ConstructState(currentVector, 0, nullptr);
 
  EXPECT_TRUE(GaussTest->ComputesPriorGrad());
  
  auto currentGrad = currentState->GetDensityGrad();
  auto likelyGrad = currentState->GetLikelihoodGrad();
  EXPECT_FALSE(currentGrad(0)==likelyGrad(0));
  EXPECT_FALSE(currentGrad(1)==likelyGrad(1));
  
  EXPECT_NEAR(0.5,currentGrad(0),1e-7);
  EXPECT_NEAR(-0.91,currentGrad(1),1e-7);
  
  VectorXd nextVector(2);
  nextVector << 0.2, 1.4;
  auto nextState = GaussTest->ConstructState(nextVector, 0, nullptr);
  EXPECT_NEAR(-3.0426395664093455, premalaProposal->ProposalDensity(currentState, nextState), 1e-7);
}

// This test just makes sure the DRKernel runs
TEST(InferenceMCMCTest, DRKernel)
{
  // create a mean vector, filled with ones
  Eigen::VectorXd TempMean = Eigen::VectorXd::Ones(2);
  Eigen::MatrixXd trueCov(2, 2);

  trueCov << 1, 1, 1, 4;


  auto GaussTest = make_shared<SamplingProblem>(make_shared<GaussianDensity>(TempMean, trueCov));


  // create an MCMC instance
  boost::property_tree::ptree params;
  params.put("MCMC.Method", "SingleChainMCMC");
  params.put("MCMC.Kernel", "DR");
  params.put("MCMC.DR.stages", 3);
  params.put("MCMC.DR.ProposalSpecification", "DRAM");
  params.put("MCMC.Steps", 1000);
  params.put("MCMC.BurnIn", 10);
  params.put("MCMC.MHProposal.PropSize", 1);
  params.put("Verbose", 0);
  auto dramKernel = MCMCKernel::Create(GaussTest, params);

  VectorXd currentVector(2);
  currentVector << 0.1, 3.4;
  auto currentState = GaussTest->ConstructState(currentVector, 0, nullptr);

  std::shared_ptr<MCMCState> newState, nextState;
  auto logEntry = make_shared<HDF5LogEntry>();
  newState = dramKernel->ConstructNextState(currentState, 1, logEntry);

}
