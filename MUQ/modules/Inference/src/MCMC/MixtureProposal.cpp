
#include "MUQ/Inference/MCMC/MixtureProposal.h"

// standard library includes
#include <string>

// boost includes
#include <boost/property_tree/ptree.hpp>

// other MUQ includes
#include "MUQ/Utilities/LogConfig.h"
#include "MUQ/Modelling/RandVarBase.h"

#include <boost/foreach.hpp>

#include "MUQ/Utilities/HDF5Wrapper.h"


// namespaces
using namespace std;
using namespace muq::Inference;
using namespace muq::Utilities;
using namespace muq::Modelling;


REGISTER_MCMCPROPOSAL(MixtureProposal);


MixtureProposal::MixtureProposal(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                                 boost::property_tree::ptree            & properties) : MCMCProposal(samplingProblem,
                                                                                                     properties)
{
  int verboseLevel = properties.get("Verbose", 0);

  if (verboseLevel > 0) {
    std::cout << "Constructing Mixture Proposal\n";
  }

  LOG(INFO) << "Constructing Mixture Proposal";

  // first, figure out how many components of the mixture there are and store their names
  int numComponents = 0;
  std::set<std::string> propNames;
  BOOST_FOREACH(const boost::property_tree::ptree::value_type & v, properties.get_child("MCMC.MixtureProposal"))
  {
    if (v.first.length() > 9) {
      if (v.first.substr(0, 9).compare("Proposal_") == 0) {
        ++numComponents;
        propNames.insert(v.first.substr(0, v.first.find(".")));
      }
    }
  }
  bool foundUniqueProposalNames = propNames.size() == numComponents;
  assert(foundUniqueProposalNames);

  // at this point, the names are in alpha-numeric order and will be called by DR in this order
  LOG(INFO) << "MCMC.MixtureProposal.numProps = " << numComponents;

  proposals.resize(numComponents);
  proposalWeights.resize(numComponents);

  // create each of the proposals
  int component = 0;
  for (auto iter = propNames.begin(); iter != propNames.end(); ++iter) {
    boost::property_tree::ptree subProperties;

    std::string extractPath = "MCMC.MixtureProposal." + *iter;
    subProperties.put_child("MCMC", properties.get_child(extractPath));

    proposalWeights(component) = properties.get(extractPath + ".Weight", 1.0);

    proposals.at(component) = MCMCProposal::Create(samplingProblem, subProperties);
    assert(proposals.at(component));

    ++component;
  }

  // normalize the weights so they add to 1
  double totalWeight = proposalWeights.sum();
  proposalWeights /= totalWeight;
}

/** Function performs one MCMC step using the fixed proposal and simple accept/reject stage through the
 *  Metropolis-Hastings Rule */
std::shared_ptr<muq::Inference::MCMCState> MixtureProposal::DrawProposal(
  std::shared_ptr<muq::Inference::MCMCState>    currentState,
  std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
  int const currIteration)
{
  // first get a uniform random number
  double u = RandomGenerator::GetUniform();

  double weightSum = 0.0;

  for (int i = 0; i < proposals.size() - 1; ++i) {
    weightSum += proposalWeights(i);
    if (u < weightSum) {
      logEntry->loggedData["mixtureComponent"] = Eigen::VectorXd::Constant(1, i);
      return proposals.at(i)->DrawProposal(currentState, logEntry);
    }
  }

  logEntry->loggedData["mixtureComponent"] = Eigen::VectorXd::Constant(1, proposals.size() - 1);
  return proposals.at(proposals.size() - 1)->DrawProposal(currentState, logEntry);
}

double MixtureProposal::ProposalDensity(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                        std::shared_ptr<muq::Inference::MCMCState> proposedState)
{
    assert(proposedState);
    assert(currentState);

  double result = 0;
  for (int i = 0; i < proposals.size(); ++i) {
    result += proposalWeights(i) * proposals.at(i)->ProposalDensity(currentState, proposedState);
  }

  return result;
}

void MixtureProposal::SetWeights(const Eigen::VectorXd& newWeights)
{
    assert(newWeights.size() == proposalWeights.size());
  double weightSum = newWeights.sum();
  proposalWeights = newWeights / weightSum;
}

void MixtureProposal::WriteAttributes(std::string const& groupName, std::string prefix) const
{
  HDF5Wrapper::WriteStringAttribute(groupName, prefix + "Proposal", "Mixture");

  HDF5Wrapper::WriteScalarAttribute<unsigned>(groupName, prefix + "Mixture - Number of components", proposals.size());

  // now, loop through the proposals and get their attributes
  for (int i = 0; i < proposals.size(); ++i) {
    std::stringstream newPrefix;
    newPrefix << prefix << "Mix Proposal " << i << " - ";
    proposals.at(i)->WriteAttributes(groupName, newPrefix.str());
    HDF5Wrapper::WriteScalarAttribute(groupName, newPrefix.str() + "Weight", proposalWeights(i));
  }
}
