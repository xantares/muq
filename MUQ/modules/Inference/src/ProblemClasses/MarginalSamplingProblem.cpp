#include "MUQ/Inference/ProblemClasses/MarginalSamplingProblem.h"

#include "MUQ/Inference/MCMC/SingleChainMCMC.h"
#include "MUQ/Modelling/ModGraphPiece.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Approximation;
using namespace muq::Inference;

shared_ptr<MCMCState> MarginalSamplingProblem::ConstructState(Eigen::VectorXd const& state, int const currIteration, shared_ptr<HDF5LogEntry> logEntry)
{
  assert(densityEstimator->outputSize == 1);
  double logLikelihood = LogLikelihood(state, currIteration, logEntry);

  boost::optional<Eigen::VectorXd> likelihoodGrad;
  boost::optional<double> logPrior; //only used for inference problems
  boost::optional<Eigen::MatrixXd> metric;
  boost::optional<Eigen::VectorXd> priorGrad;

  //if it's not valid, return null
  if (!isfinite(logLikelihood)) {
    return nullptr;
  }

  //compute gradient as desired
  if (useLikelihoodGrad) {
    likelihoodGrad = boost::optional<Eigen::VectorXd>(density->Gradient(state, Eigen::VectorXd::Constant(1, 1.0), 0));
  } else {
    likelihoodGrad = boost::optional<Eigen::VectorXd>();
  }

  //compute metric as desired
  if (useMetric) {
    metric = boost::optional<Eigen::MatrixXd>(metricObject->ComputeMetric(state));
  } else {
    metric = boost::optional<Eigen::MatrixXd>();
  }

  //note there's no forward model
  auto mcmcState = make_shared<MCMCState>(state, logLikelihood, likelihoodGrad, logPrior, priorGrad, metric,  boost::optional<Eigen::VectorXd>());
  assert(mcmcState);

  return mcmcState;
}

double MarginalSamplingProblem::LogLikelihood(Eigen::VectorXd const& state, int const currIteration, shared_ptr<HDF5LogEntry> logEntry)
{
  // make sure the importance sampler has been constructed
  assert(densityEstimator);
  assert(densityEstimator->outputSize == 1);

  // if the approximator has not been constructed 
  if( !approximator ) {
    // use pseudo marginal
    return log(densityEstimator->Evaluate(state) (0));
  }

  // make sure the output size is one
  assert(approximator->outputSize == 1);

  // the number of nearest neighbors used by the approximation
  const int newKN = (int)(knBase + pow(numRefined, knExp));
 
  // while there aren't enough points
  while( !approximator->ResetNumNearestNeighbors(newKN) ) {
    // refine 
    approximator->RefineNear(state);
    
    // add to the number of refinements
    ++numRefined;
  }

  // random probability refinement 
  const double beta = betaBase * pow((float) currIteration, betaExp);
  const double betaDraw = uniform->Sample() (0);
  if( logEntry ) {
    logEntry->loggedData["beta"] = beta * Eigen::VectorXd::Ones(1);
    logEntry->loggedData["betaDraw"] = betaDraw * Eigen::VectorXd::Ones(1);
  }

  // error indicator refinement 
  const double gamma = gammaBase * pow((double) currIteration, gammaExp);
  if( logEntry ) {
    logEntry->loggedData["gamma"] = gamma * Eigen::VectorXd::Ones(1);
  }

  // compute the error indicator 
  const double errInd = approximator->ErrorIndicator(state);
  if( logEntry ) {
    logEntry->loggedData["errorIndicator"] = errInd * Eigen::VectorXd::Ones(1);
  }

  // a uniform([0.0, 1.0]) sample is less than beta or the error indictor triggers refinement
  if( (betaDraw < beta) || (errInd > gamma) ) {
    // refine near the current state 
    approximator->RefineNear(state);

    // add to the number of refinements
    ++numRefined;
  }

  if( logEntry ) { 
    // store how many refinements
    logEntry->loggedData["refined"] = numRefined * Eigen::VectorXd::Ones(1);
  }

  // return the log likelihood (approximation)
  return log(approximator->Evaluate(state) (0));
}

shared_ptr<PolynomialApproximator> MarginalSamplingProblem::GetApproximator() const 
{
  return approximator;
}
