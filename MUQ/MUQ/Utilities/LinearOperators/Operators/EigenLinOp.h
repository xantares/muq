
#ifndef _EigenLinOp_h
#define _EigenLinOp_h

#include "MUQ/Utilities/LinearOperators/Operators/LinOpBase.h"

namespace muq {
namespace Utilities {
class EigenLinOp : public LinOpBase {
public:

  template<typename derived>
  EigenLinOp(const Eigen::DenseBase<derived>& Ain) : LinOpBase(Ain.rows(), Ain.cols()), A(Ain) {}

  virtual Eigen::VectorXd apply(const Eigen::VectorXd& x) override;

protected:

  Eigen::MatrixXd A;
};

// class EigenLinOp
} // namespace Utilities
} // namespace muq
#endif // ifndef _EigenLinOp_h
