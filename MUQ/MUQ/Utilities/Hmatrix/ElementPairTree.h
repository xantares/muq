
#ifndef _ElementPairTree_h
#define _ElementPairTree_h

#include "MUQ/Utilities/mesh/Mesh.h"


namespace muq {
namespace Utilities {
template<unsigned int dim>
class HMatrix;

template<unsigned int dim>
class ElementPairNode;

template<unsigned int dim>
class ElementTree;


template<unsigned int dim>
class ElementPairTree {
public:

  friend class ElementPairNode<dim>;
  friend class HMatrix<dim>;

  // construct the pair tree from a mesh
  ElementPairTree(double eta, std::shared_ptr < Mesh < dim >>& MeshIn);

  // store the root node
  std::shared_ptr < ElementPairNode < dim >> root;

private:

  // store the ElementTree this pairTree was derived from
  std::shared_ptr < ElementTree < dim >> ClusterTree;
};
} // namespace Utilties
} // namespace muq

#endif // ifndef _ElementPairTree_h
