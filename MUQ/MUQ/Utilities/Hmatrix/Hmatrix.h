
#ifndef _Hmatrix_h
#define _Hmatrix_h


// standard library includes
#include <memory>
#include <iostream>
#include <vector>

// other external library includes
#include <Eigen/Core>

#include "MUQ/Geostatistics/CovKernel.h"


namespace muq {
namespace Utilities {
template<unsigned int dim>
class ElementPairTree;

/** A hierarchical matrix. */
template<unsigned int dim>
class HMatrix {
public:

  /** Construct a hierarchical approximation to the covariance matrix using a covariance kernel and mesh.
   *  @param[in] KernPtr Pointer to a covariance kernel
   *  @param[in] MeshPtr Pointer to a mesh
   */
  HMatrix(muq::Geostatistics::CovKernelPtr& KernPtr, std::shared_ptr < Mesh < dim >>& MeshPtr);

  /** Apply this hierarchical matrix to a vector and return the result.
   *  @param[in] x The input vector
   *  @return b=Ax
   */
  Eigen::VectorXd MatVec(const Eigen::VectorXd& x) const;


  Eigen::VectorXd operator*(const Eigen::VectorXd& x) const
  {
    return MatVec(x);
  }

  int rows() const;
  int cols() const;

private:

  std::shared_ptr < ElementPairTree < dim >> Tree;
};

// class Hmatrix
} // namespace Utilities
} // namespace muq
#endif // ifndef _Hmatrix_h
