
#ifndef VectorTranslater_h_
#define VectorTranslater_h_

// muq configuration file
#include "MUQ/config.h"

// standard library includes
#include <vector>
#include <memory>
#include <type_traits>
#include <iostream>

// external library includes
#include <Eigen/Core>

#if MUQ_Armadillo
# include <armadillo>
#endif // if HAVE_ARMADILLO == 1

namespace muq {
namespace Utilities {
template<typename ScalarTypeNative, typename ScalarTypeForeign>
ScalarTypeNative ConvertScalar(const ScalarTypeForeign& v2)
{
  ScalarTypeNative temp = v2;

  return temp;
}

/** \class EigenTranslater
 *  \brief A simple wrapper around the more general Translater class for use with Eigen::VectorXd
 *  \author Matthew Parno
 */
class EigenTranslater {
public:

  /** Return a pointer to the Eigen::VectorXd*
   *  @return A pointer to a Eigen::VectorXd translated from constructor input
   */
  virtual Eigen::VectorXd* GetEigenPtr() = 0;
};

/** \brief General deep copy
 *  \author Matthew Parno
 *
 *  This general template function should performs a deep copy defined for types T1 and T2.  Note that without any
 *template specialization, this function requires that the types T1 and T2 have:
 *  <ul>
 *  <li> A resize function that resizes the vector </li>
 *  <li> A square bracket ([]) access operator that returns vector elements. </li>
 *  </ul>
 *  Vector types that satisfy these requirements include
 *  <ul>
 *  <li> Eigen::VectorXd</li>
 *  <li> std::vector<double> </li>
 *  <li> arma::vec, arma::rowvec, and arma::colvec </li>
 *  </ul>
 */
template<typename T1, typename T2>
void VectorTranslate(const T1& Input, T2& Output, int size)
{
  // resize the output vector
  Output.resize(size);

  // copy the information
  //std::cout << "Printing iteration:\n";
  for (int i = 0; i < size; ++i) {
    //  std::cout << i << " / " << size << std::endl;
    Output[i] =
      ConvertScalar<typename std::remove_const<typename std::remove_reference<decltype(Output[0])>::type>::type,
                    typename std::remove_const<typename std::remove_reference<decltype(Input[0])>::type>::type>(
        Input[i]);
  }
}

template<typename T1>
void VectorTranslate(const T1& Input, double *& Output, int size)
{
  // copy the information
  for (int i = 0; i < size; ++i) {
    Output[i] = Input[i];
  }
}

/** \class Translater
 *  \author Matthew Parno
 *  \todo When non-const object is passed to constructor, use a shallow copy for double pointers. Throughout MUQ, we may
 *work with many different vector types.  This class provides a convenient way of translating between vector types. This
 *has been tested with the following vector types:
 *  <ul>
 *  <li> Eigen::VectorXd</li>
 *  <li> std::vector<double> </li>
 *  <li> arma::vec and arma::colvec </li>
 *  </ul>
 *
 *  To get a feel of how this class is typically used.  Consider two functions:
 *  \code{.cpp}
 *  void func(VecType1 &InOut);
 *  void func(VecType2 &InOut);
 *  \endcode These functions should perform the same operation on the input InOut but we only want to implement the
 *method details the second function for vectors of type VecType2.  To do this, the first function could be implemented
 *using this translater class:
 *  \code{.cpp}
 *  void func(VecType1 &InOut){
 *   Translater<VecType1,VecType2> Trans(InOut,true);
 *   func(*Trans.GetPtr());
 *  }
 *  \endcode The template arguments tell us that the translater will convert a vector from VecType1 to VecType2.  The
 *second argument of the constructor tells us that we want the InOut vector of VecType1 to be updated at the end of the
 *function.  The update is performed in the destructor of Trans.  If you have a different set of function with constant
 **reference arguments:
 *  \code{.cpp}
 *  void func(const VecType1 &InOut);
 *  void func(const VecType2 &InOut);
 *  \endcode Then you would use the Translater class without copying the results:
 *  \code{.cpp}
 *  void func(const VecType1 &InOut){
 *  Translater<VecType1,VecType2> Trans(InOut);
 *  func(*Trans.GetPtr());
 *  }
 *  \endcode
 *
 */
template<typename T1, typename T2>
class Translater : public EigenTranslater {
public:

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const T1& vec1)
  {
    CopyBack = false; // we do not need to copy the contents because vec2=vec1

    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from T1 to T2
  }

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const T1& vec1, int VecSizeIn)
  {
    CopyBack = false; // we do not need to copy the contents because the input is const

    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from T1 to T2
  }

  /** Usual Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(T1& vec1, bool CopyBackIn)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = &vec1;

    if ((CopyBackIn) && (std::is_same<T1, T2>::value)) {
      CopyBack = false;

      vec2ptr = reinterpret_cast<T2 *>(vec1ptr);
    } else {
      CopyBack = CopyBackIn;
      ForwardTranslate(); // translate from T1 to T2
    }
  }

  /** More general constructor that also works for double pointer inputs but requires vector size
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] VecSizeIn The number of elements in the vector
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(T1& vec1, int VecSizeIn, bool CopyBackIn = false)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = &vec1;

    if ((CopyBackIn) && (std::is_same<T1, T2>::value)) {
      CopyBack = false;

      vec2ptr = reinterpret_cast<T2 *>(vec1ptr);
    } else {
      CopyBack = CopyBackIn;
      ForwardTranslate(); // translate from T1 to T2
    }
  }

  /** Destructor. */
  ~Translater()
  {
    if (CopyBack) {
      BackwardTranslate();
    }
  }

  /** Return a pointer to the translated vector.
   *  @return A pointer to a T2 vector translated from constructor input
   */
  T2* GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    if (std::is_same<T2, Eigen::VectorXd>::value) {
      return reinterpret_cast<Eigen::VectorXd *>(vec2ptr);
    } else {
      std::cerr << "ERROR: Attempt to access Eigen::VectorXd pointer from different type.\n";
      throw("Invalid template parameter.");
      return nullptr;
    }
    return nullptr;
  }

private:

  /** Translate the T1 type vector to an internally stored T2 type vector. */
  void ForwardTranslate() // strange to native
  {
    VectorTranslate(*vec1ptr, this->vec2, this->VecSize);
    this->vec2ptr = &(this->vec2);
  }

  /** Translate the potentially changed T2 type vector to a T1 type vector. */
  void BackwardTranslate() // native to strange
  {
    VectorTranslate(vec2, *vec1ptr, VecSize);
  }

  // keep a pointer to the original untranslated vector
  T1 *vec1ptr;

  // keep a pointer tot he translated vector
  T2 *vec2ptr;

  // in some cases we will create a vector of type T2 to hold the new vector
  T2 vec2;

  // store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;

  // during destruction, should we copy the contents of the translated vector back to the original? default is false
  bool CopyBack;
};

// end of class translater


template<typename T1>
class Translater<T1, double *> : public EigenTranslater {
public:

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const T1& vec1)
  {
    CopyBack = false; // we do not need to copy the contents because vec2=vec1

    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from T1 to T2
  }

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const T1& vec1, int VecSizeIn)
  {
    CopyBack = false; // we do not need to copy the contents because vec2=vec1

    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from T1 to T2
  }

  /** Usual Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(T1& vec1, bool CopyBackIn)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = &vec1;

    if (CopyBackIn) {
      CopyBack = false;
      vec2     = &vec1[0];
      vec2ptr  = &vec2;
    } else {
      CopyBack = CopyBackIn;
      ForwardTranslate(); // translate from T1 to T2
    }
  }

  /** More general constructor that also works for double pointer inputs but requires vector size
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] VecSizeIn The number of elements in the vector
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(T1& vec1, int VecSizeIn, bool CopyBackIn = false)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = &vec1;

    if (CopyBackIn) {
      CopyBack = false;

      vec2    = &vec1[0];
      vec2ptr = &vec2;
    } else {
      CopyBack = CopyBackIn;
      ForwardTranslate(); // translate from T1 to T2
    }
  }

  /** Destructor. */
  ~Translater()
  {
    if (CopyBack) {
      BackwardTranslate();
      delete[] vec2;
    }
  }

  /** Return a pointer to the translated vector.
   *  @return A pointer to a T2 vector translated from constructor input
   */
  double** GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    std::cerr << "ERROR: Attempt to access Eigen::VectorXd pointer from different type.\n";

    throw("Invalid template parameter.");
    return nullptr;
  }

private:

  /** Translate the T1 type vector to an internally stored T2 type vector. */
  void ForwardTranslate() // strange to native
  {
    vec2 = new double[VecSize];
    VectorTranslate(*vec1ptr, vec2, VecSize);
    vec2ptr = reinterpret_cast<double **>(&vec2);
  }

  /** Translate the potentially changed T2 type vector to a T1 type vector. */
  void BackwardTranslate() // native to strange
  {
    VectorTranslate(vec2, *vec1ptr, VecSize);
  }

  // keep a pointer to the original untranslated vector
  T1 *vec1ptr;

  // keep a pointer tot he translated vector
  double **vec2ptr;

  // in some cases we will create a vector of type T2 to hold the new vector
  double *vec2;

  // store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;

  // during destruction, should we copy the contents of the translated vector back to the original? default is false
  bool CopyBack;
};

// end of class double* translater

/** Specialized class for working only with Eigen vectors. */
template<>
class Translater<Eigen::VectorXd, Eigen::VectorXd> : public EigenTranslater {
public:

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const Eigen::VectorXd& vec1)
  {
    CopyBack = false; // we cannot copy the contents back because the input is const

    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = const_cast<Eigen::VectorXd *>(&vec1);

    vec2    = vec1;
    vec2ptr = &vec2;
  }

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const Eigen::VectorXd& vec1, int VecSizeIn)
  {
    CopyBack = false; // we cannot copy the contents back because the input is const

    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<Eigen::VectorXd *>(&vec1);

    vec2    = vec1;
    vec2ptr = &vec2;
  }

  /** Usual Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(Eigen::VectorXd& vec1, bool CopyBackIn)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = &vec1;

    if (CopyBackIn) {
      CopyBack = false;
      vec2ptr  = reinterpret_cast<Eigen::VectorXd *>(vec1ptr);
    } else {
      CopyBack = false;
      vec2     = vec1;
      vec2ptr  = &vec2;
    }
  }

  /** More general constructor that also works for double pointer inputs but requires vector size
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] VecSizeIn The number of elements in the vector
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(Eigen::VectorXd& vec1, int VecSizeIn, bool CopyBackIn = false)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = &vec1;

    if (CopyBackIn) {
      CopyBack = false;
      vec2ptr  = reinterpret_cast<Eigen::VectorXd *>(vec1ptr);
    } else {
      CopyBack = false;
      vec2     = vec1;
      vec2ptr  = &vec2;
    }
  }

  /** Destructor. */
  ~Translater() {}

  /** Return a pointer to the translated vector.
   *  @return A pointer to a T2 vector translated from constructor input
   */
  Eigen::VectorXd* GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    return reinterpret_cast<Eigen::VectorXd *>(vec2ptr);
  }

private:

  // keep a pointer to the original untranslated vector
  Eigen::VectorXd *vec1ptr;

  // keep a pointer tot he translated vector
  Eigen::VectorXd *vec2ptr;

  // in some cases we will create a vector of type T2 to hold the new vector
  Eigen::VectorXd vec2;

  // store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;

  // during destruction, should we copy the contents of the translated vector back to the original? default is false
  bool CopyBack;
};

// end of class translater


#if MUQ_Armadillo

template<typename T1>
class Translater<T1, arma::vec> : public EigenTranslater {
public:

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const T1& vec1)
  {
    CopyBack = false; // we do not need to copy the contents because vec2=vec1

    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from T1 to T2
  }

  /** Const Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   */
  Translater(const T1& vec1, int VecSizeIn)
  {
    CopyBack = false; // we do not need to copy the contents because vec2=vec1

    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = const_cast<T1 *>(&vec1);

    // check the template types, if they are the same, just set the translated pointer to the input pointer
      ForwardTranslate(); // translate from T1 to T2
  }

  /** Usual Constructor
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(T1& vec1, bool CopyBackIn)
  {
    // store the vector size
    VecSize = vec1.size();

    // store the location of the first vector
    vec1ptr = &vec1;

    if (CopyBackIn) {
      CopyBack = false;

      vec2    = arma::vec(&vec1[0], VecSize, false);
      vec2ptr = &vec2;
    } else {
      CopyBack = CopyBackIn;
      ForwardTranslate(); // translate from T1 to T2
    }
  }

  /** More general constructor that also works for double pointer inputs but requires vector size
   *  @param[in] vec1 A vector of type T1 that we wish to translate to a vector of type T2.
   *  @param[in] VecSizeIn The number of elements in the vector
   *  @param[in] CopyBackIn  During destruction, should we copy the contents of the translated vector back to the
   *original?  If CopyBackIn==false, then no copy will be performed.  If CopyBackIn==true, the contents of the
   *translated vector will be copied back to vec1.
   */
  Translater(T1& vec1, int VecSizeIn, bool CopyBackIn = false)
  {
    // store the vector size
    VecSize = VecSizeIn;

    // store the location of the first vector
    vec1ptr = &vec1;

    if (CopyBackIn) {
      CopyBack = false;

      vec2    = arma::vec(&vec1[0], VecSize, false);
      vec2ptr = &vec2;
    } else {
      CopyBack = CopyBackIn;
      ForwardTranslate(); // translate from T1 to T2
    }
  }

  /** Destructor. */
  ~Translater() {}

  /** Return a pointer to the translated vector.
   *  @return A pointer to a T2 vector translated from constructor input
   */
  arma::vec* GetPtr()
  {
    return vec2ptr;
  }

  virtual Eigen::VectorXd* GetEigenPtr()
  {
    std::cerr << "ERROR: Attempt to access Eigen::VectorXd pointer from different type.\n";

    throw("Invalid template parameter.");
    return nullptr;
  }

private:

  /** Translate the T1 type vector to an internally stored T2 type vector. */
  void ForwardTranslate() // strange to native
  {
    vec2          = arma::vec(&((*vec1ptr)[0]), VecSize, true);
    this->vec2ptr = &(this->vec2);
  }

  // keep a pointer to the original untranslated vector
  T1 *vec1ptr;

  // keep a pointer tot he translated vector
  arma::vec *vec2ptr;

  // in some cases we will create a vector of type T2 to hold the new vector
  arma::vec vec2;

  // store the size of the vector -- this is important for handling double pointers
  unsigned int VecSize;

  // during destruction, should we copy the contents of the translated vector back to the original? default is false
  bool CopyBack;
};

// end of class translater


#endif // if HAVE_ARMADILLO == 1
} // namespace GeneralUtilties
} // namespace muq


#endif // ifndef VectorTranslater_h_
