//MIT UQ Library
//Copyright (C) 2012 Patrick R. Conrad
//
//This program is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <string>
#include <memory>

#include <boost/serialization/access.hpp>

namespace muq {
 namespace Utilities {

class RecursivePolynomialFamily1D;
class QuadratureFamily1D;

///A class for defining the properties of a dimension/variable
/**
 @class Variable
 @ingroup Quadrature
 @ingroup PolynomialChaos
 @brief A structure to hold a name, polynomial family, and quadrature family.
 @details This structure holds the definition of a variable, in that it holds
 * a family of polynomials and/or quadrature, which specifies a domain
 * and probability weighting.  This class is employed in
 */
class Variable {
public:

  typedef std::shared_ptr<Variable> Ptr;

  Variable();

  /** Constructor that takes a name and polynomial family, but sets the quadrature family to nullptr.
   * @param[in] name a string
   * @param[in] polyFamily Pointer to the polynomial 1D family or nullptr.
   */
  Variable(std::string                                  name,
           std::shared_ptr<RecursivePolynomialFamily1D> polyFamily);
  
  /** Constructor that takes a name and quadrature family, but sets the polynomial family to nullptr.
   * @param[in] name a string
   * @param[in] quadFamily Pointer to the quadrature 1D family or nullptr.
   */
  Variable(std::string                                  name,
           std::shared_ptr<QuadratureFamily1D>          quadFamily);
  
  
  /** Constructor that sets the name, polynomial family, and quadrature family.
   * @param[in] name a string
   * @param[in] polyFamily Pointer to the polynomial 1D family or nullptr.
   * @param[in] quadFamily Pointer to the quadrature 1D family or nullptr.
   */
  Variable(std::string                                  name,
           std::shared_ptr<RecursivePolynomialFamily1D> polyFamily,
           std::shared_ptr<QuadratureFamily1D>          quadFamily);

  /** Default virtual destructor. */
  virtual ~Variable() = default;

  std::shared_ptr<RecursivePolynomialFamily1D> polyFamily;
  std::shared_ptr<QuadratureFamily1D> quadFamily;

  ///A name for the variable.
  std::string name;

private:

  friend class boost::serialization::access;

  template<class Archive>
  void serialize(Archive& ar, const unsigned int version);
};
}
}

#endif /* VARIABLE_H_ */
