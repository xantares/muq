
#ifndef SQUAREMESH_H_
#define SQUAREMESH_H_

#include "MUQ/Pde/GenericMesh.h"

namespace muq {
namespace Pde {
/// Constructed a generic square mesh
  /**
     A square mesh with regularly spaced points.
   */ 
class SquareMesh : public GenericMesh {
public:

  virtual ~SquareMesh() = default;

  /**
     Construct a regular mesh for a \f$[B_x, L_x] \times [B_y, L_y]\f$ square mesh. 
     Ptree options:
     -# The right boundary in the \f$x\f$ and \f$y\f$ direction
       - The \f$x\f$ direction (<EM>mesh.Lx</EM>)
       - The \f$y\f$ direction (<EM>mesh.Ly</EM>)
       - Both default to zero
     -# The left boundary in the \f$x\f$ and \f$y\f$ direction
       - The \f$x\f$ direction (<EM>mesh.Bx</EM>)
       - The \f$y\f$ direction (<EM>mesh.By</EM>)
       - Both default to zero
     -# The number of points in the \f$x\f$ and \f$y\f$ direction
       - The \f$x\f$ direction (<EM>mesh.Nx</EM>)
       - The \f$y\f$ direction (<EM>mesh.Ny</EM>)
       - Both default to zero, which is invalid
   */
  SquareMesh(boost::property_tree::ptree const& params);

private:

};

//DEFINE_MESH(SquareMesh)
} // namespace Pde
} // namespace muq

#endif // ifndef SQUAREMESH_H_
