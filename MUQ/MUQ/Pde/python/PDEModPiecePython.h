#ifndef PDEMODPIECEPYTHON_H_
#define PDEMODPIECEPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Pde/PDEModPiece.h"

namespace muq {
namespace Pde {
/// Nonmember function to construct nontransient versions of muq::Pde::PDEModPiece
/**
   Assumes a point observer (muq::GenericPDE::PointObserver) on the system solution.
   @param[in] inputSizes The input sizes of the system parameters
   @param[in] system System hold the domain mesh
   @param[in] param All the parameters for the system and the solver
   \return A nontransient version of muq::Pde::PDEModPiece
 */
  std::shared_ptr<PDEModPiece<muq::Modelling::SolverModPiece> > NontransientConstructPDE(boost::python::list const& inputSizes, std::shared_ptr<muq::Pde::GenericEquationSystems> const& system, boost::python::dict& param);
 
/// Nonmember function to construct nontransient versions of muq::Pde::PDEModPiece
/**
   Assumes a point observer (muq::GenericPDE::PointObserver) on the system solution.
   @param[in] inputSizes The input sizes of the system parameters
   @param[in] pnts The points in the domain to observe
   @param[in] system System hold the domain mesh
   @param[in] param All the parameters for the system and the solver
   \return A nontransient version of muq::Pde::PDEModPiece
 */
  std::shared_ptr<PDEModPiece<muq::Modelling::SolverModPiece> > NontransientConstructPDEPoints(boost::python::list const& inputSizes, boost::python::list const& pnts, std::shared_ptr<muq::Pde::GenericEquationSystems> const& system, boost::python::dict& param);

/// Nonmember function to construct nontransient versions of muq::Pde::PDEModPiece
/**
   Assumes a point observer (muq::Pde::PointObserver) on the system solution.
   @param[in] inputSizes The input sizes of the system parameters
   @param[in] system System hold the domain mesh
   @param[in] param All the parameters for the system and the solver
   \return A nontransient version of muq::Pde::PDEModPiece
 */
  std::shared_ptr<PDEModPiece<muq::Modelling::OdeModPiece> > TransientConstructPDE(boost::python::list const& inputSizes, boost::python::list const& obsTimes, std::shared_ptr<muq::Pde::GenericEquationSystems> const& system, boost::python::dict& param);

/// Nonmember function to construct nontransient versions of muq::Pde::PDEModPiece
/**
   Assumes a point observer (muq::Pde::PointObserver) on the system solution.
   @param[in] inputSizes The input sizes of the system parameters
   @param[in] pnts The points in the domain to observe
   @param[in] system System hold the domain mesh
   @param[in] param All the parameters for the system and the solver
   \return A nontransient version of muq::Pde::PDEModPiece
*/
  std::shared_ptr<PDEModPiece<muq::Modelling::OdeModPiece> > TransientConstructPDEPoints(boost::python::list const& inputSizes, boost::python::list const& pnts, boost::python::list const& obsTimes, std::shared_ptr<muq::Pde::GenericEquationSystems> const& system, boost::python::dict& param);

 void ExportPDE();
} // namespace Pde
} // namespace muq

#endif // ifndef PDEMODPIECEPYTHON_H_
