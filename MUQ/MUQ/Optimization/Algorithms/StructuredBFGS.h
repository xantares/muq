
#ifndef _StructuredBFGS_h
#define _StructuredBFGS_h

#include "MUQ/Optimization/Algorithms/BFGS.h"

namespace muq {
namespace Optimization {
/** @class StructuredBFGS
 *  @ingroup Optimization
 *  @brief BFGS correction to approximate Newton method with linesearch
 *  @details This class implements the usual full storage BFGS algorithm but uses the BFGS Hessian as a correction term.
 *      The True Hessian of the objective is assumed to be of the form H(x) = C(x) + A.  We assume that the optimization
 *     problem returns the C(x) approximation and we use the BFGS update to approximate A.  A line search is used to
 *     ensure global convergence.
 */
class StructuredBFGS : public GradLineSearch {
public:

  REGISTER_OPT_CONSTRUCTOR(StructuredBFGS)
  /** parameter list constructor
   *  @param[in] ProbPtr A shared_ptr to the optimization problem we want to solve.
   *  @param[in] properties A parameter list containing any parameters that were set by the user.  If the parameters
   * were
   *     not set, default values will be used.
   */
  StructuredBFGS(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties);

  /** step function, return the BFGS Modified Quasi-Newton direction
   *  @return A vector holding the BFGS Modified Quasi-Newton direction scaled by the step size.
   */
  virtual Eigen::VectorXd step();

protected:

  int stepCalls;

  Eigen::VectorXd ApplyFullApproxInverse(const Eigen::VectorXd& xc, const Eigen::VectorXd& y);


  // the gradient at the previous step
  Eigen::VectorXd OldGrad;
  Eigen::VectorXd OldState;

  // the approximate hessian matrix
  Eigen::MatrixXd ApproxHess;

private:

  REGISTER_OPT_DEC_TYPE(StructuredBFGS)
};
} // namespace Optimization
} // namespace muq


#endif // ifndef _StructuredBFGS_h
