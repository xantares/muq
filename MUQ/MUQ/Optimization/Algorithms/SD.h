
#ifndef _SD_H_
#define _SD_H_

#include "MUQ/Optimization/Algorithms/GradLineSearch.h"

namespace muq {
namespace Optimization {
/** @class SD_Line
 *   @ingroup Optimization
 *
 *   @brief Class implementing the steepest descent method with a line search
 *   @details This class implements a simple steepest descent algorithm with a line search for globalization.  You will
 *      almost always want to use something more efficient (like BFGS or one of the NLOPT algorithms), but this provides
 *      an easy to understand illustration of how to implement a gradient based line search algorithm in MUQ.
 */
class SD_Line : public GradLineSearch {
public:

  REGISTER_OPT_CONSTRUCTOR(SD_Line)
  /** Construct an instance of the SD_Line method with the parameters found in paramList.
   *  @param[in] ProbPtr A shared pointer to the optimization problem
   *  @param[in] properties A boost ptree holding the line search settings and any SD-specific parameters
   */
  SD_Line(std::shared_ptr<OptProbBase> ProbPtr, boost::property_tree::ptree& properties) : GradLineSearch(ProbPtr,
                                                                                                          properties)
  {}

  /** The reason we use this class hierarchy (i.e. SD_Line as a child of GradLineSearch) is so that to add a new
   *  line-search based method, this is the only function that really needs to be written.  In general, this function
   *  uses the current state and any other information saved from the past, to produce the delta step.  In the case of
   *  steepest descent, we have \f[\delta = -\alpha\nabla f\f] where \f$\alpha\f$ is the step length, and \f$\nabla f\f$
   *  is the gradient of the objective.
   *  @return A vector holding the pre line-search step: \f$-\alpha\nabla f\f$
   */
  virtual Eigen::VectorXd step();

protected:

private:

  REGISTER_OPT_DEC_TYPE(SD_Line)
};

// class SD_Line
} // namespace Optimization
} // namespace muq

#endif // ifndef _SD_H_
