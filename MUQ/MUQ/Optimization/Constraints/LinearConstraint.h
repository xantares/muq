
#ifndef _LinearConstraint_h
#define _LinearConstraint_h

#include "MUQ/Optimization/Constraints/ConstraintBase.h"


namespace muq {
namespace Optimization {
/** @class LinearConstraint
 *   @ingroup Optimization
 *
 *   @brief Implement a linear constraint
 *   @details While the constraint base function provides a general interface for defining constraints: both linear and
 * nonlinear.  This class focuses on constraints of the form: g(x) = Ax-b.
 */
class LinearConstraint : public ConstraintBase {
public:

  /** Construct the Linear constraint using the linear operator A and the vector b.  The constraint will return Ax-b
   *  @param[in] A A linear operator with type MatType
   *  @param[in] b A vector
   */
  LinearConstraint(const Eigen::MatrixXd& A, const Eigen::VectorXd& b);

  /** Evaluate the linear constraint.
   *  @param[in] xc Input vector
   *  @return A*xc-b
   */
  virtual Eigen::VectorXd eval(const Eigen::VectorXd& xc);

  /** Evaluate the action of the Jacobian of this constraint on a vector.  Another way of thinking of this is that we
   * want to compute the gradient of some objective with respect to the input to this constraint and vecIn is the
   * sensitivity of that objective to the output of this constraint.  In that setting, this function is one step in a
   * chain rule.  For example, if we want to evaluate df/dx =df/dg dg/dx.  This function would take df/dg as input
   * (vecIn) and compute df/dg*dg/dx
   *  @param[in] xc The location where we want to perform the evaluation -- i.e. where do we take the derivative?
   *  @param[in] vecIn The input vector that may represent df/dg
   *  @return The Jacobian of this constraint applied to vecIn
   */
  virtual Eigen::VectorXd ApplyJacTrans(const Eigen::VectorXd& xc, const Eigen::VectorXd& vecIn);

  virtual Eigen::MatrixXd getHess(const Eigen::VectorXd& xc, const Eigen::VectorXd& sensitivity) override;

protected:

  Eigen::MatrixXd Astored;
  Eigen::VectorXd bstored;
};

// class LinearConstraint
} // namespace Optimization
} // namespace muq

#endif // ifndef _LinearConstraint_h
