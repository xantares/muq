#ifndef IMPORTANCESAMPLER_H_
#define IMPORTANCESAMPLER_H_

#include "MUQ/config.h"

#if MUQ_PYTHON == 1
#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"
#endif // MUQ_PYTHON == 1

#include <boost/property_tree/ptree.hpp>

#include "MUQ/Utilities/VariableNameManipulation.h"

#include "MUQ/Modelling/ModPiece.h"
#include "MUQ/Modelling/ModPieceDensity.h"
#include "MUQ/Modelling/ModGraphPiece.h"
#include "MUQ/Modelling/ModGraph.h"
#include "MUQ/Modelling/DensityRVPair.h"
#include "MUQ/Modelling/Density.h"
#include "MUQ/Modelling/RandVar.h"

namespace muq {
namespace Inference {
/// Approximate the expectation of any muq::Modelling::ModPiece with respect to any muq::Modelling::Density
/**
 *  Importance sampling approximates the expectation of a function \f$f(\mu)\f$ with respect to a density over
 * \f$\Omega\f$
 *  \f{equation}{
 *  \int_\Omega f(\mu) \pi(\mu) \, d\mu = \int_\Omega f(\mu) \frac{\pi(\mu)}{q(\mu)} q(\mu) \, d\mu \approx
 * \frac{1}{N}
 * \sum_{i=1}^N f(\mu_i)  \frac{\pi(\mu_i)}{q(\mu_i)} \nonumber
 *  \f}
 *  where \f$\mu_i \sim q(\cdot)\f$.  This class is constructed given \f$f(\mu)\f$, \f$\pi(\mu)\f$, and \f$q(\mu)\f$
 * and approximates the expectation of \f$f(\mu)\f$ with respect to \f$\pi(\mu)\f$ via importance sampling (using
 * \f$q(\mu)\f$ as the importance sampling distribution).
 */
class ImportanceSampler : public muq::Modelling::ModPiece {
public:

  /// Create an importance sampler
  /**
     <ol>
       <li> A list of the output node names <EM>ImportanceSampler.OutputNames</EM>
       <ul>
         <li> Must have four output names:
	 <ol>
	   <li> The output name of the function we are computing the expectation of 
	   <li> The density we are taking the expectation with respect to 
	   <li> The importance sampling density 
	   <li> The importance sampling random variable
	 </ol>
	 <li> Defaults to "", which is invalid
	 </uL>
	 <li> The name of the input being marginalized <EM>ImportanceSampler.MarginalizedNode</EM>
	 <ul>
	   <li> Defaults to "", which is invalid
	 </ul>
	 <li> The number of importance samples to use <EM>ImportanceSampler.N</EM>
	 <ul>
	   <li> Defaults to \f$100\f$
	 </ul>
	 <li> Should we normalize the importance weights? <EM>ImportanceSampler.Normalize</EM>
	 <ul>
	   <li> Defaults to true
	 </ul>
     </ol>
     @param[in] graph A graph whose outputs are the function, target distribution, biasing random variable, and biasing
     @param[in] para A boost::property_tree::ptree containing all the options for the importance sampler
   */
  ImportanceSampler(std::shared_ptr<muq::Modelling::ModGraph> const& graph, boost::property_tree::ptree const& para);

#if MUQ_PYTHON == 1 
  ImportanceSampler(std::shared_ptr<muq::Modelling::ModGraph> const& graph, boost::python::dict const& para);
#endif

  /// Create an importance sampler
  /**
   *  @param[in] graph A graph whose outputs are the function, target distribution, biasing random variable, and biasing
   *distribution
   *  @param[in] names The names of the output nodes
   *  @param[in] marginalizedNode The input node we are taking the expectation with respect to
   *  @param[in] N The number of importance sampling points
   * @param[in] normalize True: normalize importance weights, False: do not normalize
   */
  ImportanceSampler(std::shared_ptr<muq::Modelling::ModGraph> const& graph,
                    std::vector<std::string> const                 & names,
                    std::string const                              & marginalizedNode,
                    unsigned int const                               N,
                    bool const                                       normalize = true);

  /// Create an importance sampler to estimate the marginal of a distribution
  /**
   *
   */
  template<typename rv, typename dens, typename specs>
  ImportanceSampler(std::shared_ptr<muq::Modelling::Density> const& pi,
                    std::shared_ptr<muq::Modelling::DensityRVPair<rv, dens, specs> > const& pair,
                    unsigned int const inputDimWrt,
                    unsigned int const N) :
    ImportanceSampler(MarginalGraph(pi, pair->density, pair->rv, inputDimWrt), MarginalNames(), "para", N, false) {}

  virtual ~ImportanceSampler() = default;

private:

  static Eigen::VectorXi InputSizes(std::shared_ptr<muq::Modelling::ModGraph> const& graph,
                                    std::vector<std::string> const                 & names,
                                    std::string const                              & marginalizedNode);

  /// compute the input sizes for the importance sampler
  /**
     @param[in] graph A graph whose outputs are the function, target distribution, biasing random variable, and biasing
     @param[in] para The importane sampler options
   */
  static Eigen::VectorXi InputSizes(std::shared_ptr<muq::Modelling::ModGraph> const& graph, boost::property_tree::ptree const& para);

  /// compute the output size for the importance sampler
  /**
     @param[in] graph A graph whose outputs are the function, target distribution, biasing random variable, and biasing
     @param[in] para The importane sampler options
   */
  static int OutputSize(std::shared_ptr<muq::Modelling::ModGraph> const& graph, boost::property_tree::ptree const& para);

  static std::shared_ptr<muq::Modelling::ModGraph> MarginalGraph(
    std::shared_ptr<muq::Modelling::Density> const& pi,
    std::shared_ptr<muq::Modelling::Density> const& q,
    std::shared_ptr<muq::Modelling::RandVar> const& rv,
    unsigned int const                              inputDimWrt);

  static std::vector<std::string> MarginalNames();

  /// Input indices for the densities, models, and random variables
  std::vector<int>                InputIndices(std::shared_ptr<muq::Modelling::ModGraph> const& graph,
                                               std::string const& name,
                                               std::string const& marginalizedNode,
                                               std::map<std::string, int>& indexMap,
                                               unsigned int& numInputs) const;

  /// Implement the importance sampling
  /**
   *  Sample \f$mu\f$ from muq::Inference::ImportanceSampler::q and compute the sum
   * \f$\Omega\f$
   *  \f{equation}{
   *  \int_\Omega f(\mu) \pi(\mu) \, d\mu = \int_\Omega f(\mu) \frac{\pi(\mu)}{q(\mu)} q(\mu) \, d\mu \approx
   * \frac{1}{N}
   * \sum_{i=1}^N f(\mu_i)  \frac{\pi(\mu_i)}{q(\mu_i)} \nonumber
   *  \f}
   *  @param[in] inputs The inputs to f in the same order but missing inputDimWrt
   */
  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  /// We are computing the expectation of this muq::Modelling::ModPiece
  std::pair<std::shared_ptr<muq::Modelling::ModGraphPiece>, std::vector<int> > fPair;

  /// The input index to put in the sample from biasing distribution (model)
  int fInd;

  /// The density the expectation is respect to
  std::pair<std::shared_ptr<muq::Modelling::ModPieceDensity>, std::vector<int> > piPair;

  /// The input index to put in the sample from biasing distribution (distribution)
  int piInd;

  /// The density for the importance sampling
  std::pair<std::shared_ptr<muq::Modelling::ModPieceDensity>, std::vector<int> > qDensPair;

  /// The input index to put in the sample from biasing distribution (biasing distribution)
  int qInd;

  /// The random variable for the importance sampling
  std::pair<std::shared_ptr<muq::Modelling::ModGraphPiece>, std::vector<int> > qRVPair;

  /// Number of times to samples
  const unsigned int N;

  /// True: normalize importance weights, False: do not normalizec
  const bool normalize;
};
} // namespace Inference
} // namespace muq

#endif // ifndef IMPORTANCESAMPLER_H_
