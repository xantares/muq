#ifndef MAPUPDATEMANAGERPYTHON_H_
#define MAPUPDATEMANAGERPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/TransportMaps/MapUpdateManager.h"

namespace muq {
namespace Inference {
class MapUpdateManagerPython : public MapUpdateManager, public boost::python::wrapper<MapUpdateManager> {
public:

  MapUpdateManagerPython(std::shared_ptr<TransportMap> mapIn, int expectedSamps, double priorPrecisionIn);

  MapUpdateManagerPython(std::shared_ptr<TransportMap> mapIn, int expectedSamps);

  void PyUpdateMap(boost::python::list const& pyNewSamps);

  boost::python::list PyGetInducedDensities(int startInd, int segmentLength) const;

private:
};

void ExportMapUpdateManager();
} // namespace Inference
} // namespace muq

#endif // ifndef MAPUPDATEMANAGERPYTHON_H_
