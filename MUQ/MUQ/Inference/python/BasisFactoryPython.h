#ifndef BASISFACTORYPYTHON_H_
#define BASISFACTORYPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Inference/TransportMaps/BasisFactory.h"

namespace muq {
namespace Inference {
void ExportBasisSet();

class PyBasisFactory {
public:

  /// delete default constructor
  PyBasisFactory() = delete;

  static std::shared_ptr<BasisSet> PySingleTermHermite(int totalDim, int nonzeroDim, int order);
  static std::shared_ptr<BasisSet> PySingleTermLegendre(int totalDim, int nonzeroDim, int order);
  static std::shared_ptr<BasisSet> PySingleTermMonomial(int totalDim, int nonzeroDim, int order);

  static boost::python::list       PyFullLinear(int dim);
  static boost::python::list       PyTotalOrderHermite(int dim, int order);
  static boost::python::list       PyTotalOrderLegendre(int dim, int order);
  static boost::python::list       PyTotalOrderMonomial(int dim, int order);

  static boost::python::list       PyNoCrossHermite(int dim, int order);
  static boost::python::list       PyNoCrossLegendre(int dim, int order);
  static boost::python::list       PyNoCrossMonomial(int dim, int order);

private:
};

void ExportPyBasisFactory();
} // namespace muq
} // namespace Inference

#endif // ifndef BASISFACTORYPYTHON_H_
