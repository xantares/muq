
#ifndef TEMPEREDMCMC_H
#define TEMPEREDMCMC_H

#include <Eigen/Dense>

#include "MUQ/Inference/MCMC/ParallelChainMCMC.h"

namespace muq {
namespace Modelling {
class EmpiricalRandVar;
}

namespace Inference {
class TemperedMCMC : public ParallelChainMCMC {
public:

  TemperedMCMC(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
               boost::property_tree::ptree                            & properties);

  REGISTER_MCMC_CONSTRUCTOR(TemperedMCMC) virtual ~TemperedMCMC();

  virtual void                                              PrintChildStats();
  virtual void                                              PrintProgress() override;

  virtual std::shared_ptr<muq::Modelling::EmpiricalRandVar> GetResults() override;

protected:

  virtual void SwapChains(unsigned i, unsigned j);

private:

  REGISTER_MCMC_DEC_TYPE(TemperedMCMC) virtual void ProposeSwaps() override;

  ///Record of the temperatures of the chains
  Eigen::VectorXd temperatures;

  ///track the number of proposed swaps
  Eigen::VectorXd proposedSwaps;

  ///track the number of accepted swaps
  Eigen::VectorXd acceptedSwaps;

  static std::vector < std::shared_ptr < SingleChainMCMC >>
  ConstructChildChains(std::shared_ptr<muq::Inference::AbstractSamplingProblem> inferenceProblem,
                       boost::property_tree::ptree & properties);
};
}
}

#endif // TEMPEREDMCMC_H
