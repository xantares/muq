#ifndef TRANSPORTMAPKERNEL_H
#define TRANSPORTMAPKERNEL_H

#include "MUQ/Inference/MCMC/MCMCKernel.h"

namespace muq {
namespace Inference {
class TransportMap;
class MapUpdateManager;
class MixtureProposal;

class TransportMapKernel : public MCMCKernel {
public:

  // TransportMapKernel(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
  //                                     boost::property_tree::ptree& properties);

  TransportMapKernel(std::shared_ptr<AbstractSamplingProblem> samplingProblem,
                     boost::property_tree::ptree            & properties,
                     std::shared_ptr<TransportMap>            initialMap);

  virtual ~TransportMapKernel() = default;

  virtual void                                       PrintStatus() const override;

  std::shared_ptr<TransportMap>                      GetMap() const;

  virtual void                                       SetupStartState(Eigen::VectorXd const& xStart) override;

  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName,
                               std::string        prefix = "") const;

protected:

  Eigen::VectorXd GetMixtureWeights(double var) const;

  std::shared_ptr<AbstractSamplingProblem> mapInducedProblem;

  // construct the reference state from the target state
  std::shared_ptr<MCMCState> ConstructReferenceState(std::shared_ptr<MCMCState> targetState);
  std::shared_ptr<MCMCState> ConstructTargetState(std::shared_ptr<MCMCState> referenceState);

  void                       PostStepProcessing(std::shared_ptr<muq::Inference::MCMCState> currentState,
                                                int const                                  currIteration);

  // some parameters on when to adapt
  int adaptStart, adaptGap, adaptStop;

  // the prior-regularization weight for the optimization problem
  double adaptScale;


  std::shared_ptr<TransportMap> transportMap;

  // class to update the map with new samples
  std::shared_ptr<MapUpdateManager> mapManager;

  // transition kernel on the reference space
  std::shared_ptr<MCMCKernel> referenceKernel;

  // guess at the map inverse, this pointer is used to pass information to the map-induced density
  std::shared_ptr<Eigen::VectorXd> inverseGuess;

  // mixture-proposal related variables
  double varScale, maxIndProb;
  bool   propIsMixture;
  std::shared_ptr<MixtureProposal> mixPtr;

  bool monitorMapConvergence;
  Eigen::VectorXd targetDensities;
  double varT  = -1.0;
  double meanT = -1.0;

  // we store new samples from the last time the map was updated
  Eigen::MatrixXd newSamps;
  int newSampInd;

  Eigen::VectorXd oldTargetState;
  std::shared_ptr<muq::Inference::MCMCState> currentReferenceState;
};
} // namespace inference
} // namespace muq


#endif // TRANSPORTMAPKERNEL_H_