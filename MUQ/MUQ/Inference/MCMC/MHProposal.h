
#ifndef MH_H_
#define MH_H_

#include "MUQ/Inference/MCMC/MCMCProposal.h"
#include "MUQ/Modelling/GaussianPair.h"


namespace muq {
namespace Inference {
/**
 *         \class MH
 *         \author Matthew Parno
 *         \brief Simple MH algorithm with isotropic proposal
 *         This class implements a basic Metropolis-Hastings style MCMC algorithms using a simple isotropic Gaussian
 * proposal density.
 */
class MHProposal : public MCMCProposal {
public:

  /** Read in parameters a from parsed xml document.
   *             @param[in] paramList Parameter list containing the MCMC parameters such as number of steps, burnin, and
   * isotropic variance
   */
  MHProposal(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties);

  virtual ~MHProposal() {}

  virtual std::shared_ptr<muq::Inference::MCMCState> DrawProposal(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry,
    int const currIteration) override;

  virtual void PostProposalProcessing(
    std::shared_ptr<muq::Inference::MCMCState> const currentState,
    MCMCProposal::ProposalStatus const               proposalAccepted,
    int const                                        iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry>    logEntry) {}


  virtual double ProposalDensity(
    std::shared_ptr<muq::Inference::MCMCState> currentState,
    std::shared_ptr<muq::Inference::MCMCState> proposedState) override;

  /** Set the constant proposal.  The input distribution is for the step from the current state, so an isotropic
   *  proposal will have zero mean and some scaled identity for a covariance.
   *             @param[in] newProp Smart pointer (shared_ptr or the like) to a gaussian to use as the proposal density.
   **/
  virtual void setProposal(std::shared_ptr<muq::Modelling::GaussianPair> newProp);

  /** Set the constant proposal to an isotropic Gaussian distribution with zero mean and variance var
   *             @param[in] var Variance (diagonal of covariance matrix) to use for proposal.
   */
  virtual void setProposal(double var);


  /** Write important information about the settings in this proposal as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName, std::string prefix = "") const override;

protected:

  /// pointer to proposal
  std::shared_ptr<muq::Modelling::GaussianPair> proposal;

private:

  double propSize;
};
} // namespace inference
} // namespace muq

#endif /* MH_H_ */
