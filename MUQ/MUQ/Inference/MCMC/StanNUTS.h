
#ifndef STANNUTS_H
#define STANNUTS_H

#include <boost/random.hpp>
#include "stan/mcmc/nuts.hpp"

#include "MUQ/Inference/MCMC/MCMCKernel.h"


namespace muq {
namespace Inference {
class StanModelInterface;

/**
 * This class is a wrapper around Stan's NUTS class. It adapts the number of leaps and the step size, so has no
 * user parameters.
 *
 * Note that we do not provide the random number generator for this class, so be careful
 * about that assumption.
 *
 * See also Inference/ProblemClasses/StanModelInterface.h
 */
class StanNUTS : public MCMCKernel {
public:

  StanNUTS(std::shared_ptr<muq::Inference::AbstractSamplingProblem> samplingProblem,
           boost::property_tree::ptree                            & properties);
  virtual ~StanNUTS() {}

  ///Perform setup using the start state. Most do not use this, but the Stan interfaces do.
  virtual void SetupStartState(Eigen::VectorXd const& xStart) override;

  virtual void PrintStatus() const override;

  //   typedef std::tuple < std::shared_ptr < muq::Inference::MCMCState >, bool> NewState; //the new state and whether
  // it reflects an accepted move
  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override;

  /** Write important information about the settings in this kernel as attributes in an HDF5 file.
   *   @params[in] groupName The location in the HDF5 file where we want to add an attribute.
   *   @params[in] prefix A string that should be added to the beginning of every prefix name.
   */
  virtual void WriteAttributes(std::string const& groupName,
                               std::string        prefix = "") const;

  /** Get the acceptance rate.
   *             @return The acceptance rate in [0,1]
   */
  virtual double GetAccept() const;

private:

  /// keep track of the number of accepted steps
  int numAccepts = 0;

  /// keep track of the total number of times ConstructNextState was called
  int totalSteps = 0;

  std::shared_ptr < stan::mcmc::nuts < boost::mt19937 >> nutsSampler;

  std::shared_ptr<muq::Inference::StanModelInterface> modelInterface;
};
}
}

#endif // STANNUTS_H
