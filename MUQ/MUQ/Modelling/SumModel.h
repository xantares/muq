
#ifndef SUMMODEL_H_
#define SUMMODEL_H_

#include "MUQ/Modelling/ModPiece.h"

namespace muq {
namespace Modelling {
/// A model that returns the sum of the inputs
/**
 *  Takes an arbitrary number of inputs (of the same length) and returns their componentwise sum.
 */
class SumModel : public ModPiece {
public:

  // Basic constructor
  /**
   *  @param[in] numInputs The number of input vectors
   *  @param[in] outputSize The output size (and the input size for ALL input vectors)
   */
  SumModel(int const numInputs, int const outputSize);

  virtual ~SumModel() = default;
private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& inputs,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;

};
} // namespace Modelling
} // namespace muq

#endif // ifndef SUMMODEL_H_
