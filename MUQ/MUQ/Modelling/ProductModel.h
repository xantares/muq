
#ifndef PRODUCTMODEL_H_
#define PRODUCTMODEL_H_

#include "MUQ/Modelling/ModPiece.h"

namespace muq {
namespace Modelling {
/// A model that returns the product of the inputs
/**
 *  Takes an arbitrary number of inputs (of the same length) and returns their componentwise product.
 */
class ProductModel : public ModPiece {
public:

  // Basic constructor
  /**
   *  @param[in] numInputs The number of input vectors
   *  @param[in] outputSize The output size (and the input size for ALL input vectors)
   */
  ProductModel(int const numInputs, int const outputSize);
  
  virtual ~ProductModel() = default;

private:

  virtual Eigen::VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& inputs) override;

  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& inputs, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& inputs,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt) override;

  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;

  virtual Eigen::MatrixXd HessianImpl(std::vector<Eigen::VectorXd> const& inputs,
                                       Eigen::VectorXd const             & sens,
                                       int const                           inputDimWrt) override;

};
} // namespace Modelling
} // namespace muq

#endif // ifndef PRODUCTMODEL_H_
