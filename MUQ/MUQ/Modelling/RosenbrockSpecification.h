#ifndef ROSENBROCKSPECIFICATION_H_
#define ROSENBROCKSPECIFICATION_H_

#include "MUQ/Modelling/Specification.h"

namespace muq {
  namespace Modelling {
    /// Specification for a 2D Rosenbrock denisty/random variable
    class RosenbrockSpecification : public Specification {
    public:

      /// Construct specification from known, constant parameters 
      /**
	 @param[in] a Parameter \f$a\f$
	 @param[in] b Parameter \f$b\f$
       */
      RosenbrockSpecification(double const a, double const b);

      /// Default destructor
      ~RosenbrockSpecification() = default;

      /// Return the valut of parameter \f$a\f$ 
      /**
	 Extracts the inputs \f$a\f$ requires and evaluates the muq::Modelling::ModPiece
	 @param[in] inputs The inputs for all parameters
	 \return The value of parameter \f$a\f$
       */
      double a(std::vector<Eigen::VectorXd> const& inputs);

      /// Return the valut of parameter \f$b\f$ 
      /**
	 Extracts the inputs \f$a\f$ requires and evaluates the muq::Modelling::ModPiece
	 @param[in] inputs The inputs for all parameters
	 \return The value of parameter \f$b\f$
       */
      double b(std::vector<Eigen::VectorXd> const& inputs);
      
    private:

      /// A graph to hold the parameters when they are known scalars
      /**
	 @param[in] a Parameter \f$a\f$
	 @param[in] b Parameter \f$b\f$
	 \return A graph with two muq::Modelling::ModParameter nodes (one for each parameter)
       */
      static std::shared_ptr<ModGraph> ConstantParametersGraph(double const a, double const b);

      /// Make the vector of parameter names 
      /**
	 @param[in] a Name of parameter \f$a\f$
	 @param[in] b Name of parameter \f$b\f$
	 \return A vector [a b]
       */
      static std::vector<std::string> MakeParameterNames(std::string const a, std::string const b);

    };
  } // namespace Modelling 
} // namespace muq

#endif
