#ifndef CONCATENATEMODELPYTHON_H_
#define CONCATENATEMODELPYTHON_H_

#include "MUQ/Modelling/ConcatenateModel.h"

namespace muq { 
  namespace Modelling {
    void ExportConcatenateModel();
  } // namespace Modelling
} // namespace muq

#endif
