#ifndef ROSENBROCKDENSITYPYTHON_H_
#define ROSENBROCKDENSITYPYTHON_H_

#include "MUQ/Modelling/RosenbrockDensity.h"

namespace muq {
  namespace Modelling {
    void ExportRosenbrockDensity();
  } // namespace Modelling 
} // namespace muq

#endif
