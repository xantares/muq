#ifndef UNIFORMPYTHON_H_
#define UNIFORMPYTHON_H_

#include <boost/python.hpp>
#include <boost/python/def.hpp>
#include <boost/python/class.hpp>

#include "MUQ/Utilities/python/PythonTranslater.h"

#include "MUQ/Modelling/UniformSpecification.h"
#include "MUQ/Modelling/UniformDensity.h"
#include "MUQ/Modelling/UniformRandVar.h"

namespace muq {
namespace Modelling {
class UniformBoxPython : public UniformBox, public boost::python::wrapper<UniformBox> {
public:

  UniformBoxPython(boost::python::list const& lb, boost::python::list const& ub);

  /// Python wrapper to sample
  boost::python::list PySample();

  /// Python wrapper on PredicateFun
  double              PyPredicateFn(boost::python::list const& input);

  /// Python wrapper on Density
  double              PyDensity(boost::python::list const& input);
};

void           ExportUniform();

} // namespace modelling
} // namespace modelling


#endif // ifndef UNIFORMPYTHON_H_
