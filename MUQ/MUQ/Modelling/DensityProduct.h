
#ifndef _DensityProduct_h
#define _DensityProduct_h


#include "MUQ/Modelling/Density.h"

namespace muq {
namespace Modelling {
/** @ingroup Modelling
 *  @class DensityProduct
 *  @author Matthew Parno
 *  @brief Handles densities that are products of two other Densities
 *  @details This class forms the product density by summing its inputs, since densities are in log-space. It may have
 * as
 *     many inputs
 *  as desired, although each must be a length-1 vector.
 */
class DensityProduct : public Density {
public:

  /** Create a density product from smart pointers to DensityBase instances */
  DensityProduct(int const numInputs);

  virtual ~DensityProduct() = default;

private:

  virtual double          LogDensityImpl(std::vector<Eigen::VectorXd> const& input) override;

  /**
   * Say that you have an adjoint where you need to run the forward model first, then can compute
   * a gradient. The one step caching means that your implementation here may simply call Evaluate()
   * as the first step, which will ensure that it either has been run exactly once at this point, or
   * else will do so. This concept gets strange if the quantity is random.
   **/
  virtual Eigen::VectorXd GradientImpl(std::vector<Eigen::VectorXd> const& input,
                                       Eigen::VectorXd const             & sensitivity,
                                       int const                           inputDimWrt) override;

  ///The jacobian is outputDim x inputDim
  virtual Eigen::MatrixXd JacobianImpl(std::vector<Eigen::VectorXd> const& input, int const inputDimWrt) override;

  virtual Eigen::VectorXd JacobianActionImpl(std::vector<Eigen::VectorXd> const& input,
                                             Eigen::VectorXd const             & target,
                                             int const                           inputDimWrt = 0) override;
};

// class DensityProduct
} // namespace Modelling
} // namespace muq


#endif // ifndef _DensityProduct_h
