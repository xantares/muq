#ifndef POLYNOMIALREGRESSORPYTHON_H_
#define POLYNOMIALREGRESSORPYTHON_H_

#include "MUQ/Approximation/Regression/PolynomialRegressor.h"

namespace muq {
  namespace Approximation {
    void ExportPolynomialRegressor();
  } // namespace Approximation 
} // namespace muq

#endif
