
#ifndef SMOLYAKTRADITIONALPCEFACTORY_H_
#define SMOLYAKTRADITIONALPCEFACTORY_H_


#include "PolynomialChaos/smolyak/SmolyakEstimate.h"
#include "PolynomialChaos/pce/PolynomialChaosExpansion.h"

class VariableCollection;
class FunctionWrapper;

/**
 * An implementation for performing the traditional way of generating PCEs. I'm not
 * commenting it because you shouldn't use it - it's only here to demonstrate why it's bad.
 *
 *  It's a lot like SmolyakPCEFactory if you're curious, except that you recompute
 *  the old index estimates to include the any new polynomial terms you add to the set.
 */
class SmolyakTraditionalPCEFactory : public SmolyakEstimate<PolynomialChaosExpansion::Ptr> {
public:

  SmolyakTraditionalPCEFactory(std::shared_ptr<VariableCollection> inVariables, std::shared_ptr<FunctionWrapper> inFn);
  virtual ~SmolyakTraditionalPCEFactory();

  virtual unsigned int ComputeWorkDone() const;

private:

  typedef SmolyakEstimate<PolynomialChaosExpansion::Ptr> super; //define the type super for convenience

  PolynomialChaosExpansion::Ptr ComputeOneEstimate(arma::urowvec const& multiIndex);

  /**
   * A pure abstract function for subclasses to override, must sum the termEstimates with
   * multiplicative weights. Returns a pointer to the result.
   */
  PolynomialChaosExpansion::Ptr WeightedSumOfEstimates(arma::colvec const& weights) const;

  ///Compute the magnitude of the input estimate, used for error estimation
  double                        ComputeMagnitude(PolynomialChaosExpansion::Ptr estimate);

  double                        ComputeLocalErrorIndicator(unsigned int const termIndex);

  ///actually return the orders of the quadrature
  arma::umat                    GetEffectiveIncludedTerms();

  bool                          Refine(arma::ucolvec& newTermIndices);

  std::shared_ptr<VariableCollection> variables;

  std::shared_ptr<FunctionWrapper> fn;
};

#endif /* SMOLYAKTRADITIONALPCEFACTORY_H_ */
