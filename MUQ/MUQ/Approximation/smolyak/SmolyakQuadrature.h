#ifndef SMOLYAKQUADRATURE_H_
#define SMOLYAKQUADRATURE_H_

#include <string.h>

#include <boost/function.hpp>
#include <boost/serialization/access.hpp>
#include <boost/property_tree/ptree_fwd.hpp>

#include <Eigen/Core>

#include "MUQ/Approximation/smolyak/SmolyakEstimate.h"

namespace muq {
namespace Modelling {
class ModPiece;
}
namespace Utilities{
  class VariableCollection;
}
}

namespace muq {
namespace Approximation {
/**
 @class SmolyakQuadrature
 @ingroup Quadrature
 @brief Adaptive and fixed Smolyak quadrature
 @details 
 * Implements Smolyak estimation for quadrature of functions
 * that have inputs and outputs as Eigen::VectorXd's.
 *
 * The VariableCollection specifies the 1D quadrature rules
 * to use, and the FunctionWrapper specifies the function.
 @see SmolyakEstimate
 */
class SmolyakQuadrature : public SmolyakEstimate<Eigen::VectorXd> {
public:

  typedef std::shared_ptr<SmolyakQuadrature> Ptr;

  ///default constructor ONLY for serialization
  SmolyakQuadrature();

  SmolyakQuadrature(boost::property_tree::ptree const& pt,
                    std::shared_ptr<muq::Modelling::ModPiece> inFn);
  
  ///Initialize the function and variables.
  SmolyakQuadrature(std::shared_ptr<muq::Utilities::VariableCollection>       inVariables,
                    std::shared_ptr<muq::Modelling::ModPiece> inFnWrapper);
  virtual ~SmolyakQuadrature();


  /**
   * Load previously saved progress. Because saving function pointers is hard,
   * you must provide a function wrapper with the same function as before.
   */

  //       static Ptr LoadProgress(std::string baseName, boost::function<Eigen::VectorXd (Eigen::VectorXd const&)> fn);

  /**
   * Save the progress so far using boost::serialization. Writes to results/
   */

  //       static void SaveProgress(SmolyakQuadrature::Ptr quad, std::string baseName);

  virtual unsigned int ComputeWorkDone() const;

private:

  //       ///Make estimates serializable
  //       friend class boost::serialization::access;
  //
  //       template<class Archive>
  //       void serialize(Archive & ar, const unsigned int version);

  /**
   * Computes the full tensor quadrature of order multiIndex as defined
   * by the 1D quadrature rules in the variable collection.
   */
  Eigen::VectorXd ComputeOneEstimate(Eigen::RowVectorXu const& multiIndex);

  double          CostOfOneTerm(Eigen::RowVectorXu const& multiIndex);

  /**
   * Computes the weighted sum using simple linear algebra.
   */
  Eigen::VectorXd WeightedSumOfEstimates(Eigen::VectorXd const& weights) const;

  ///Computes the magnitude as the max of the abs of the estimate.
  virtual double  ComputeMagnitude(Eigen::VectorXd estimate);

  ///Asks the quadrature rules what their precise polynomial order is.
  Eigen::MatrixXu GetEffectiveIncludedTerms();

  /** @brief Defines the MultiIndexLimiter to use during adaptation.
   @details The 1D quadrature rules used in this class may only be defined up to a maximum level.  This function looks through the variable collection and creates a MultiIndexLimiter that prevents the adaptive SmolyakQuadrature algorithm from adding unfeasible levels.
   @param[in] inVariables A shared ptr to a variable collection holding the quadrature rules.
   @return A shared_ptr to a MultiIndexLimiter that returns true when the quadrature limits are satisfied.
   */
  static std::shared_ptr<muq::Utilities::MultiIndexLimiter> GetLimiter(std::shared_ptr<muq::Utilities::VariableCollection> inVariables);
  
  
  std::shared_ptr<muq::Utilities::VariableCollection> variables;
  std::shared_ptr<muq::Modelling::ModPiece> fnWrapper;
};
}
}


#endif /* SMOLYAKQUADRATURE_H_ */
