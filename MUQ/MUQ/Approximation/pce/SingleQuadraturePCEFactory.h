#ifndef SINGLEQUADRATUREPCEFACTORY_H_
#define SINGLEQUADRATUREPCEFACTORY_H_

#include "MUQ/Approximation/pce/PolynomialChaosExpansion.h"

namespace muq {
namespace Modelling {
  ///forward declarations to avoid includes
  class ModPiece;
}
namespace Utilities{
  class VariableCollection;
}

namespace Approximation {

/** @class SingleQuadraturePCEFactory
    @ingroup PolynomialChaos
    @brief A factory class for constructing a polynomial expansion using a fixed quadrature rule.
    @see SmolyakPCEFactory
 */
class SingleQuadraturePCEFactory {
public:

  typedef std::shared_ptr<SingleQuadraturePCEFactory> Ptr;

  ///Initialize the PCE Factory with a collection of variables.
  SingleQuadraturePCEFactory(std::shared_ptr<muq::Utilities::VariableCollection> variables);

  virtual ~SingleQuadraturePCEFactory();

  /**
   * Construct a PCE approximating the input function and return a pointer to it.
   *
   * The quadrature is done with a single full tensor rule of order order+offset, and is defined
   * by the quadrature rules in the variable collection. The default behavior is to include in
   * the PCE all the terms that this quadrature rule can correctly integrate. Typically this
   * leads to a full tensor rule. The parameter tensorOrdersIncluded gives more flexibility,
   * allowing simulation of traditional PCE generation.
   *
   * @param fn the function to approximate
   * @param order This is the order of the full tensor quadrature rule to used.
   * @param offset The true order for quadrature used is order + offset, we do this
   * because Smolyak starts with 0th order rules, but quadrature only exists for 1st order
   * rules. Thus, we often add an offset of one. If the user calls it directly, use zero
   * offset. A vec with the same length as the number of variables.
   * @param tensorOrdersIncluded This variable allows you to accomplish traditional
   * PCE generation. If you pass in all the quadrature tensor orders included in the
   * expansion, it will create a PCE including all the terms that any of the quadrature orders
   * can integrate correctly. Omit for the full tensor PCE.
   * @return
   */
  PolynomialChaosExpansion::Ptr ConstructPCE(std::shared_ptr<muq::Modelling::ModPiece>     fn,
                                             Eigen::RowVectorXu const                      &order,
                                             Eigen::RowVectorXu const                      &offset,
                                             std::shared_ptr<muq::Utilities::MultiIndexSet> tensorOrdersIncluded = nullptr);

private:

  ///The variables to build the PCE around.
  std::shared_ptr<muq::Utilities::VariableCollection> variables;
};
}
}

#endif /* SINGLEQUADRATUREPCEFACTORY_H_ */
