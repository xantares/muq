#ifndef POLYNOMIALAPPROXIMATOR_H_
#define POLYNOMIALAPPROXIMATOR_H_

#include "MUQ/config.h"

#include <boost/optional.hpp>

#if MUQ_PYTHON == 1
# include <boost/python.hpp>
# include <boost/python/def.hpp>
# include <boost/python/class.hpp>

# include "MUQ/Utilities/python/PythonTranslater.h"
#endif // if MUQ_PYTHON == 1

#include "MUQ/Modelling/CachedModPiece.h"

#include "MUQ/Optimization/Problems/OptProbBase.h"
#include "MUQ/Approximation/Incremental/Approximator.h"
#include "MUQ/Approximation/Regression/PolynomialRegressor.h"

namespace muq {
namespace Approximation {
/// Build, evaluate, and refine polynomial model approximations.
class PolynomialApproximator : public Approximator {
public:

  /// Construct a polynomial approximator
  /**
   *  The boost::property_tree::ptree must also include the options for the polynomial regressor
   * muq::Approximation::PolynomialRegressor and optimization muq::Optimization::OptAlgBase.
   *  <ol>
   *  <li> The number of nearest neighbors to use <EM>PolynomialApproximator.kN</EM>
   *    <ul>
   *      <li> Defaults to the number of points to interpolate
   *    </ul>
   *  <li> The tolerance for space filling <EM>PolynomialApproximator.SpaceFillTol</EM>
   *    <ul>
   *      <li> Defaults to \f$1 \times 10^{-3}\f$
   *    </ul>
   *  <li> The number of times to try space filling refinement <EM>PolynomialApproximator.SpaceFillTries</EM>
   *    <ul>
   *      <li> Defaults to \f$3\f$
   *    </ul>
   *  <li> An upper bound on the \f$(p+1)^{th}\f$ derivative of the approximated function
   * <EM>PolynomialApproximator.DerivativeBound</EM>
   *    <ul>
   *      <li> \f$p\f$ is the order of the polynomial approximation
   *      <li> Defaults to \f$1\f$
   *    </ul>
   *   <li> The variance of the function to be approximated <EM>PolynomialApproximator.FunctionVariance</EM>
   *     <ul>
   *       <li> Defaults to \f$0.0\f$ (Nonnoisy function)
   *     </ul>
   *  </ol>
   *  @param[in] fnIn The model we wish to approximate (must have only one input)
   *  @param[in] para Options for the approximator
   */
  PolynomialApproximator(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn, boost::property_tree::ptree para);

  virtual ~PolynomialApproximator() = default;
  /// Reset the number of nearest neighbors to use
  /**
   *  @param[in] newKN The new number of nearest neighbors to use
   *  \return True the newKN is less than (or equal to) the number of points in the cache, false if not
   */
  bool ResetNumNearestNeighbors(unsigned int const newKN);

  /// Get the number of nearest neighbors
  /**
   *  \return The number of nearest neighbors currently being used by the regression
   */
  unsigned int NumNearestNeighbors() const;

  /// Get the number of available nearest neighbors
  /**
   *  \return The number of nearest neighbors currently available for use by the regression
   */
  unsigned int NumAvailableNearestNeighbors() const;

#if MUQ_PYTHON == 1
  /// Construct a polynomial approximator using a python dictionary instead of a boost::property_tree::ptree
  /**
   *  The boost::python::dict must also include the options for the polynomial regressor
   * muq::Approximation::PolynomialRegressor.
   *  @param[in] fnIn The model we wish to approximate (must have only one input)
   *  @param[in] para Options for the approximator
   */
  PolynomialApproximator(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn, boost::python::dict const& para);
#endif // MUQ_PYTHON == 1


  /// Get the radius of the ball used for the local approximation at a point
  /**
   *  Does not acutally compute the approximation or change the locally regression; just find the
   * muq::Approximation::PolynomialApproximator::kN nearest neighbors and return the largest distance from the point.
   *  @param[in] point The point
   *  \return The radius of the local approximation
   */
  double LocalRadius(Eigen::VectorXd const& point) const;

#if MUQ_PYTHON == 1
  /// Get the radius of the ball used for the local approximation at a point
  /**
   *  Does not acutally compute the approximation or change the locally regression; just find the
   * muq::Approximation::PolynomialApproximator::kN nearest neighbors and return the largest distance from the point.
   *  @param[in] point The point
   *  \return The radius of the local approximation
   */
  double PyLocalRadius(boost::python::list const& point) const;
#endif // MUQ_PYTHON == 1

  /// Compute the error indicator at a point
  /**
   *  Compute an error indicator based on the error bound of the local regression
   *  @param[in] point The point
   *  \return The error indicator
   */
  double ErrorIndicator(Eigen::VectorXd const& point) const;

#if MUQ_PYTHON == 1
  /// Compute the error indicator at a point
  /**
   *  Compute an error indicator based on the error bound of the local regression
   *  @param[in] point The point
   *  \return The error indicator
   */
  double      PyErrorIndicator(boost::python::list const& point) const;
#endif // MUQ_PYTHON == 1

  virtual int GetNumberOfCrossValidationFits() const override;

private:

  /// Evaluated the (local) polynomial approximating fn
  /**
   *  @param[in] input The input point
   */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override;

  /// Compute the Jacobian of (local) polynomial approximating fn
  /**
   *  @param[in] input The input point
   */
  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override;

protected:

  /// Fit the regressor using local points
  /**
   *  @param[in] point The point to center the approximation around
   */
  void ComputeRegression(Eigen::VectorXd const& point);

  /// The polynomial regressor used to build the local approximations
  std::shared_ptr<PolynomialRegressor> regressor;

  /// An upper bound on the \f$(p+1)^{th}\f$ derivative of the approximated function
  /**
   *  \f$p\f$ is the order of the polynomial approximation.  This is used in the error indicator.
   */
  double derivativeBound;

  /// Variance of the function to be approximated
  double fnVar;
};


/**
 * A simple add on to the GPApproximator that allows you to shift the mode of operation, so that Evaluate
 * can return the results of cross validation instead of the nominal values.
 */
class PolynomialApproximatorWithModes : public PolynomialApproximator {
public:

  virtual ~PolynomialApproximatorWithModes() = default;

  ///Delgate to the main constructor.
  PolynomialApproximatorWithModes(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn,
                                  boost::property_tree::ptree                      para) : PolynomialApproximator(fnIn,
                                                                                                                  para)
  {}


#if MUQ_PYTHON == 1

  ///Delgate to the main constructor.
  PolynomialApproximatorWithModes(std::shared_ptr<muq::Modelling::ModPiece> const& fnIn,
                                  boost::python::dict const                      & para) : PolynomialApproximator(fnIn,
                                                                                                                  para)
  {}

#endif // MUQ_PYTHON == 1


  virtual   void SetMode(Modes newMode);

  ///Select which index is desired, and set to crossValidation mode.
  virtual void   SetCrossValidationIndex(int const i);

private:

  /// Evaluated the (local) polynomial approximating fn
  /**
   *  @param[in] input The input point
   */
  virtual Eigen::VectorXd EvaluateImpl(Eigen::VectorXd const& input) override;

  /// Compute the Jacobian of (local) polynomial approximating fn
  /**
   *  @param[in] input The input point
   */
  virtual Eigen::MatrixXd JacobianImpl(Eigen::VectorXd const& input) override;


  ///Which mode the evaluations occur in
  Modes mode = standard;

  ///If the mode is crossValidation, which of the fits to return
  int cvIndex = 0;
};
} // namespace Approximation
} // namespace muq

#endif // ifndef POLYNOMIALAPPROXIMATOR_H_
