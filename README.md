# README #

Welcome to MUQ, the MIT Uncertainty Quantification library.  Check out our [new website](http://muq.mit.edu) for more details!

### What is MUQ? ###

In a nutshell, MUQ is a collection of tools for constructing models and a collection of UQ-orientated algorithms for working on those model.  Our goal is to provide an easy and clean way to set up and efficiently solve uncertainty quantification problems.  On the modelling side, we have a suite of tools for:

* Combining many simple model components into a single sophisticated model.
* Propagating derivative information through sophisticated models.
* Solving systems of partial differential equations (via LibMesh)
* Integrating ordinary differential equations and differential algebraic equations (via Sundials)

Furthermore, on the algorithmic side, we have tools for

* Performing Markov chain Monte Carlo (MCMC) sampling
* Constructing polynomial chaos expansions (PCE)
* Computing Karhunen-Loeve expansions
* Building optimal transport maps
* Solving nonlinear constrained optimization problems (both internally and through NLOPT)
* Solving robust optimization problems
* Regression (including Gaussian process regression)


### How do I get started? ###

1. The first step is to download and install MUQ.  Currently, the best way to get started is by checking our our git repository from this bitbucket site.  Go to the folder you where you want to keep the MUQ source code, and use:
```
#!bash

git clone https://bitbucket.org/mituq/muq.git
```
2. Now that you have the MUQ source, you need to compile and install MUQ.  In a very basic installation of MUQ, all you need to do is specify an installation prefix.  To keep all of the installed MUQ files together, we suggest using something other than /usr/local for the prefix.  A typically choice may be ~/MUQ_INSTALL.  For this basic installation of MUQ, change into muq/MUQ/build/ and type:
```
#!bash

cmake -DCMAKE_INSTALL_PREFIX=/your/install/directory ../
```
For more information on installing MUQ, see our [installation guide](http://muq.mit.edu/develop-docs/muqinstall.html). During this command, cmake will generate a make file, which can now be run as usual:
```
#!bash

make -j4 install
```
The "-j4" is an option specifying that make can use 4 threads for parallel compilation.
3. After installing MUQ, check out the examples in the muq/MUQ/examples to start MUQ'ing around!  Additional Doxygen generated documentation can also be found [here](http://muq.mit.edu/develop-docs/).


### Where can I get help? ###
There are currently two ways to learn more about MUQ and get help.  The first is to dig into the doxygen documentation found [here](http://muq.mit.edu/develop-docs/) and the second is to post a question to our [Q&A site](http://muq.mit.edu/qa).  A manual is also in the works for future inquiries.

### Nightly builds ###
[![Build Status](https://acdl.mit.edu/csi/buildStatus/icon?job=MUQ_Develop_Nightly)](https://acdl.mit.edu/csi/job/MUQ_Develop_Nightly/)